<style>
    .modal-dialog {
        min-height: calc(100vh - 60px);
        display: flex;
        flex-direction: column;
        justify-content: center;
    }

    @media(max-width: 768px) {
        .modal-dialog {
            min-height: calc(100vh - 20px);
        }
    }
</style>


<script type="text/javascript">
    $(document).ready(function() {
        $("#link-forgot").click(function() {
            $(".title-forget").show();
            $(".form-for").show();
            $(".bg-for").show();
            $(".title-log").hide();
            $(".form-log").hide();
            $(".manvec").hide();
        });

        $("#btn-back").click(function() {
            $(".title-forget").hide();
            $(".form-for").hide();
            $(".bg-for").hide();
            $(".title-log").show();
            $(".form-log").show();
            $(".manvec").show();
        });
    });
    $(function() {
        $(".form-signin").on('submit', function() {
            $(".resultlogin").html("<div class='alert alert-success loading '>Hold On...</div>");
            $.post("<?php echo base_url() . $this->uri->segment(1); ?>/attemp", $(".form-signin").serialize(), function(response) {
                var resp = $.parseJSON(response);
                console.log(resp);
                if (!resp.status) {
                    $('#failedmodal').modal('show');
                    $(".print-error-msg").html(resp.error);
                } else {
                    $('#successModal').modal('show');

                }
            });
        });

        $(".resetbtn").on('click', function() {
            var resetemail = $("#resetemail").val();
            if (resetemail == "") {
                $('#pleasemodal').modal('show');
            } else {
                $(".resultreset").html("<div class='matrialprogress'><div class='indeterminate'></div></div>");
                $.post("<?php echo base_url() . $this->uri->segment(1); ?>/resetpass", $("#passresetfrm").serialize(), function(response) {
                    if ($.trim(response) == '1') {
                        $(".resultreset").html("<div class='alert alert-success'>New Password sent to " + resetemail + ", Kindly check email.</div>");
                    } else {
                        $(".resultreset").html("<div class='alert alert-danger'>Email Not Found</div>");
                    }
                });
            }
        });

    });
</script>
<script>
    // brand
    function hover_brand(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMAGES . 'brand-hov.png') ?>'
        );
    }

    function unhover_brand(element) {
        element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
    }
    // competition
    function hover_com(element) {
        element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
        $('.hov').show();
        $('.hov-1').hide();
    }
    $(document).ready(function() {
        $('.hov-menu').mouseleave(function() {
            $('.hov').hide();
        });
    });

    // home
    function hover(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMAGES . 'home-hov.png') ?>'
        );
    }

    function unhover(element) {
        element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
    }

    function unhover_com(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMAGES . 'COMPETITION.png') ?>'
        );
    }
    // join
    function hover_join(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
        $('.hov-1').show();
        $('.hov').hide();
    }

    $(document).ready(function() {
        $('.hov-1').mouseleave(function() {
            $('.hov-1').hide();
        });
    });

    function unhover_join(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
    }

    // about competition

    function hover_about_com(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>'
        );
    }

    function unhover_about_com(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMGS . 'about-competition.svg') ?>'
        );
    }

    // upload

    function hover_upload(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMGS . 'upload-blue.svg') ?>'
        );
    }

    function unhover_upload(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'UPLOAD.svg') ?>');
    }

    // register

    function hover_regis_new(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>'
        );
    }

    function unhover_regis_new(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER.svg') ?>');
    }

    $(document).ready(function() {
        AOS.init();
    });
</script>

<!-- menu mobile -->
<script>
	
		// competition
		function com() {
			$("#com").hide();
			$("#commob").show();
			$("#ul").show();
			$("#aboutsub").show();
			$("#aboutpr").show();
			$("#aboutvw").show();
			$("#aboutjr").show();
			$("#aboutdg").show();
			$("#abouttem").show();
			$("#abouttoc").show();
			$("#aboutuplo").show();
		}
		function comcolor() {
			$("#commob").hide();
			$("#ul").hide();
			$("#aboutsub").hide();
			$("#aboutpr").hide();
			$("#aboutvw").hide();
			$("#aboutjr").hide();
			$("#aboutdg").hide();
			$("#abouttem").hide();
			$("#abouttoc").hide();
			$("#aboutuplo").hide();
			$("#com").show();
		}

</script>

<div class="modal fade" id="successModal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body text-center p-3 pt-4 pb-5">

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h6>Welcome to ESSE CHANGE SV design competition</h6>
                    </div>
                    <div class="col-md-12 mt-4">
                        <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMAGES . 'back-home.png') ?>" class="img-fluid px-3 mbm"  />
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="failedmodal">
    <div class="modal-dialog">
        <div class="modal-content"> 

            <div class="modal-body text-center p-3 pt-4 pb-5">

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h6 class="print-error-msg"></h6>
                    </div>
                    <div class="col-md-12 mt-4">
                        <a href="<?php echo base_url('login') ?>">Reload
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="pleasemodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center p-3 pt-4 pb-5">

                <div class="row">
                    <div class="col-md-12">
                        <h6>Please enter email address</h6>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="justify-content-start text-left">
                    <img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo"  />
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-right pt-5">
                <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm"  style="height:18px" /></a><br>
                <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm"  style="height:18px" /></a>
                <!-- competition -->
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm"  style="height:18px" /></a>  </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2"  style="height:30px" /></a>
					<!-- sub menu -->
					<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid "  style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid "  style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutvw" style="display: none;"href="<?php echo base_url('about-competition') ?>#virtual-workshop"><img src="<?php echo base_url(IMGS . 'vw-menu.png') ?>" class="img-fluid "  style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid "  style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutdg" style="display: none;"href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid "  style="height:16px" /></a></li>
					<li class="list-group-item"><a id="abouttem" style="display: none;"href="<?php echo base_url(DOCS . 'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>"><img src="<?php echo base_url(IMGS . 'dt-menu.png') ?>" class="img-fluid "  style="height:16px" /></a></li>
					<li class="list-group-item"><a id="abouttoc" style="display: none;"href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid "  style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutuplo" style="display: none;"href="<?php echo base_url('upload') ?>"><img src="<?php echo base_url(IMGS . 'upload-blue.svg') ?>" class="img-fluid "  style="height:18px" /></a></li>
					</ul>
					<!-- sub menu -->
				<!-- competition -->
				<!-- join -->
				<?php
					if (!empty($this->data['isuser'])) {
						echo '<a href="' . base_url('signout') . '"><img src="' . base_url(IMGS . "logout.png") . '"class="img-fluid px-2"  style="height:18px" /></a>';
					} else {
						echo '<a id="join" href="javascript:void(0);" onclick="join()"><img src="' . base_url(IMGS . 'joincolor.png') . '" class="img-fluid px-2 mbm"  style="height:30px" /></a><br>';
					}
				?>
				
				<a id="btnlogmenu"  href="<?php echo base_url('login') ?>"><img src="<?php echo base_url(IMGS . 'LOGIN.png') ?>" class="img-fluid px-2 mbm"  style="height:16px" /></a><br>
				<a id="btnregmenu"  href="<?php echo base_url('register') ?>"><img src="<?php echo base_url(IMGS . 'REGISTER.png') ?>" class="img-fluid px-2 mbm"  style="height:16px" /></a>
				<!-- join -->
            </div>

        </div>
    </div>
</div>

<!-- HEADER -->
<div class="container-fluid header pt-4 px-0">
    <div class="container">
        <div class="row text-center">

            <div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
                <button type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid"  /></button>
            </div>

            <div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
                <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3"  style="height:18px" /></a>

                <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2"  style="height:18px" /></a>
                <a href="<?php echo base_url('competition') ?>"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2"  style="height:18px" /></a>
                <a href="#"><img src="<?php echo base_url(IMGS . 'join-blue.png') ?>" onmouseover="hover_join(this);" class="img-fluid px-2"  style="height:18px" /></a>
            </div>

            <!-- hover -->
            <div class="hov" style="position:relative;display: none; z-index: 1">
                <div class="hov-menu d-none d-md-block">
                    <ul class="list-group text-right  d-flex justify-content-end">
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid "  style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0"  style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#virtual-workshop"><img src="<?php echo base_url(IMGS . 'vw-menu.png') ?>" class="img-fluid pr-0"  style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0"  style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid pr-0"  style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url(DOCS . 'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>"><img src="<?php echo base_url(IMGS . 'dt-menu.png') ?>" class="img-fluid pr-0"  style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0"  style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url('upload') ?>"><img src="<?php echo base_url(IMGS . 'upload-blue.svg') ?>" class="img-fluid pr-0"  style="height:15px" /></a></li>
                    </ul>
                </div>
            </div>
            <!-- join -->
            <div class="hov-1" style="position:relative; display: none; z-index: 1;">
                <div class="hov-join d-none d-md-block">
                    <ul class="list-group text-right  d-flex justify-content-end">
                        <li class="list-group-item"><img src="<?php echo base_url(IMGS . 'log-active.svg') ?>" class="img-fluid mb-2"  style="height:25px" /></li>
                        <li class="list-group-item pl-5">
                            <a href="<?php echo base_url('register') ?>"><img src="<?php echo base_url(IMGS . 'REGISTER.svg') ?>" onmouseover="hover_regis_new(this);" onmouseout="unhover_regis_new(this);" class="img-fluid pr-0"  style="height:15px" /></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 mb-3 head-title-log" style="z-index: -1">
                <!-- login -->
                <div class="d-none d-md-block">
                    <div class="mb-3 title-log wow flipInY animated">
                        <img src="<?php echo base_url(IMGS . 'log-title.svg') ?>" class="img-fluid"  style="width: 400px;" data-aos="zoom-in">
                    </div>
                    <div class="mb-3 title-forget wow flipInY animated" style="display: none">
                        <img src="<?php echo base_url(IMGS . 'forget.png') ?>" class="img-fluid"  style="width: 400px;" data-aos="zoom-in">
                    </div>
                </div>
                <!-- login -->
            </div>

            <div class="col-12 mb-3 d-md-none d-block">
                <div class="mb-2 title-log wow flipInY animated">
                    <img src="<?php echo base_url(IMGS . 'log-title.svg') ?>" class="img-fluid"  style="width: 250px;" data-aos="zoom-in">
                </div>
                <div class="mb-3 title-forget wow flipInY animated" style="display: none">
                    <img src="<?php echo base_url(IMGS . 'forget.png') ?>" class="img-fluid"  style="width: 250px;" data-aos="zoom-in">
                </div>
            </div>
        </div>
    </div>
    <div style="position:relative">

        <div class="d-none d-md-block man-log">
            <img src="<?php echo base_url(IMAGES . 'man-reg.png') ?>"  class="manvec img-fluid" style="width:250px">
        </div>

        <div class="d-none d-md-block bg-log">
            <img src="<?php echo base_url(IMGS . 'bg-forget-1.png') ?>"  class="bg-for img-fluid" style="width:450px; z-index-1; display: none;">
        </div>

        <div class="d-none d-md-block bg-2-log">
            <img src="<?php echo base_url(IMGS . 'brush-bg.png') ?>"  class="bg-for" style="width:380px; display: none;">
        </div>
        <div class="d-none d-md-block col-log">
            <img src="<?php echo base_url(IMAGES . 'color.png') ?>"  class="manvec" style="width:380px">
        </div>
        <div style="position:absolute;right:0px;bottom:-34rem;z-index:-1 " class="d-block d-md-none">
            <img src="<?php echo base_url(IMAGES . 'color.png') ?>"  style="width:180px">
        </div>

    </div>
</div>

<div class="login-card wow form-log flipInY animated container-fluid" data-aos="zoom-out">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8  mb-5">
                <div class="card card-reg">
                    <div class="card-body card-reg d-flex justify-content-center">
                        <div class="row card-reg">

                            <form method="POST" class="form-signin form-horizontal" role="form" onsubmit="return false;">

                                <div class="form-group">
                                    <label>EMAIL
                                    </label>
                                    <input type="email" name="email" placeholder="Email " required="" autofocus="" class="form-control">

                                </div>
                                <div class="form-group">
                                    <label>PASSWORD</label>
                                    <input type="password" name="password" placeholder="Password " required="" autofocus="" class="form-control">
                                </div>

                                <div class="form-check text-right">
                                    <a id="link-forgot" href="javascript:void(0)" style="color: #828282;">Forgot Password ?</a>
                                </div>
                                <br>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn-primary-outline">
                                        <img src="<?php echo base_url(IMGS . 'login-blue-form.svg') ?>" class="img-fluid">
                                    </button>
                                    <div style="margin-top:10px" class="resultlogin"></div>
                                    <p class="text-center  mt-3" style="color: #828282;">Or don’t have an account?</p>
                                    <a href="<?php echo base_url('register') ?>" class="btn-primary-outline">
                                        <img src="<?php echo base_url(IMGS . 'reg-white-form.svg') ?>" class="img-fluid">
                                    </a>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>
<div class="login-card form-for wow flipInY animated container-fluid" data-aos="zoom-out" style="display: none; z-index:1;">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8  mb-5">
                <div class="card card-reg">
                    <div class="card-body card-reg d-flex justify-content-center">
                        <div class="row card-reg">


                            <form class="form-forgot form-horizontal " id="passresetfrm" onsubmit="return false;">

                                <div class="resultreset"></div>
                                <div class="input-group text-center">
                                    <input type="email" id="resetemail" name="email" placeholder="Email" class="form-control">
                                </div>
                                <br>
                                <!-- <div class="form-group text-center">
                                    <button id="btn-forgot" type="button" class="resetbtn btn-primary-outline ladda-button">
                                        <img src="<?php echo base_url(IMGS . 'send-mail.svg') ?>" class="img-fluid">
                                    </button>
                                </div> -->
                                <div class="form-group text-center">
                                    <button id="btn-forgot" type="button" class="resetbtn btn-primary-outline ladda-button">
                                        <img src="<?php echo base_url(IMGS . 'send-mail.svg') ?>" class="img-fluid">
                                    </button>
                                    <p class="text-center py-2 mt-3" style="color: #828282;">Or</p>
                                    <button id="btn-back" type="button" class="btn-primary-outline">
                                        <img src="assets/img-phase-2/log-btn.svg" class="img-fluid">
                                        </a>
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div class="container-fluid footer py-2" style="margin-top: 0px !important;">
    <div class="container">
        <div class="row">

            <div class="col-4 col-md-6 text-left">
                <img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid"  />
            </div>

            <div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
                <img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid"  />
            </div>

        </div>
    </div>
</div>