<!DOCTYPE html>
<html lang="en">
 
<head>
    <title><?php echo WEB_TITLE." - ".$page_title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="Please join the most innovative, modern and high-tech product in bringing out your greatest potential and bringing a CHANGE to our world!" />
    <meta name="keywords" content="esse, esse change, esse makechange, makechange, esse make change, make change" />
    <meta name="author" content="Esse" />

    <meta name="og:title" content="<?php echo WEB_TITLE." - ".$page_title ?>"/>
    <meta name="og:type" content="event"/>
    <meta name="og:url" content="<?php echo current_url() ?>"/>
    <meta name="og:image" content="<?php echo base_url(IMG."main-banner.png")  ?>"/>
    <meta name="og:site_name" content="Esse Make Change"/>
    <meta name="og:description" content="Please join the most innovative, modern and high-tech product in bringing out your greatest potential and bringing a CHANGE to our world!"/>
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?php echo base_url(IMG.'/logo.png') ?>" type="image/png">
    <!-- prism css -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/dashboard/assets/css/plugins/prism-coy.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/dashboard/assets/css/plugins/lightbox.min.css">
    <!-- CSS PLUGIN -->
    <link rel="stylesheet" href="<?= base_url();?>assets/dashboard/assets/css/plugins/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets/dashboard/assets/css/style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
    <script src="<?= base_url(); ?>assets/dashboard/assets/js/ripple.js"></script>
    <script src="<?= base_url(); ?>assets/dashboard/assets/js/pcoded.min.js"></script>
    <style>
        :root {
        --avatar-size: 3rem;
        }

        .circle {
        background-color: #ccc;
        border-radius: 50%;
        height: var(--avatar-size);
        text-align: center;
        width: var(--avatar-size);
        margin-left: 65px;
        margin-bottom: 5px;
        }
        .initials {
        font-size: calc(var(--avatar-size) / 2); 
        line-height: 1;
        position: relative;
        top: calc(var(--avatar-size) / 4);
        }

        

        .zoom:hover {
        transform: scale(1.5); 
        }
    </style>
</head>

<body class="">
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <?= $_sidebar; ?>
    <?= $_header; ?>
    <?= $_content; ?> 
    <script src="<?= base_url(); ?>assets/dashboard/assets/js/vendor-all.min.js"></script>
    <script src="<?= base_url(); ?>assets/dashboard/assets/js/plugins/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/dashboard/assets/js/plugins/lightbox.min.js"></script>
    <script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true
        })
    </script>

</body>

</html>