<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Home';
$route['brandstory'] = 'Story';
$route['about-competition'] = 'Competition';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['signout'] = 'home/logout';

// dashboard
$route['dashboard'] = 'backend/dashboard';
$route['dashboard/participants'] = 'backend/participants';
$route['dashboard/submission'] = 'backend/submission';
$route['dashboard/login'] = 'backend/dashboard/login';
$route['logout'] = 'backend/dashboard/logout';
