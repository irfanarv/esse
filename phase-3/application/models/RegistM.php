<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistM extends CI_Model {

    var $table = 'participans';
	var $column_order = array('name','email','idcard','city');
	var $column_search = array('name','email','idcard','city');
    var $order = array('id' => 'desc');

    

    private function _get_datatables_query()
	{
		$this->db->from($this->table);
		$i = 0;
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }

	private function _get_datatables_query2()
	{
		$this->db->from($this->table);
		$this->db->where('category','GENERAL');
		$i = 0;
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }

	private function _get_datatables_query3()
	{
		$this->db->from($this->table);
		$this->db->where('category','STUDENTS');
		$i = 0;
		foreach ($this->column_search as $item)
		{
			if($_POST['search']['value'])
			{
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if(count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }
    
    function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
    }

	function get_datatables2()
	{
		$this->_get_datatables_query2();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
    }

	function get_datatables3()
	{
		$this->_get_datatables_query3();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
    }
	
	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered2()
	{
		$this->_get_datatables_query2();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered3()
	{
		$this->_get_datatables_query2();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
    }

	public function count_all2()
	{
		$this->db->from($this->table);
		$this->db->where('category','GENERAL');
		return $this->db->count_all_results();
    }

	public function count_all3()
	{
		$this->db->from($this->table);
		$this->db->where('category','STUDENTS');
		return $this->db->count_all_results();
    }
    
    public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	function simpan($name,$password,$idcard,$city,$email,$dob_day,$dob_month,$dob_year,$category,$image){
        $data = array(
			'name' => $name,
			'image' => $image,
			'password' => password_hash($password, PASSWORD_BCRYPT),
			'idcard' => $idcard,
			'city' => $city,
			'email' => $email,
			'dob_day' => $dob_day,
			'dob_month' => $dob_month,
			'dob_year' => $dob_year,
			'category' => $category,
				
            );  
        $result= $this->db->insert($this->table, $data);
        return $result;
    }

	function totalStudent(){
        $query=$this->db->query("SELECT category FROM participans WHERE category = 'STUDENTS'");
        return $query->num_rows();
    }

	function todayStudent(){
        $query=$this->db->query("SELECT category,date_register FROM participans WHERE category = 'STUDENTS' AND DATE_FORMAT(date_register,'%d%m%y')=DATE_FORMAT(CURDATE(),'%d%m%y') ");
        return $query->num_rows();
    }

    function totalGeneral(){
        $query=$this->db->query("SELECT category FROM participans WHERE category = 'GENERAL'");
        return $query->num_rows();
    }

	function todayGeneral(){
        $query=$this->db->query("SELECT category,date_register FROM participans WHERE category = 'GENERAL' AND DATE_FORMAT(date_register,'%d%m%y')=DATE_FORMAT(CURDATE(),'%d%m%y') ");
        return $query->num_rows();
    }

	public function get_by_id($id)
	{
		$this->db->select("*");
        $this->db->from("participans");
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->result();
	}
     

    
    
}