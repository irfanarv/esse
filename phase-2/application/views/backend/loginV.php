<!DOCTYPE html>
<html lang="en">

<head>

	<title><?php echo $pagetitle;?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="shortcut icon" href="<?php echo base_url(IMG.'/logo.png') ?>" type="image/png">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/dashboard/assets/css/style.css">
	<link href="<?php echo base_url(); ?>assets/dashboard/login/ladda-themeless.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/dashboard/include/icheck/square/grey.css" rel="stylesheet">

	<script src="<?php echo base_url(); ?>assets/dashboard/js/jquery-1.11.2.js"></script>
    <script src="<?php echo base_url(); ?>assets/dashboard/login/spin.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dashboard/login/ladda.min.js"></script>
	<script>
    $(function() {
      Login.init()
    }); 
  </script>
  <script type="text/javascript">
    $(function () {
    $(".form-signin").on('submit',function(){
    $(".resultlogin").html("<div class='alert alert-inverse loading '>Hold On...</div>");
    $.post("<?php echo base_url().$this->uri->segment(1);?>/login",$(".form-signin").serialize(), function(response){
    var resp = $.parseJSON(response);
    console.log(resp);
    if(!resp.status){
    $(".resultlogin").html("<div class='alert alert-danger loading '>"+resp.msg+"</div>");
    }else{
    $(".resultlogin").html("<div class='alert alert-success login '>Redirecting Please Wait...</div>");
    window.location.replace(resp.url);
    }
    }); });
    
     });
  </script>

</head>

<div class="auth-wrapper align-items-stretch aut-bg-img">
	<div class="flex-grow-1">
		
		<div class="auth-side-form">
			<div class=" auth-content">
				<h3 class="mb-4 f-w-400 mt-4">Login</h3>
					<form method="POST" class="form-signin form-horizontal" role="form" onsubmit="return false;">
						<div class="form-group fill">
							<label>Email address</label>
							<input type="text" name="email" placeholder=" " required="" autofocus="" class="form-control">
							
						</div>
						<div class="form-group fill">
							<label for="Password">Password</label>
							<input type="password" name="password" placeholder=" " required="" autofocus="" class="form-control">
						</div>
						
						<button  type="submit" class="btn btn-primary btn-block ladda-button" data-style="zoom-in">Login</button>
						<div style="margin-top:10px" class="resultlogin"></div>
					</form>
			</div>
		</div>
	</div>
</div>


</body>

</html>
<script src="<?php echo base_url(); ?>assets/dashboard/js/login.js"></script>
<script src="<?php echo base_url(); ?>assets/dashboard/include/icheck/icheck.min.js"></script>
<script src="<?= base_url(); ?>assets/dashboard/assets/js/plugins/bootstrap.min.js"></script>

<script>
    Ladda.bind( 'div:not(.progress-demo) button', { timeout: 2000 } );
    Ladda.bind( '.progress-demo button', {
    	callback: function( instance ) {
    		var progress = 0;
    		var interval = setInterval( function() {
    			progress = Math.min( progress + Math.random() * 0.1, 1 );
    			instance.setProgress( progress );
    			if( progress === 1 ) {
    				instance.stop();
    				clearInterval( interval );
    			}
    		}, 200 );
    	}
    } ); 
  </script>

<script>
    var cb, optionSet1;
        $(".checkbox").iCheck({
          checkboxClass: "icheckbox_square-grey",
          radioClass: "iradio_square-grey"
        });

        $(".radio").iCheck({
          checkboxClass: "icheckbox_square-grey",
          radioClass: "iradio_square-grey"
        });
  </script>