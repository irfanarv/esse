<nav class="pcoded-navbar menu-light ">
	<div class="navbar-wrapper  ">
		<div class="navbar-content scroll-div " >
			
			<div class="">
				<div class="main-menu-header">
					<div class="circle"> 
						<span class="initials">
						<?php
							// $nama = $this->session->userdata('nama');
							// $pn = explode(' ' , $nama);
							// foreach($pn as $i =>$inisial) {
							// $i > 0;
							// echo ($inisial),0,1;
							// }
						?>
						</span>
					</div>
					<div class="user-details">
						<div><?php echo $this->session->userdata('nama'); ?></div>
					</div>
				</div>
			</div>
			
			<ul class="nav pcoded-inner-navbar ">
				<li class="nav-item <?php if($this->uri->segment(2)==""){echo 'active';}?>">
					<a href="<?= base_url('dashboard'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
				</li>
				<li class="nav-item <?php if($this->uri->segment(2)=="participants"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/participants'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-users"></i></span><span class="pcoded-mtext">Participants</span></a>
				</li>
				<li class="nav-item <?php if($this->uri->segment(2)=="submission"){echo 'active';}?>">
					<a href="<?= base_url('dashboard/submission'); ?>" class="nav-link "><span class="pcoded-micon"><i class="feather icon-award"></i></span><span class="pcoded-mtext">Design Submission</span></a>
				</li>
			</ul>
			
		</div>
	</div>
</nav>