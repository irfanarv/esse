<?php
$this->data['isuser'] = $this->session->userdata('id');
?>

<style>
    .modal-dialog {
        min-height: calc(100vh - 60px);
        display: flex;
        flex-direction: column;
        justify-content: center;
    }

    @media(max-width: 768px) {
        .modal-dialog {
            min-height: calc(100vh - 20px);
        }
    }
</style>
<script>
    function hover_logout(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
    }

    function unhover_logout(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
    }
    // about competition

    function hover_about_com(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>'
        );
    }

    function unhover_about_com(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMGS . 'about-competition.svg') ?>'
        );
    }
    // home
    function hover(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMAGES . 'home-hov.png') ?>'
        );
    }

    function unhover(element) {
        element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
    }
    // brand
    function hover_brand(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMAGES . 'brand-hov.png') ?>'
        );
    }

    function unhover_brand(element) {
        element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
    }
    // competition
    function hover_com(element) {
        element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
        $('.hov').show();
        $('.hov-1').hide();
    }
    $(document).ready(function() {
        $('.hov').mouseleave(function() {
            $('.hov').hide();
        });
    });

    // join
    function hover_join(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png ') ?>');
        $('.hov-1').show();
        $('.hov').hide();
    }

    $(document).ready(function() {
        $('.hov-1').mouseleave(function() {
            $('.hov-1').hide();
        });
    });

    function unhover_join(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png ') ?>');
    }

    // login

    function hover_login_new(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN-1.svg ') ?>');
    }

    function unhover_login_new(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN.svg') ?>');
    }

    // register

    function hover_regis_new(element) {
        element.setAttribute(
            'src',
            '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>'
        );
    }

    function unhover_regis_new(element) {
        element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER.svg') ?>');
    }

    $(document).ready(function() {
        AOS.init();
    });
</script>

<?php if ($this->session->flashdata('msg') == 'sukses') : ?>
    <script>
        $(window).on('load', function() {
            $('#successModal').modal('show');
        });
    </script> 
<?php elseif ($this->session->flashdata('msg') == 'gagal') : ?>
    <script>
        $(window).on('load', function() {
            $('#failedmodal').modal('show');
        });
    </script>
<?php else : ?>
<?php endif; ?>
<!-- menu mobile -->
<script>
    // competition
    function com() {
        $("#com").hide();
        $("#commob").show();
        $("#ul").show();
        $("#aboutsub").show();
        $("#aboutpr").show();
        $("#aboutvw").show();
        $("#aboutjr").show();
        $("#aboutdg").show();
        $("#abouttem").show();
        $("#abouttoc").show();
        $("#aboutuplo").show();
    }

    function comcolor() {
        $("#commob").hide();
        $("#ul").hide();
        $("#aboutsub").hide();
        $("#aboutpr").hide();
        $("#aboutvw").hide();
        $("#aboutjr").hide();
        $("#aboutdg").hide();
        $("#abouttem").hide();
        $("#abouttoc").hide();
        $("#aboutuplo").hide();
        $("#com").show();
    }
</script>

<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="justify-content-start text-left">
                    <img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-right pt-5">
                <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" style="height:18px" /></a><br>
                <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" style="height:18px" /></a>
                <!-- competition -->
                <a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
                <a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
                <!-- sub menu -->
                <ul id="ul" style="display: none; margin-bottom:0px;">
                    <li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
                    <li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
                    
                    <li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
                    <li class="list-group-item"><a id="aboutdg" style="display: none;" href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
                    <li class="list-group-item"><a id="abouttem" style="display: none;" href="<?php echo base_url(DOCS . 'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>"><img src="<?php echo base_url(IMGS . 'dt-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
                    <li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
                    <li class="list-group-item"><a id="aboutuplo" style="display: none;" href="<?php echo base_url('upload') ?>"><img src="<?php echo base_url(IMGS . 'upload-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
                </ul>
                <!-- sub menu -->
                <!-- competition -->
                <!-- join -->
                <?php
                if (!empty($this->data['isuser'])) {
                    echo '<a href="' . base_url('signout') . '"><img src="' . base_url(IMGS . "logout.png") . '"class="img-fluid px-2"  style="height:18px" /></a>';
                } else {
                    echo '<a id="join" href="javascript:void(0);" onclick="join()"><img src="' . base_url(IMGS . 'joincolor.png') . '" class="img-fluid mbm"  style="height:30px" /></a><br>';
                }
                ?>

                <!-- join -->
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="successModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body text-center p-3 pt-4 pb-5">

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h6>Thank for your contribution</h6>
                    </div>
                    <div class="col-md-12 mt-4">
                        <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMAGES . 'back-home.png') ?>" class="img-fluid px-3 mbm" />
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="failedmodal">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body text-center p-3 pt-4 pb-5">

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h6>Opps... Upload Failed please try again</h6>
                    </div>
                    <div class="col-md-12 mt-4">
                        <a href="<?php echo base_url('upload') ?>">Reload
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- <div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="justify-content-start text-left">
                    <img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo"  />
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-right pt-5">
                <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm"  style="height:18px" /></a><br>
                <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm"  style="height:18px" /></a>
                <a href="<?php echo base_url('competition') ?>"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm"  style="height:18px" /></a>
                <img src="<?php echo base_url(IMAGES . 'man 1.png') ?>"  style="position:absolute; width:193px;left:10px;bottom:0">
                <img src="<?php echo base_url(IMAGES . 'brush-nav.png') ?>"  style="position:absolute; right:0;top:220px">
            </div>

        </div>
    </div>
</div> -->

<!-- HEADER -->
<div class="container-fluid header pt-4 px-0">
    <div class="container">
        <div class="row text-center">

            <div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
                <button type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
            </div>

            <!-- menu -->
            <div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
                <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" style="height:18px" /></a>
                <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" style="height:18px" /></a>
                <a href="#"><img src="<?php echo base_url(IMAGES . 'com-hov.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>
                <?php
                if (!empty($this->data['isuser'])) {
                    echo '<a href="' . base_url('signout') . '"><img src="' . base_url(IMGS . "logout.png") . '" onmouseover="hover_logout(this);" onmouseout="unhover_logout(this);" class="img-fluid px-2"  style="height:18px" /></a>';
                } else {
                    echo '<a href="#"><img src="' . base_url(IMGS . 'join-menu.png') . '" onmouseover="hover_join(this);" onmouseout="unhover_join(this);" class="img-fluid px-2"  style="height:18px" /></a>';
                }
                ?>
            </div>

            <!-- hover -->
            <div class="hov" style="position:relative;display: none;">
                <div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
                    <ul class="list-group text-right  d-flex justify-content-end">
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
                        
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url(DOCS . 'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>"><img src="<?php echo base_url(IMGS . 'dt-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
                        <li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>

                        <li class="list-group-item">
                            <a href="<?php echo base_url('upload') ?>"><img src="<?php echo base_url(IMGS . 'upload-active.svg') ?>" class="img-fluid pr-0" style="height:22px" /></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- join -->
            <div class="hov-1" style="position:relative; display: none;">
                <div class="hov-join d-none d-md-block">
                    <ul class="list-group text-right  d-flex justify-content-end">
                        <li class="list-group-item">
                            <a href="<?php echo base_url('login') ?>"><img src="<?php echo base_url(IMGS . 'LOGIN.svg') ?>" onmouseover="hover_login_new(this);" onmouseout="unhover_login_new(this);" class="img-fluid " style="height:15px" /></a>
                        </li>
                        <li class="list-group-item pl-5">
                            <a href="<?php echo base_url('register') ?>"><img src="<?php echo base_url(IMGS . 'REGISTER.svg') ?>" onmouseover="hover_regis_new(this);" onmouseout="unhover_regis_new(this);" class="img-fluid pr-0" style="height:15px" /></a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 mb-5  d-none d-md-block" style="display: none; z-index: -1;">

                <img src="<?php echo base_url(IMGS . 'upload-title.svg') ?>" class="img-fluid" style="width: 250px;" data-aos="zoom-in-up">
            </div>

            <div class="col-12 mb-3 d-md-none d-block">
                <img src="<?php echo base_url(IMGS . 'upload-title.svg') ?>" class="img-fluid" style="width: 200px;" data-aos="zoom-in-up">
            </div>

        </div>
    </div>
    <div style="position:relative">
        <div style="position:absolute;bottom:-468px;z-index:1000" class="d-none d-md-block">
            <img src="<?php echo base_url(IMGS . 'bimur.svg') ?>" class="img-fluid">
        </div>
        <div style="position:absolute;bottom:-400px;" class="d-block d-md-none">
            <img src="<?php echo base_url(IMGS . 'bimur.svg') ?>" class="img-fluid">
        </div>



        <div style="position:absolute;bottom:-70.3rem;z-index:1000" class="d-none d-md-block">
            <img src="<?php echo base_url(IMGS . 'blue.svg') ?>" class="img-fluid">
        </div>

        <div style="position:absolute;right:0px;bottom:-70rem;z-index:999 " class="d-none d-md-block">
            <img src="<?php echo base_url(IMGS . 'purple.svg') ?>">
        </div>

        <div style="position:absolute;right:0px;bottom:-20.3rem;z-index:1000" class="d-none d-md-block">
            <img src="<?php echo base_url(IMGS . 'kuning.svg') ?>" class="img-fluid">
        </div>

        <div style="position:absolute;right:0px;bottom:-51rem;z-index:-1 " class="d-block d-md-none">
            <img src="<?php echo base_url(IMAGES . 'color.png') ?>" style="width:180px">
        </div>

    </div>
</div>

<div class="container-fluid upload-bg" data-aos="zoom-out">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8  mb-5">
                <div class="card card-reg">
                    <div class="card-body card-reg d-flex justify-content-center">
                        <div class="row card-reg pt-5">
                            <img src="<?php echo base_url(IMGS . 'po.png') ?>" class="img-fluid pb-3" style="width: 1000px;">
                            <form action="<?php echo base_url() . 'Upload/save' ?>" method="post" id="myForm" enctype="multipart/form-data">
                                <input type="hidden" name="participan_id" id="participan_id" value="<?php echo $this->data['isuser']; ?>">
                                <div class="form-group">
                                    <label>COMPLETE NAME
                                    </label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" required="required">

                                </div>
                                <div class="form-group">
                                    <label>PHONE NUMBER</label>
                                    <input type="number" name="phone" id="phone" class="form-control" placeholder="08XXXXXXXXXX" required="required">
                                </div>
                                <div class="form-group">
                                    <label>ADDRESS</label>
                                    <input type="text" class="form-control" name="address" id="address" placeholder="Address" required="required">
                                </div>
                                <div class="form-group">
                                    <label>CITY</label>
                                    <input type="text" name="city" id="city" class="form-control" placeholder="City" required="required">
                                </div>
                                <div class="form-group">
                                    <label>PROVINCE</label>
                                    <input type="text" name="province" id="province" class="form-control" placeholder="Province" required="required">
                                </div>
                                <div class="form-group">
                                    <label>ZIP CODE</label>
                                    <input type="number" name="zip" id="zip" class="form-control" placeholder="ZIP Code" required="required">
                                </div>

                                <img src="<?php echo base_url(IMGS . 'submission.svg') ?>" class="img-fluid pb-3" style="width: 1000px;">

                                <div class="form-group">
                                    <label>DESCRIPTION</label>
                                    <input type="text" name="desc" id="desc" class="form-control" placeholder="Describe the ideas behind your design" required="required">

                                </div>

                                <div class="form-group">
                                    <label id="uploadID">DESIGN PREVIEW</label>
                                    <input id="image" type="file" name="image" style="border: 0px solid #ced4da;" class="form-control-file" required="required">

                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="check" id="check" value="aggre" required="required">
                                    <label class="form-check-label" for="check" style="color: #828282;">
                                        I understand the design brief and agree to all the requirements
                                    </label>
                                </div>
                                <br>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn-primary-outline">
                                        <img src="<?php echo base_url(IMGS . 'upload-btn.svg') ?>" class="img-fluid" style="width: 1000px;">
                                    </button>

                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div class="container-fluid footer py-2" style="margin-top: 0px;">
    <div class="container">
        <div class="row">

            <div class="col-4 col-md-6 text-left">
                <img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
            </div>

            <div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
                <img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
            </div>

        </div>
    </div>
</div>