<?php
$this->data['isuser'] = $this->session->userdata('id');
?>
<style>
	div#countdown {
		margin-top: -1rem;
	}

	li {
		display: inline-block;
		font-family: Proud Regular;
		font-size: 1em;
		list-style-type: none;
		padding: 1em;
		text-transform: uppercase;
		color: #0062EB;
	}

	li span {
		display: block;
		font-size: 4.5rem;
	}

	.ig-btn:hover {
		opacity: 0.5;
	}


	@media all and (max-width: 768px) {

		li {
			font-size: 1.125rem;
			padding: .75rem;
		}

		li span {
			font-size: 3.375rem;
		}
	}
</style>
<!-- menu & important -->
<script>
	// in home
	function hovig(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'ig-2.png') ?>');
	}

	function unhovig(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'ignew.png') ?>');
	}

	function hoversignvir(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew2.svg') ?>');
	}

	function unhoversignvir(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew.svg') ?>');
	}

	function hover_about_home(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-2.png') ?>');
	}

	function unhover_about_home(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about.png') ?>');
	}


	function hover_joinreg(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join.png') ?>');
	}

	function unhover_joinreg(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'join.png') ?>');
	}
	// end in home

	// brand
	function hover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
	}



	function unhover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
	}

	function hover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'brand-hov.png') ?>');
	}

	function unhover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
	}
	// competition
	function hover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
		$('.hov').show();
		$('.hov-1').hide();
	}
	$(document).ready(function() {
		$('.hov').mouseleave(function() {
			$('.hov').hide();
		});
	});

	function unhover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'COMPETITION.png') ?>');
	}
	// join
	function hover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
		$('.hov-1').show();
		$('.hov').hide();
	}

	$(document).ready(function() {
		$('.hov-1').mouseleave(function() {
			$('.hov-1').hide();
		});
	});

	function unhover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
	}



	// upload

	function hover_upload(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'upload-blue.svg') ?>');
	}

	function unhover_upload(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'UPLOAD.svg') ?>');
	}


	// login

	function hover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN-1.svg') ?>');
	}

	function unhover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN.svg') ?>');
	}

	// register

	function hover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>');
	}

	function unhover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER.svg') ?>');
	}

	$(document).ready(function() {

		$('.slider').slick({
			dots: true,
			autoplay: true,
			autoplaySpeed: 1000,
			infinite: true,
			speed: 500
		});
	});
</script>
<!-- effect & init count down -->
<script>
	$(document).ready(function() {
		AOS.init();
		$("#banner").mousemove(function(e) {
			parallaxCriteria(e, "#img-banner", -2);
			parallaxCriteria(e, "#btn-reg", 1);
		});

		function parallaxCriteria(e, target, movement) {
			var $this = $("#banner");
			var relX = e.pageX - $this.offset().left;
			var relY = e.pageY - $this.offset().top;

			TweenMax.to(target, 1, {
				x: (relX - $this.width() / 2) / $this.width() * movement,
				y: (relY - $this.height() / 2) / $this.height() * movement
			});
		}

		$("#virtual").mousemove(function(e) {
			parallaxCriteria(e, "#img-virtual-1", -40);
			parallaxCriteria(e, "#img-virtual-2", 20);

		});

		function parallaxCriteria(e, target, movement) {
			var $this = $("#virtual");
			var relX = e.pageX - $this.offset().left;
			var relY = e.pageY - $this.offset().top;

			TweenMax.to(target, 1, {
				x: (relX - $this.width() / 2) / $this.width() * movement,
				y: (relY - $this.height() / 2) / $this.height() * movement
			});
		}


		// (function() {
		// 	const second = 1000,
		// 		minute = second * 60,
		// 		hour = minute * 60,
		// 		day = hour * 24;

		// 	let event = "June 12, 2021 14:00:00",
		// 		countDown = new Date(event).getTime(),
		// 		x = setInterval(function() {

		// 			let now = new Date().getTime(),
		// 				distance = countDown - now;

		// 			document.getElementById("days").innerText = Math.floor(distance / (day)),
		// 				document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
		// 				document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
		// 				document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);


		// 			if (distance < 0) {


		// 				clearInterval(x);
		// 			}

		// 		}, 0)
		// }());
	});
</script>
<!-- menu mobile -->
<script>
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>


<!-- MOBILE MENU -->
<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<img src="<?php echo base_url(IMAGES . 'HOME.png') ?>" class="img-fluid px-3 mbm" style="height:30px" />
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a>
				<!-- competition -->
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
				<!-- sub menu -->
				<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutdg" style="display: none;" href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="abouttem" style="display: none;" href="<?php echo base_url(DOCS . 'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>"><img src="<?php echo base_url(IMGS . 'dt-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutuplo" style="display: none;" href="<?php echo base_url('upload') ?>"><img src="<?php echo base_url(IMGS . 'upload-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
				</ul>
				<!-- sub menu -->
				<!-- competition -->
				<!-- join -->
				<?php
				if (!empty($this->data['isuser'])) {
					echo '<a href="' . base_url('signout') . '"><img src="' . base_url(IMGS . "logout.png") . '"class="img-fluid px-2"  style="height:18px" /></a>';
				} else {
					echo '<a id="join" href="javascript:void(0);" onclick="join()"><img src="' . base_url(IMGS . 'join-menu-mob.png') . '" class="img-fluid px-2"  style="height:18px" /></a>';
				}
				?>
				<a id="joincolor" style="display: none;" href="javascript:void(0);" onclick="joincolor()"><img src="<?php echo base_url(IMGS . 'joincolor.png') ?>" class="img-fluid px-2 mbm" style="height:30px" /></a><br>
				<a id="btnlogmenu" style="display: none;" href="<?php echo base_url('login') ?>"><img src="<?php echo base_url(IMGS . 'LOGIN.png') ?>" class="img-fluid px-2 mbm" style="height:16px" /></a><br>
				<a id="btnregmenu" style="display: none;" href="<?php echo base_url('register') ?>"><img src="<?php echo base_url(IMGS . 'REGISTER.png') ?>" class="img-fluid px-2 mbm" style="height:16px" /></a>
				<!-- join -->
			</div>

		</div>
	</div>
</div>
<!-- HEADER -->

<div style="position:relative;">
	<div class="d-block d-md-none container-fluid burger-home" style="z-index: 1000;">
		<div class="container">
			<div class="row text-center">
				<div class="col-12 text-right d-block d-sm-none">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="position:relative;">
	<div class="d-block d-md-none container-fluid burger-home" style="z-index: 1000;">
		<div class="container">
			<div class="row text-center">
				<div class="col-12 text-right d-block d-sm-none">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- home -->
<div class="home banner-home-new" style="margin-bottom: 0px;">
	<div class="d-none d-md-block">
		<img src="<?php echo base_url(IMGS . 'home-dekstop.gif') ?>" class="img-fluid banner-home wow tada animated" style="z-index: -1 !important;" id="banner" />
		<div style="position:relative">


			<div class="logo-head">
				<img src="<?php echo base_url(IMGS . 'logohead.png') ?>" class="img-fluid" />
			</div>

			<div class="container-fluid head-new pt-4 px-0 " id="header">
				<div class="container">
					<div class="row text-center">
						<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
							<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
						</div>

						<div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
							<img src="<?php echo base_url(IMAGES . 'HOME.png') ?>" class="img-fluid px-3" style="height:30px" />
							<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" style="height:18px" /></a>
							<a href="#"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>
							<?php
							if (!empty($this->data['isuser'])) {
								echo '<a href="' . base_url('signout') . '"><img src="' . base_url(IMGS . "logout.png") . '" onmouseover="hover_logout(this);" onmouseout="unhover_logout(this);" class="img-fluid px-2"  style="height:18px" /></a>';
							} else {
								echo '<a href="#"><img src="' . base_url(IMGS . 'join-menu.png') . '" onmouseover="hover_join(this);" onmouseout="unhover_join(this);" class="img-fluid px-2"  style="height:18px" /></a>';
							}
							?>
						</div>


						<div class="hov" style="position:relative;display: none;">
							<div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
								<ul class="list-group text-right  d-flex justify-content-end">
									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
									
									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url(DOCS . 'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>"><img src="<?php echo base_url(IMGS . 'dt-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url('upload') ?>"><img src="<?php echo base_url(IMGS . 'upload-blue.svg') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
								</ul>
							</div>
						</div>

						<div class="hov-1" style="position:relative; display: none;">
							<div class="hov-join d-none d-md-block">
								<ul class="list-group text-right  d-flex justify-content-end">
									<li class="list-group-item"><a href="<?php echo base_url('login') ?>"><img src="<?php echo base_url(IMGS . 'LOGIN-1.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
									<li class="list-group-item pl-5"><a href="<?php echo base_url('register') ?>"><img src="<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="join-new d-none d-md-block">
				<a href="<?php echo base_url('register') ?>"> <img src="<?php echo base_url(IMAGES . 'join.png') ?>" onmouseover="hover_joinreg(this);" onmouseout="unhover_joinreg(this);" class="img-fluid wow flash animated" id="btn-reg" /></a>
			</div>
		</div>
	</div>


	<div class="d-block d-md-none" style="z-index: -1; width:100%">
		<div style="position:relative">
			<div class="logo-head" style="z-index: 1;">
				<img src="<?php echo base_url(IMGS . 'logohead.png') ?>" class="img-fluid" width="108px;" />
			</div>
		</div>
		<img src="<?php echo base_url(IMGS . 'home-mob-2.gif') ?>" class="ban-mobile wow tada animated" />
	</div>
	<div style="position:relative">
		<div class="join-mob d-block d-md-none">
			<a href="<?php echo base_url('register') ?>"> <img src="<?php echo base_url(IMAGES . 'join.png') ?>" class="img-fluid reg wow swing animated" /></a>
		</div>

	</div>

	<!-- mobile -->
</div>

<!-- about -->
<div class="container-fluid  bg-home-about pb-5">
	<div class="container d-none d-md-block">
		<div class="row  text-center">
			<div class="col-md-6  isi-home">
				<img src="<?php echo base_url(IMGS . 'about-white-2.png') ?>" class="img-fluid mb-3" style="width: 500px;" data-aos="zoom-up" />
				<a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_new">
					<img src="<?php echo base_url(IMGS . 'ignew.png') ?>" onmouseover="hovig(this);" onmouseout="unhovig(this);" class="img-fluid ig-home" data-aos="zoom-in-up" />
				</a>
			</div>
			<div class="col-md-6 isi-home-card">
				<div class="card" style="margin-top:0px !important; margin-bottom:80px; left:0px !important;" data-aos="zoom-in">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container d-block d-md-none">
		<div class="row text-center">
			<div class="col-md-12 mb-3 pl-3 pr-3">
				<div class="slider" data-aos="zoom-in">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'double.jpg') ?>" alt="Double">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'applemint.jpg') ?>" alt="Apple Mint">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'grape.jpg') ?>" alt="Grape">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'juicy.jpg') ?>" alt="Juice">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="container d-block d-md-none">
		<div class="row text-center">
			<div class="col-md-12 pl-4 pr-4">
				<img class="img-fluid" src="<?php echo base_url(IMGS . 'artikel-home-1.png') ?>" data-aos="zoom-in-up">
			</div>
			<div class="col-md-12">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_new">
					<img src="<?php echo base_url(IMGS . 'ig-mob-white-1.png') ?>" class="img-fluid ig-home" data-aos="zoom-in-up" />
				</a>
			</div>
		</div>
	</div>
</div>

<div class="d-md-none d-block" style="position:relative;">
	<div style="position:absolute;bottom:-75px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
</div>

<div class="container-fluid d-block d-md-none bg-home-about pb-5 mt-5">

	<div class="container ">
		<div class="row text-center">
			<div class="col-md-12 mb-3 pl-3 pr-3">
				<div class="card" style="margin-top:50px !important; margin-bottom:50px; left:0px !important;" data-aos="zoom-in">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- join -->
<div class="container-fluid">
	<div class="row text-center banner">
		<div class="col-md-12 mb-3 mt-5 d-none d-md-block">
			<img src="<?php echo base_url(IMGS . 'makechange.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
		</div>

		<div class="container d-md-none d-block">
			<div class="row">
				<div class="col-1"></div>
				<div class="col-10  mb-5">
					<img src="<?php echo base_url(IMGS . 'express the change.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
				</div>
				<div class="col-1"></div>
			</div>
		</div>



		<div class="col-md-4 text-left d-none d-md-block" style="position: relative;">
			<img src="<?php echo base_url(IMG . 'hand.gif') ?>" class="hand-new" style="width:450px " data-aos="zoom-in-up" />
		</div>

		<div class="col-6 offset-3 col-md-4 offset-md-0 mt-0 mt-md-5">
			<img src="<?php echo base_url(IMG . 'change packaging.png') ?>" class="img-fluid" data-aos="zoom-out-up" />
		</div>

		<div class="col-6 col-md-4 text-left d-block d-md-none">
			<img src="<?php echo base_url(IMG . 'hand.gif') ?>" class="hand-new" style="width:250px" data-aos="zoom-in-down" />
		</div>

		<div class="col-6 col-md-4 text-right">
			<img id="img-header-3" src="<?php echo base_url(IMG . 'pack.gif') ?>" class="img-fluid" style="width:330px" data-aos="zoom-in-up" />
		</div>

	</div>
</div>
<!-- absolute 3 icon -->
<div style="position:relative">
	<div style="position:absolute;bottom:-160px;right:0px;z-index:1000" class="d-none d-md-block">
		<img src="<?php echo base_url(IMG . 'yellow bruh.png') ?>" class="img-fluid" style="width:300px">
	</div>

	<div style="position:absolute;bottom:-50px;right:0px;z-index:1000" class="d-block d-md-none">
		<img src="<?php echo base_url(IMG . 'yellow bruh.png') ?>" class="img-fluid" style="width:100px">
	</div>

	<div style="position:absolute;bottom:-1px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>

</div>
<!-- greeting from esse -->
<div class="container-fluid  participant-cat" style="padding-bottom: 5px !important;">
	<div class="container">
		<div class="row text-center">

			<div class="col-md-6">
				<div class="card home-greeting-card" data-aos="zoom-in-up">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row mb-4">
					<img src="<?php echo base_url(IMGS . 'greeting.png') ?>" class="img-fluid mb-3 d-none d-md-block" style="width: 550px;" data-aos="zoom-in-down" />
					<div class="container d-block d-md-none">
						<div class="row">
							<div class="col-1"></div>
							<img src="<?php echo base_url(IMGS . 'greeting.png') ?>" class="img-fluid col-10 mb-3 " style="width: 250px;" data-aos="zoom-in-down" />
							<div class="col-1"></div>
						</div>
					</div>

					<div class="col-md-12" style="z-index:1000">
						<img src="<?php echo base_url(IMGS . 'homegreetmobnew.png') ?>" class="img-fluid mb-3 d-block d-md-none" data-aos="fade-down" />
						<img src="<?php echo base_url(IMGS . 'greathome3.png') ?>" class="img-fluid mb-3 d-none d-md-block" data-aos="fade-down" />
					</div>

					<div class="container d-none d-md-block">
						<div class="row">
							<div class="col-3"></div>
							<div class="col-9">
								<a href="<?php echo base_url('about-competition') ?>">
									<img src="<?php echo base_url(IMGS . 'about.png') ?>" onmouseover="hover_about_home(this);" onmouseout="unhover_about_home(this);" class="img-fluid d-none d-md-block" style="width: 310px;" data-aos="fade-up" />
								</a>
							</div>

						</div>
					</div>

					<div class="container d-block d-md-none">
						<div class="row">
							<div class="col-1"></div>
							<div class="col-10">
								<a href="<?php echo base_url('about-competition') ?>">
									<img src="<?php echo base_url(IMGS . 'about.png') ?>" onmouseover="hover_about_home(this);" onmouseout="unhover_about_home(this);" class="img-fluid d-block d-md-none" data-aos="fade-up" />
								</a>
							</div>
							<div class="col-1"></div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
	<div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom-home">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
	<div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile-new">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
</div>


<div class="container-fluid ig-story d-none d-md-block" style="padding-top: 10rem !important;">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid"  style="width:350px" /></a>
			</div>

		</div>
	</div>
</div>

<div class="container-fluid ig-story d-block d-md-none" style="padding-top: 5rem !important;">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid"  style="width:350px" /></a>
			</div>

		</div>
	</div>
</div>
<!-- virtual workshop -->
<!-- <div class="container-fluid  important-dates vw-home">
	<div class="container">
		<div class="row text-center">

			<div class="col-md-12 mb-5 pb-3 d-none d-md-block">
				<img src="<?php echo base_url(IMGS . 'virtuall.svg') ?>" class="img-fluid" data-aos="fade-down" />
			</div>

			<div class="col-md-12 d-block d-md-none">
				<img src="<?php echo base_url(IMGS . 'virtuall.svg') ?>" class="img-fluid" data-aos="fade-down" />
			</div>

			<div class="col-md-12 mt-5">
				<div class="row">
					<div id="virtual" class=" col-md-6 mb-4">
						<img src="<?php echo base_url(IMGS . 'bg-vw.svg') ?>" class="img-fluid" />
						<div style="position: relative;">
							<img id="img-virtual-1" src="<?php echo base_url(IMGS . 'vitual3.svg') ?>" class="img-fluid" data-aos="fade-down" />
							<img id="img-virtual-2" src="<?php echo base_url(IMGS . 'vitual2.svg') ?>" class="img-fluid" data-aos="fade-up" />
						</div>
					</div>
					<div class=" col-md-6 mb-4">
						<img src="<?php echo base_url(IMGS . 'title-count.png') ?>" class="img-fluid d-none d-md-block" data-aos="zoom-in" />

						<div class="container d-block d-md-none">
							<div class="row">
								<div class="col-1"></div>
								<div class="col-10">
									<img src="<?php echo base_url(IMGS . 'titlecountmob.png') ?>" class="img-fluid" data-aos="zoom-in" />
								</div>

								<div class="col-1"></div>
							</div>
						</div>
						<div id="countdown mb-3" data-aos="zoom-in">
							<ul class="cont">
								<li><span id="days"></span>days</li>
								<li><span id="hours"></span>Hours</li>
								<li><span id="minutes"></span>Minutes</li>
								<li><span id="seconds"></span>Seconds</li>
							</ul>

						</div>

						<img src="<?php echo base_url(IMGS . 'infonew.svg') ?>" class="img-fluid d-none d-md-block" style="margin-left: 8rem;" data-aos="zoom-in" />
						<div class="container d-block d-md-none">
							<div class="row">
								<div class="col-1"></div>
								<img src="<?php echo base_url(IMGS . 'infomobnew.png') ?>" class="img-fluid col-10 mb-3 " style="width: 250px;" data-aos="zoom-in-down" />

								<div class="col-1"></div>
							</div>
						</div>
						<div class="container d-block d-md-none">
							<div class="row">
								<div class="col-1"></div>

								<a href="http://bit.ly/ESSEmakechange_workshop" target="_new">
									<img src="<?php echo base_url(IMGS . 'signnew.svg') ?>" class="img-fluid col-10" data-aos="zoom-in">
								</a>
								<div class="col-1"></div>
							</div>
						</div>
						<div style="position: relative;" class="d-none d-md-block">
							<a href="http://bit.ly/ESSEmakechange_workshop" target="_new">
								<img src="<?php echo base_url(IMGS . 'signnew.svg') ?>" onmouseover="hoversignvir(this);" onmouseout="unhoversignvir(this);" class="img-fluid sign-in-home" data-aos="zoom-in">
							</a>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>
</div> -->


<!-- important -->

<!-- FOOTER -->
<div class="container-fluid footer footer-home py-1 d-none d-md-block" style="margin-top: 3rem !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>

<div class="container-fluid footer footer-home py-1 d-block d-md-none" style="margin-top: 1.5rem !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>

<!-- footer -->