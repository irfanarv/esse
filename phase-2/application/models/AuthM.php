<?php
defined('BASEPATH') or exit('No direct script access allowed');



class AuthM extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }

  function login_admin($username, $password)
  {
    $this->db->select('id,nama,email,password');
    $this->db->where('email', $username);
    $this->db->where('password', sha1($password));
    $q = $this->db->get('user');
    $user = $q->result();
    $num = $q->num_rows();
    if ($num > 0) {
      $this->session->set_userdata('id', $user[0]->id);
      $this->session->set_userdata('nama', $user[0]->nama);
      return true;
    } else {
      return false;
    }
  }

  public function check_login($email, $password)
  {

    $this->db->where('email', $email);
    $query = $this->db->get('participans');
    $user = $query->result();
    if ($query->num_rows() == 1) {
      $hash = $query->row('password');
      if (password_verify($password, $hash)) {
        $this->session->set_userdata('id', $user[0]->id);
        $this->session->set_userdata('name', $user[0]->name);
        // return $query->result();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public function checkEmail($username)
  {
    $this->db->where('email', $username);
    return $this->db->get('participans')->row_array();
  }


  function reset_password($email, $newpass)
  {
    $this->load->library('email');
    $this->load->config('email');

    $from   = "makechange.id <admin@makechange.id>";
    $_from   = "admin@makechange.id";
    $_me   = "Make Change";

    $subject = 'Make Change | Reset Password';

    $data['email'] = $email;
    $data['newpass'] = $newpass;
    //
    $email_body = $this->load->view('template/email/reset', $data, true);

    $this->email->set_newline("\r\n");
    $this->email->from($from);
    $this->email->to($email);
    $this->email->subject($subject);
    $this->email->message($email_body);
    $this->email->set_mailtype("html");
    $this->email->reply_to($_from, $_me);

    if ($this->email->send()) {
      
    } else {
      show_error($this->email->print_debugger());
    }
  }


  // function reset_password($email, $newpass)
  // {
  //   $this->load->library('phpmailer_lib');
  //   $mail = $this->phpmailer_lib->load();
  //   // $mail->isSMTP();  
  //   // $mail->SMTPOptions = array(
  //   //   'ssl' => array(
  //   //     'verify_peer' => false,
  //   //     'verify_peer_name' => false,
  //   //     'allow_self_signed' => true
  //   //   )
  //   // );
  //   $mail->IsSMTP();
  //   $mail->SMTPDebug = 0;
  //   $mail->Mailer = "ssmtp";
  //   $mail->Host     = 'smtp.gmail.com';
  //   $mail->SMTPAuth = true;
  //   $mail->Username = 'admin@makechange.id';
  //   $mail->Password = 'Rima@1234';
  //   $mail->SMTPSecure = 'ssl';
  //   $mail->Port     = 465;

  //   $mail->setFrom('admin@makechange.id', 'Admin Make Change');

  //   $mail->addAddress($email);
  //   $mail->Subject = 'Your Lost Login Information';
  //   $mail->isHTML(true);

  //   // Email body content
  //   $mailContent = "<h1>Your Lost Login Information</h1>
  //                   <p>Your Mail: {$email}</p>
  //                   <p>Your Password: {$newpass}</p>
  //                   ";
  //   $mail->Body = $mailContent;

  //   // Send email
  //   if (!$mail->send()) {
  //     echo 'Message could not be sent.';
  //     echo 'Mailer Error: ' . $mail->ErrorInfo;
  //   } else {

  //   }
  // }
}
