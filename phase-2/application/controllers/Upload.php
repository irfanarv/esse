<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Upload extends CI_Controller
{

	private $ctrl = "upload";
	private $title = "Upload";
	private $menu = "upload";

	function __construct()
	{
		parent::__construct();
		$this->data['isuser'] = $this->session->userdata('id');
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->model('UploadM');
		$this->load->model('VisitorsM');
		$this->VisitorsM->count_visitor();
	}


	public function index()
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/index.php";
		if ($this->validlogin()) {
			$this->load->view(FRONTEND_LAYOUT, $data);
		} else {
			redirect('login');
		}
	}
 

	function validlogin()
	{

		if (!empty($this->data['isuser'])) {
			return true;
		} else {
			return false;
		}
	}

	public function save()
	{
		$config['upload_path'] = './assets/desain';
		$config['allowed_types'] = 'psd|ai|gif|jpg|png|xlsx|xls|pdf|doc|docx|eps|jpeg|zip|rar';
		$config['max_size'] = 1000000;
		

		$this->upload->initialize($config);

		if (!empty($_FILES['image']['name'])) {
			if ($this->upload->do_upload('image')) {
				$img = $this->upload->data();
				$image				= $img['file_name'];
				$name				= $this->input->post('name', true);
				$phone				= $this->input->post('phone', true);
				$address			= $this->input->post('address', true);
				$city				= $this->input->post('city', true);
				$province			= $this->input->post('province', true);
				$zip				= $this->input->post('zip', true);
				$desc				= $this->input->post('desc', true);
				$participan_id		= $this->input->post('participan_id', true);


				$data = [
					'name' 		=> $name,
					'phone' 	=> $phone,
					'image' 	=> $image,
					'city' 		=> $city,
					'address' 	=> $address,
					'province' 	=> $province,
					'zip' 		=> $zip,
					'desc' 		=> $desc,
					'participan_id' => $participan_id,
				];

				$result = $this->UploadM->save($data);
				echo $this->session->set_flashdata('msg','sukses');
				redirect('upload');
			} else {
				$error = array('error' => $this->upload->display_errors());
				echo $this->session->set_flashdata('msg','gagal');
				redirect('upload');
			}
		}
	}
}
