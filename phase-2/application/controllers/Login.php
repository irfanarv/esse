<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    private $ctrl = "login";
    private $title = "Login";
    private $menu = "login";
    public $data = array();
    public $userid;


    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('RegistM');
        $this->load->model('AuthM');
        $this->load->model('VisitorsM');
        $this->load->helper('string');

        $this->VisitorsM->count_visitor();
    }


    public function index()
    {
        $data['menu'] = $this->menu;
        $data['sub_menu'] = "";
        $data['ctrl'] = $this->ctrl;
        $data['page_title'] = $this->title;
        $data['content_view'] = "$this->ctrl/index.php";
        $this->load->view(FRONTEND_LAYOUT, $data);
    }

    function attemp()
    {

        $username = $this->input->post('email');
        $password = $this->input->post('password');
        if ($this->input->is_ajax_request()) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {
                $errors = validation_errors();
				echo json_encode(['error'=>$errors]);
            } else {
                $data = $this->AuthM->checkEmail($username);
                if($data['email'] == '')
                {
                    echo json_encode(['error'=>'Email not found']);
                }else{
                    if(password_verify($password, $data['password']))
                    {
                        $this->session->set_userdata('id', $data['id']);
                        $this->session->set_userdata('name', $data['name']);
                        echo json_encode(['status'=> true, 'Success']);
                    }
                    else
                    {
                        echo json_encode(['error'=>'Wrong Password']);
                    }
                }
            }
            
        }
    }

    function resetpass() {
        $email = $this->input->post('email'); 
        $this->db->where('email', $email);

        $check = $this->db->get('participans')->num_rows();
        if ($check > 0) {
            $newpass = random_string('alnum', 8);
            $updata = array('password' => password_hash($newpass, PASSWORD_BCRYPT));
            $this->db->where('email', $email);
            $this->db->update('participans', $updata);
            $this->AuthM->reset_password($email, $newpass);
        } 
        echo $check;
    }


}
