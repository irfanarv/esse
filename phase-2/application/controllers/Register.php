<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller 
{
	
	private $ctrl = "register";
	private $title = "Register";
	private $menu = "register";
	
	
	function __construct(){
		parent::__construct();	
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->model('RegistM');
		$this->load->model('VisitorsM');
		$this->VisitorsM->count_visitor(); 
	}

	
	public function index()
	{				
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/index.php";
		$this->load->view(FRONTEND_LAYOUT, $data);
	}

	public function save() 
	{		 
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[participans.email]', array('required' => 'Email Required', 'is_unique' => 'Oops Email already registered','valid_email' => 'Invalid Email'));
			$this->form_validation->set_rules('idcard', 'ID Card', 'required|is_unique[participans.idcard]', array('required' => 'ID Card Required', 'is_unique' => 'Oops ID Card already registered '));

			if ($this->form_validation->run() == FALSE){
				$errors = validation_errors();
				echo json_encode(['error'=>$errors]);
			}else{
				$config['upload_path'] = './assets/ktp';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = 8000;
				$config['encrypt_name'] = TRUE;
		
				$this->upload->initialize($config);

				if(!empty($_FILES['image']['name'])){
					if ($this->upload->do_upload('image')){
						$img = $this->upload->data();
						$image			= $img['file_name'];
						$name			= $this->input->post('name',true); 
						$password		= $this->input->post('password', true);
						$idcard			= $this->input->post('idcard', true);
						$city			= $this->input->post('city',true);
						$email			= $this->input->post('email', true);
						$nohp			= $this->input->post('nohp', true);
						$dob_day		= $this->input->post('dob_day', true);
						$dob_month		= $this->input->post('dob_month', true);
						$dob_year		= $this->input->post('dob_year', true);
						$category		= $this->input->post('cat', true);
						
						$data = [
							'name' => $name,
							'image' => $image,
							'password' => password_hash($password, PASSWORD_BCRYPT),
							'idcard' => $idcard,
							'city' => $city,
							'email' => $email,
							'phone' => $nohp,
							'dob_day' => $dob_day,
							'dob_month' => $dob_month,
							'dob_year' => $dob_year,
							'category' => $category,
						]; 
						
						$this->RegistM->save($data);
						echo json_encode(['success'=>'Record added successfully.']);
					}else{
						$error = array('error' => $this->upload->display_errors());
						echo json_encode(['error'=>$error]); 
					}
				}
			}
	}


	
}
