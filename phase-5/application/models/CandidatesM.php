<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CandidatesM extends CI_Model
{

	var $table = 'submission';
	var $column_order = array('name', 'email', 'idcard', 'category', 'vote_total', 'phone', 'set_win');
	var $column_search = array('name', 'email', 'idcard', 'category', 'vote_total', 'phone', 'set_win');
	var $order = array('vote_total' => 'desc');


	// win
	private function _get_datatables_query()
	{

		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where_not_in('submission.set_win', '');
		$this->db->where_not_in('submission.set_win', NULL);

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_POST['search']['value']) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// general
	private function _get_datatables_query1()
	{
		// $this->db->from($this->table);
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', '');
		$this->db->where_not_in('submission.set_win', NULL);
		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_POST['search']['value']) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	// student
	private function _get_datatables_query2()
	{
		// $this->db->from($this->table);
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('participans', 'participans.id=submission.participants_id');

		$this->db->where('participans.category', 'STUDENTS');

		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_POST['search']['value']) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if (count($this->column_search) - 1 == $i)
					$this->db->group_end();
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	// win
	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	// general
	function get_datatables1()
	{
		$this->_get_datatables_query1();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	// student
	function get_datatables2()
	{
		$this->_get_datatables_query2();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	// win
	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	// gen
	function count_filtered1()
	{
		$this->_get_datatables_query1();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all1()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}
	// student
	function count_filtered2()
	{
		$this->_get_datatables_query2();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all2()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('submission.id_sub', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_idEdit($id)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('submission.id_sub', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function getParticipant()
	{
		$this->db->from('participans');
		$query = $this->db->get();
		return $query->result();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function saveImage($data)
	{
		$this->db->insert('gallery', $data);
		return $this->db->insert_id();
	}

	public function getMockup($id)
	{
		$this->db->from('gallery');
		$this->db->where('submission_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function delete_by_id($id)
	{
		$this->db->where('id_gallery', $id);
		$this->db->delete('gallery');
	}

	public function delete($id)
	{
		$this->db->where('id_sub', $id);
		$this->db->delete($this->table);
	}

	public function dashboard()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where_not_in('submission.set_win', '');
		$this->db->order_by("set_win", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function dashboardGen()
	{
		$this->db->select('participans.name AS nama, COUNT(vote_total) AS jml');
		$this->db->from($this->table);
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', '');
		$this->db->group_by('vote_total');
		$query = $this->db->get();
		return $query->result();
	}


	// frontpage
	public function getWinner()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where_not_in('submission.set_win', '');
		$query = $this->db->get();
		return $query->result();
	}

	public function getStudent()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		// $this->db->where('submission.set_win', '');
		$query = $this->db->get();
		return $query->result();
	}
	public function getGeneral()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', '');
		$this->db->where_not_in('submission.set_win', NULL);
		$query = $this->db->get();
		return $query->result();
	}

	public function getDetail($id)
	{
		$this->db->select('submission.*,participans.*, desain.participan_id,desain.id AS id_desain, desain.desc');
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');

		$this->db->join('desain', 'desain.participan_id=submission.participants_id');
		$this->db->where('uuid_sub', $id);
		$this->db->group_by('submission.participants_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getGallery($id)
	{
		$this->db->select('*');
		$this->db->from('submission');
		$this->db->join('gallery', 'gallery.submission_id=submission.id_sub');
		$this->db->where('uuid_sub', $id);
		$query = $this->db->get();
		return $query->result();
	}

	// vote mecanisme
	// vote win
	// check quota
	function winCount($account)
	{

		$q_email = $this->db->select('vote_win', FALSE)
			->from('account')
			->where('valid_email', $account);
		$tmp_email = $q_email->get()->result();
		$result['count_v'] = $tmp_email[0]->vote_win;
		return $result;
	}
	function genCount($account)
	{

		$q_email = $this->db->select('vote_gen', FALSE)
			->from('account a')
			->where('a.valid_email', $account);
		$tmp_email = $q_email->get()->result();
		$result['count_v'] = $tmp_email[0]->vote_gen;
		return $result;
	}
	function studentCount($account)
	{

		$q_email = $this->db->select('vote_student', FALSE)
			->from('account a')
			->where('a.valid_email', $account);
		$tmp_email = $q_email->get()->result();
		$result['count_v'] = $tmp_email[0]->vote_student;
		return $result;
	}
	// end check qouta
	// vote win
	// update vote quota
	function VoteWin($account)
	{
		$this->db->set('vote_win', 'vote_win+1', FALSE);
		$this->db->where('valid_email', $account);
		$this->db->update('account');
		return $this->db->affected_rows();
	}
	function VoteGen($account)
	{
		$this->db->set('vote_gen', 'vote_gen+1', FALSE);
		$this->db->where('valid_email', $account);
		$this->db->update('account');
		return $this->db->affected_rows();
	}
	function VoteStudent($account)
	{
		$this->db->set('vote_student', 'vote_student+1', FALSE);
		$this->db->where('valid_email', $account);
		$this->db->update('account');
		return $this->db->affected_rows();
	}

	// end update vote quota
	// update total vote
	function updateSub($id_sub)
	{
		$this->db->set('vote_total', 'vote_total+1', FALSE);
		$this->db->where('id_sub', $id_sub);
		$this->db->update('submission');
		return $this->db->affected_rows();
	}
	// end update total vote

	public function saveVote($data)
	{
		$this->db->insert('voters', $data);
		return $this->db->insert_id();
	}

	// function checkVote($account){

	// 	$q_email = $this->db->select('sub_id', FALSE)
	// 		->from('voters a')
	// 		->where('a.account',$account);
	// 		$tmp_email = $q_email->get()->result();
	// 	$result['cek_v'] = $tmp_email[0]->sub_id;
	// 	return $result;
	// }

	function checkVote($account, $id_sub)
	{
		$this->db->where('account', $account);
		$this->db->where('sub_id', $id_sub);
		return $this->db->get('voters')->row_array();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	// winners
	public function get1st()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function get1ststu()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.set_win', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function getGallery1ststu()
	{
		$this->db->select('*');
		$this->db->from('submission');
		$this->db->join('gallery', 'gallery.submission_id=submission.id_sub');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.set_win', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function getGallery1st()
	{
		$this->db->select('*');
		$this->db->from('submission');
		$this->db->join('gallery', 'gallery.submission_id=submission.id_sub');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', 1);
		$query = $this->db->get();
		return $query->result();
	}

	
	// 
	public function get2nd()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', 2);
		$query = $this->db->get();
		return $query->result();
	}

	public function getGallery2nd()
	{
		$this->db->select('*');
		$this->db->from('submission');
		$this->db->join('gallery', 'gallery.submission_id=submission.id_sub');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', 2);
		$query = $this->db->get();
		return $query->result();
	}

	public function get2ndstu()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.set_win', 2);
		$query = $this->db->get();
		return $query->result();
	}

	public function getGallery2ndstu()
	{
		$this->db->select('*');
		$this->db->from('submission');
		$this->db->join('gallery', 'gallery.submission_id=submission.id_sub');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.set_win', 2);
		$query = $this->db->get();
		return $query->result();
	}
	// 
	public function get3rd()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.set_win', 3);
		$query = $this->db->get();
		return $query->result();
	}

	public function getGallery3rd()
	{
		$this->db->select('*');
		$this->db->from('submission');
		$this->db->join('gallery', 'gallery.submission_id=submission.id_sub');
		$this->db->where('submission.set_win', 3);
		
		$query = $this->db->get();
		return $query->result();
	}

	public function get3rdstu()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.set_win', 3);
		$query = $this->db->get();
		return $query->result();
	}

	public function getGallery3rdstu()
	{
		$this->db->select('*');
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->join('gallery', 'gallery.submission_id=submission.id_sub');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.set_win', 3);
		
		$query = $this->db->get();
		return $query->result();
	}
	// choice award
	public function getChoiceAward()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.ca', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function getChoiceAwardstu()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.ca', 1);
		$query = $this->db->get();
		return $query->result();
	}

	public function getSpecialPrize()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where('submission.sp', 1);
		$query = $this->db->get();
		return $query->result();
	}
	public function getSpecialPrizestu()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'STUDENTS');
		$this->db->where('submission.sp', 1);
		$query = $this->db->get();
		return $query->result();
	}
	// video
	public function getVideo()
	{
		$this->db->from('submission');
		$this->db->join('participans', 'participans.id=submission.participants_id');
		$this->db->where('participans.category', 'GENERAL');
		$this->db->where_not_in('submission.video', NULL);
		$this->db->where_not_in('submission.video', '');
		$query = $this->db->get();
		return $query->result();
	} 
}
