<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Submission extends CI_Controller
{
    private $ctrl = "submission";
	private $title = "Submission";
	private $menu = "submission";
	public $data = array();
    public $userid; 
	
	
	function __construct()
	{
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('id');
		$this->load->model('VisitorsM');
        $this->load->model('SubmissionM');
		$this->load->model('AuthM');	
        $this->load->helper('download');
	}

	function index() 
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;

		if ($this->validadmin()) {

			$this->backend->display('backend/submissionV', $data);
		} else {
			$this->data['pagetitle'] = 'Esse - Make Change | Login';
			$this->load->view('backend/loginV', $this->data);
		}
	}

    public function all()
    {
        $list = $this->SubmissionM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $desain) {
            
            $no++; 
            $row = array();
            $row[] = '<a class="btn btn-success has-ripple" href="'.base_url().DESAIN .''.$desain->image.''.'">Download</a>
            <button class="btn btn-light has-ripple" onClick="detail('."'".$desain->id."'".')">Details</button>';
            $row[] = $desain->name;
            $row[] = $desain->category;
            $row[] = $desain->email;
            $row[] = $desain->phone;
            
            $row[] = date('d M Y', strtotime($desain->date_upload)).'';
            $row[] = $desain->desc;
            
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->SubmissionM->count_all(),
                        "recordsFiltered" => $this->SubmissionM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function download($id){
        $datapartisipan = $this->SubmissionM->get_by_id($id);
		
		foreach ($datapartisipan as $dt) {
			$isi = '../assets/desain/'.$dt->image;
            $nama_file = $dt->image;
            force_download($nama_file, $isi);

		}


    }

    public function details($id){
        $datapartisipan = $this->SubmissionM->get_by_id($id);
		$data = array();
		foreach ($datapartisipan as $dt) {
			$row = array();
			if($dt->dob_month == 1){
                $date = "Jan";
            }elseif($dt->dob_month == 2) {
                $date = 'Feb';
            }elseif($dt->dob_month == 3){
				$date = 'Mar';
			}elseif($dt->dob_month == 4){
				$date = 'Apr';
			}elseif($dt->dob_month == 5){
				$date = 'May';
			}elseif($dt->dob_month == 6){
				$date = 'Jun';
			}elseif($dt->dob_month == 7){
				$date = 'Jul';
			}elseif($dt->dob_month == 8){
				$date = 'Aug';
			}elseif($dt->dob_month == 9){
				$date = 'Sept';
			}elseif($dt->dob_month == 10){
				$date = 'Oct';
			}elseif($dt->dob_month == 11){
				$date = 'Nov';
			}elseif($dt->dob_month == 12){
				$date = 'Dec';
			}
            $row[] = $dt->image;
            $row[] = $dt->name;
            $row[] = $dt->desc;
            $row[] = $dt->phone;
            $row[] = $dt->city;
            $row[] = $dt->date_upload;
            $row[] = $dt->dob_day .'  '. $date .'  '. $dt->dob_year;
			$data[] = $row;
		}
        $output = '';
		
                foreach ($datapartisipan as $dt) {
                    $output .= '
                    <div class="row">
                            <div class="col-md-3">
                                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="../assets/ktp/'.$dt->ktp.'" class="d-block w-100">
                                            <br>
                                            <a class="btn btn-success has-ripple" target="_new" href="'.base_url().KTP .''.$dt->ktp.''.'">Download ID Card</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <ul>
                                    <li>Category: '.$dt->category.'</li>
                                    <li>ID Identity: '.$dt->idcard.'</li>
                                    <li>Name: '.$dt->name.'</li>
                                    <li>Email: '.$dt->email.'</li>
                                    <li>Phone: '.$dt->phone.'</li>
                                    <li>Brithday: '.$dt->dob_day .'  '. $date .'  '. $dt->dob_year.'</li>
                                    <li>Address: '.$dt->address.'</li>
                                    <li>City: '.$dt->city.'</li>
                                    <li>Province: '.$dt->province.'</li>
                                    <li>ZIP Code: '.$dt->zip.'</li>
                                    <li>Upload Date: '.date('d M Y', strtotime($dt->date_upload)).'</li>
                                    
                                </ul>
                            </div>
                            <div class="col-lg-5">
                            <h3>Description</h3>
                            <hr>
                            <p>'.$dt->desc.'</p>
                            <hr>
                            <a class="btn btn-success has-ripple" href="'.base_url().DESAIN .''.$dt->image.''.'">Download Design</a>
                            </div>
                            
                        </div>
                    ';
                }
        
        echo $output;
    }


    function validadmin() {

        if (!empty($this->data['isadmin'])) {
            return true;
        } else {
            return false;
        }
    }


    

}