<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
    private $ctrl = "dashboard";
	private $title = "Dashboard";
	private $menu = "dashboard";
	public $data = array();
    public $userid; 
	
	
	function __construct()
	{
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('id');
		$this->load->model('VisitorsM');
        $this->load->model('RegistM');
		$this->load->model('AuthM');
        $this->load->model('CandidatesM');		
	}

	function index() 
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
        $data['visitor'] = $this->VisitorsM->statistik_pengujung();
        $data['monthvisit'] = $this->VisitorsM->visitor_month();
        $data['lastmonthvisit'] = $this->VisitorsM->visitor_last();
        $data['totalvisits'] = $this->VisitorsM->total_visits();
        $data['totalGuide'] = $this->VisitorsM->totalGuide();
        $data['todayGuide'] = $this->VisitorsM->todayGuide();
        $data['totalTemplate'] = $this->VisitorsM->totalTemplate();
        $data['todayTemplate'] = $this->VisitorsM->todayTemplate();
        $data['totalStudent'] = $this->RegistM->totalStudent();
        $data['todayStudent'] = $this->RegistM->todayStudent();
        $data['totalGeneral'] = $this->RegistM->totalGeneral();
        $data['todayGeneral'] = $this->RegistM->todayGeneral();
        $data['visitBrowser'] = $this->VisitorsM->browser();
        $data['winnerVote'] = $this->CandidatesM->dashboard();
        

		if ($this->validadmin()) {

			$this->backend->display('backend/dashboardV', $data);
		} else {
			$this->data['pagetitle'] = 'Esse - Make Change | Login';
			$this->load->view('backend/loginV', $this->data);
		}
	}


    function validadmin() {

        if (!empty($this->data['isadmin'])) {
            return true;
        } else {
            return false;
        }
    }

    function login() {

        $username = $this->input->post('email');
        $password = $this->input->post('password');
        if ($this->input->is_ajax_request()) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == FALSE) {

                $result = array("status" => false, "msg" => validation_errors(), "url" => "");
            } else {
 
                $login = $this->AuthM->login_admin($username, $password);
                if ($login) { 
                    $prevurl = $this->session->userdata('prevURL');
                    if (!empty($prevurl)) {
                        $url = $prevurl; 
                    } else {
                        $url = base_url() . 'dashboard';
                    }

                    $result = array("status" => true, "msg" => "", "url" => $url);
                } else {
                    $result = array("status" => false, "msg" => "Upps ada yang salah...", "url" => "");

                }

            }
            echo json_encode($result);

        }
    }

    public function logout()
	{
	 $this->session->unset_userdata('id');
	 $this->session->unset_userdata('nama');
	 redirect('/dashboard');
	}
	public function countDownload()
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$date  = date("Y-m-d"); 
		$file_type = $this->input->post('file_type');
		$data = array(

			'ip_address' 	=> $ip,
			'date'	        => $date,
			'file_type' 	=> $file_type,

		);
		$result = $this->VisitorsM->addCount($data);
	}

    public function win()
    {
        $list = $this->CandidatesM->get_datatables(); 
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $candidates) {

            $no++; 
            $row = array();
            $row[] = $candidates->set_win;
           
            $row[] = $candidates->name;
            $row[] = $candidates->category;
            $row[] = $candidates->email;
            $row[] = $candidates->phone;
            $row[] = $candidates->idcard;
            $row[] = "$candidates->vote_total Votes";
           
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->CandidatesM->count_all(),
            "recordsFiltered" => $this->CandidatesM->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    public function gen()
    {
        $list = $this->CandidatesM->get_datatables1();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $candidates) {

            $no++;
            $row = array();
            
            $row[] = $candidates->name;
            $row[] = $candidates->category;
            $row[] = $candidates->email;
            $row[] = $candidates->phone;
            $row[] = $candidates->idcard;
            $row[] = "$candidates->vote_total Votes";
            
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->CandidatesM->count_all1(),
            "recordsFiltered" => $this->CandidatesM->count_filtered1(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    // student
    public function student()
    {
        $list = $this->CandidatesM->get_datatables2();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $candidates) {

            $no++;
            $row = array();
            
            $row[] = $candidates->name;
            $row[] = $candidates->category;
            $row[] = $candidates->email;
            $row[] = $candidates->phone;
            $row[] = $candidates->idcard;
            $row[] = "$candidates->vote_total Votes";
            
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->CandidatesM->count_all2(),
            "recordsFiltered" => $this->CandidatesM->count_filtered2(),
            "data" => $data,
        );
        echo json_encode($output);
    }

}