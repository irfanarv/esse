<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stay extends CI_Controller 
{
	
	private $ctrl = "stay";
	private $title = "Stay tuned for the launch";
	private $menu = "stay";
	
	
	function __construct(){
		
		parent::__construct();	
		$this->load->model('VisitorsM');
		$this->VisitorsM->count_visitor(); 
			
			
	}

	
	public function index()
	{				
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/index.php";
		$this->load->view(FRONTEND_LAYOUT, $data);
	}	
	
}
