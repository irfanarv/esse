<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Winner extends CI_Controller 
{
	
	private $ctrl = "winner";
	private $title = "Congratulation for All Esse Make Change Design Competition’s Winners";
	private $menu = "winner";
	
	
	function __construct(){
		parent::__construct();	
		$this->load->model('VisitorsM');
		$this->load->model('CandidatesM');
		$this->VisitorsM->count_visitor(); 
	}

	
	public function index()
	{				

		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		// 1
		$data['data1st'] = $this->CandidatesM->get1st();
		$data['gallery1st'] = $this->CandidatesM->getGallery1st();
		// 2
		$data['data2nd'] = $this->CandidatesM->get2nd();
		$data['gallery2nd'] = $this->CandidatesM->getGallery2nd();
		// 3
		$data['data3rd'] = $this->CandidatesM->get3rd();
		$data['gallery3rd'] = $this->CandidatesM->getGallery3rd();
		// choice award
		$data['dataCA'] = $this->CandidatesM->getChoiceAward();
		// special prize
		$data['dataSP'] = $this->CandidatesM->getSpecialPrize();
		// video win 
		$data['dataVideo'] = $this->CandidatesM->getVideo();
		// 
		$data['content_view'] = "$this->ctrl/index.php";
		$this->load->view(FRONTEND_LAYOUT, $data);
	}
	
	public function general()
	{				

		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		// 1
		$data['data1st'] = $this->CandidatesM->get1st();
		$data['gallery1st'] = $this->CandidatesM->getGallery1st();
		// 2
		$data['data2nd'] = $this->CandidatesM->get2nd();
		$data['gallery2nd'] = $this->CandidatesM->getGallery2nd();
		// 3
		$data['data3rd'] = $this->CandidatesM->get3rd();
		$data['gallery3rd'] = $this->CandidatesM->getGallery3rd();
		// choice award
		$data['dataCA'] = $this->CandidatesM->getChoiceAward();
		// special prize
		$data['dataSP'] = $this->CandidatesM->getSpecialPrize();
		// video win 
		$data['dataVideo'] = $this->CandidatesM->getVideo();
		// 
		$data['content_view'] = "$this->ctrl/general.php";
		$this->load->view(FRONTEND_LAYOUT, $data);
	}

	public function student()
	{				
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		// 1
		$data['data1ststu'] = $this->CandidatesM->get1ststu();
		$data['gallery1ststu'] = $this->CandidatesM->getGallery1ststu();
		// 2
		$data['data2ndstu'] = $this->CandidatesM->get2ndstu();
		$data['gallery2ndstu'] = $this->CandidatesM->getGallery2ndstu();
		// 3
		$data['data3rdstu'] = $this->CandidatesM->get3rdstu();
		$data['gallery3rdstu'] = $this->CandidatesM->getGallery3rdstu();
		// choice award
		$data['dataCAstu'] = $this->CandidatesM->getChoiceAwardstu();
		// special prize
		$data['dataSPstu'] = $this->CandidatesM->getSpecialPrizestu();
		// video win 
		$data['dataVideo'] = $this->CandidatesM->getVideo();
		// 
		$data['content_view'] = "$this->ctrl/student.php";
		$this->load->view(FRONTEND_LAYOUT, $data);
	}
	
}
