<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competition extends CI_Controller 
{
	
	private $ctrl = "competition";
	private $title = "About Competition";
	private $menu = "competition";
	
	
	/**== Construct ==**/
	function __construct(){
		
		parent::__construct();	
		$this->load->model('CandidatesM');
		$this->load->model('VisitorsM');
		$this->VisitorsM->count_visitor(); 
			
	}

	
	/**== Index Page ==**/
	public function index()
	{				
		// Set data
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/index.php";
		$data['datastudent'] = $this->CandidatesM->getStudent();
		$data['datageneral'] = $this->CandidatesM->getGeneral();
		$data['datawinner'] = $this->CandidatesM->getWinner();
		// load view
		$this->load->view(FRONTEND_LAYOUT, $data);
	}	

	public function detail($id)
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/detail.php";
		$data['datacandidate'] = $this->CandidatesM->getDetail($id);
		$data['gallery'] = $this->CandidatesM->getGallery($id);
		$this->load->view(FRONTEND_LAYOUT, $data);
	}
	
}
