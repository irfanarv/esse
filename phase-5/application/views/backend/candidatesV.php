<style>
    .select2-close-mask {
        z-index: 2099;
    }

    .dropzone {
        border: 2px dashed #0087F7;
    }

    .select2-dropdown {
        z-index: 3051;
    }

    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }
</style>
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Hello <?php echo $this->session->userdata('nama'); ?> ,
                                <?php
                                date_default_timezone_set('Asia/Jakarta');
                                $Hour = date('G');
                                if ($Hour >= 5 && $Hour <= 11) {
                                    echo "Selamat Pagi";
                                } else if ($Hour >= 12 && $Hour <= 15) {
                                    echo "Selamat Siang";
                                } else if ($Hour >= 16 && $Hour <= 18) {
                                    echo "Selamat Sore";
                                } else if ($Hour >= 19 || $Hour <= 4) {
                                    echo "Selamat Malam";
                                }
                                ?>
                                👋
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-12">
                <div class="card user-profile-list">
                    <div class="card-header">
                        <div class="float-right">
                            <button class="btn btn-success btn-sm btn-round has-ripple" onclick="addCandidate()"><i class="feather icon-plus-circle"></i> Add Candidate</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active text-uppercase" id="winner-tab" data-toggle="tab" href="#winner" role="tab" aria-controls="winner" aria-selected="true">Winner</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="false">General</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="student-tab" data-toggle="tab" href="#student" role="tab" aria-controls="student" aria-selected="false">Students</a>
							</li>
						</ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="winner" role="tabpanel" aria-labelledby="winner-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_win" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Winner</th>
                                                <th>Thubmnail</th>
                                                <th>Design By</th>
                                                <th>Concept Name</th>
                                                <th>Instagram</th>
                                                <th>#</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>ID Card</th>
                                                <th>City</th>
                                                
                                                <th>Total Votes</th>
                                                <th></th>


                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="general" role="tabpanel" aria-labelledby="general-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_gen" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Thubmnail</th>
                                                <th>Design By</th>
                                                <th>Concept Name</th>
                                                <th>Instagram</th>
                                                <th>#</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>ID Card</th>
                                                <th>City</th>
                                                <th>Total Votes</th>
                                                <th></th>


                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="student" role="tabpanel" aria-labelledby="student-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_student" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Thubmnail</th>
                                                <th>Design By</th>
                                                <th>Concept Name</th>
                                                <th>Instagram</th>
                                                <th>#</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>ID Card</th>
                                                <th>City</th>
                                                <th>Total Votes</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modal-candidate" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id" />
                    <input type="hidden" value="" name="gambar">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="itemName form-control" style="width: 450px;" id="candidate_id" name="candidate_id" required>
                                    <option value=""></option>
                                </select>

                            </div>
                        </div>


                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" style="width: 450px;" id="set_win" name="set_win">
                                    <option>Set Winner</option>
                                    <option value="1">1st</option>
                                    <option value="2">2nd</option>
                                    <option value="3">3rd</option>
                                </select>

                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Concept Name</label>
                                <input type="text" name="concept" class="form-control" id="concept" placeholder=" " >
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Instagram Name</label>
                                <input type="text" name="ig" class="form-control" id="ig" placeholder=" " >
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Post Instagram URI</label>
                                <input type="text" name="ig_uri" class="form-control" id="ig_uri" placeholder=" " >
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group fill">
                                <label class="floating-label" for="Icon">Thumbnail</label><br>
                                <img class="img-fluid" id="modal-preview" src="https://via.placeholder.com/150"><br><br>
                                <div class="upload-btn-wrapper">
                                    <button class="btn btn-secondary btn-sm">Upload file</button>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);">
                                </div>
                                <input type="hidden" name="hidden_image" id="hidden_image">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-dange" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-candidate-edit" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form-update" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id" />
                    <input type="hidden" value="" name="gambar">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Participant</label>
                                <input type="text" name="email" class="form-control" id="email" placeholder=" " required readonly>
                            </div>
                        </div>


                        <!-- <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" style="width: 450px;" id="set_win" name="set_win">
                                    <option>Set Winner</option>
                                    <option value="1">1st</option>
                                    <option value="2">2nd</option>
                                    <option value="3">3rd</option>
                                </select>

                            </div>
                        </div> -->

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Concept Name</label>
                                <input type="text" name="concept" class="form-control" id="concept" placeholder=" " required>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Instagram Name</label>
                                <input type="text" name="ig" class="form-control" id="ig" placeholder=" " required>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Post Instagram URI</label>
                                <input type="text" name="ig_uri" class="form-control" id="ig_uri" placeholder=" " required>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Hastag 1</label>
                                <input type="text" name="tag1" class="form-control" id="tag1" placeholder=" " required>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="floating-label" for="Name">Hastag 2</label>
                                <input type="text" name="tag2" class="form-control" id="tag2" placeholder=" " required>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group fill">
                                <label class="floating-label" for="Icon">Thumbnail</label><br>
                                <img class="img-fluid" id="modal-preview_edit" src="https://via.placeholder.com/150"><br><br>
                                <div class="upload-btn-wrapper">
                                    <button class="btn btn-secondary btn-sm">Upload file</button>
                                    <input id="image" type="file" name="image" accept="image/*" onchange="readURL(this);" >
                                </div>
                                <input type="hidden" name="hidden_image" id="hidden_image_edit">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-dange" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url(); ?>assets/dashboard/assets/js/plugins/select2.full.min.js"></script>


<script type="text/javascript">
    var table_win;
    var table_gen;
    var table_student;
    var save_method;
    $(document).ready(function() {

        table_win = $('#table_win').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('backend/Candidates/win') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

        table_gen = $('#table_gen').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('backend/Candidates/gen') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

        table_student = $('#table_student').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('backend/Candidates/student') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

    });

    $('#form').submit(function(e) {
        var url;
        
            url = "<?php echo site_url('backend/candidates/candidateAdd') ?>";
        notify('Candidate added successfully', 'inverse');
        
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                $('#form')[0].reset();
                $('#modal-candidate').modal('hide');
                reload_table();

            }

        });
    });

    $('#form-update').submit(function(e) {
        var url;
        
        
            url = "<?php echo site_url('backend/candidates/candidateUpdate') ?>";
            notify('Candidate update successfully', 'inverse');
        
        e.preventDefault();
        $.ajax({
            "url": url,
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                $('#form-update')[0].reset();
                $('#modal-candidate-edit').modal('hide');
                reload_table();

            }

        });
    });

    $('.itemName').select2({
        placeholder: 'Select Candidate By Email',
        ajax: {
            url: "<?php echo site_url('backend/candidates/dataParticipant/') ?>",
            dataType: 'json',
            delay: 250,
            processResults: function(data) {
                return {
                    results: data,
                };
            },
            cache: true,
        }
    });

    function addCandidate() {
        save_method = 'add';
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $('.help-block').empty();
        $('#modal-candidate').modal('show');
        $('.modal-title').text('Add Candidate');
    }

    function editCandidate(id)
    {
        save_method = 'update';
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error'); 
        $('.help-block').empty(); 

        
        $.ajax({
            
            url : "<?php echo site_url('backend/candidates/candidateDataEdit') ?>/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id_sub);
                $('[name="email"]').val(data.email);
                $('[name="set_win"]').val(data.set_win);
                $('[name="gambar"]').val(data.thumb);
                $('[name="concept"]').val(data.concept_name);
                $('[name="ig"]').val(data.instagram);
                $('[name="tag1"]').val(data.tag1);
                $('[name="tag2"]').val(data.tag2);
                $('[name="ig_uri"]').val(data.ig_uri);
                $('#modal-candidate-edit').modal('show');
                $('.modal-title').text('Update Candidates'); 
                if (data.thumb) {
                        $('#modal-preview_edit').attr('src', '../assets/candidate' + '/' + data.thumb);
                        $('#hidden_image_edit').attr('src', '../assets/candidate' + '/' + data.thumb);
                }else{
                    $('#modal-preview_edit').attr('src', 'https://via.placeholder.com/150');
                    $('#hidden_image_edit').attr('src', 'https://via.placeholder.com/150');
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function reload_table() {
        table_win.ajax.reload();
        table_gen.ajax.reload();
        table_student.ajax.reload();
        $('#modal-preview').attr('src', 'https://via.placeholder.com/150');
    }

    function readURL(input, id) {
        id = id || '#modal-preview';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview').removeClass('hidden');

        }
    }

    function readURL(input, id) {
        id = id || '#modal-preview_edit';
        if (input.files) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
            $('#modal-preview_edit').removeClass('hidden');

        }
    }

    function deleteCandidate(id) {

        swal({
                title: "Are you sure ?",
                text: "Delete this Candidate",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                cancelButtonText: "Cancel",
                confirmButtonText: "Delete",
                closeOnConfirm: false,
                closeOnCancel: false
            },

            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "<?php echo site_url('backend/candidates/deleteCandidate') ?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function(result) {
                            if (result.status == true) {
                                table_win.ajax.reload();
                                table_gen.ajax.reload();
                                table_student.ajax.reload();
                                swal("Success", "Candidate has been deleted.", "success");
                            } else {
                                table_win.ajax.reload();
                                table_gen.ajax.reload();
                                table_student.ajax.reload();
                                swal("Cancel", "Candidate undelete", "error");

                            }
                        },
                        error: function(err) {
                            swal("Error", "Candidate delete failed", "error");
                        }

                    });
                } else {
                    swal("Error", "Candidate delete failed", "error");
                }
            });
    }

</script>