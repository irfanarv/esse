<?php
$this->data['isuser'] = $this->session->userdata('id');
?>


<script>
	// win hov
	function hov_win(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'win-menu-hov.png') ?>');
	}

	function unhov_win(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'win-menu.png') ?>');
	}

	// stay


	function hov_gall(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery-hov.png') ?>');
	}

	function unhov_gall(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery.png') ?>');
	}

	function hov_ud(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'ud-hov.png') ?>');
	}

	function unhov_ud(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'ud.png') ?>');
	}

	function hovreg(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'reg-hov.png') ?>');
	}

	function unhov_reg(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'register.png') ?>');
	}

	function hovdt(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'dt-hov.png') ?>');
	}

	function unhovdt(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'download.png') ?>');
	}

	function hovdg(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'dg.png') ?>');
	}

	function unhovdg(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'Join now.png') ?>');
	}

	function hoversignvir(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew2.svg') ?>');
	}

	function unhoversignvir(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew.svg') ?>');
	}
	// home
	function hover(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'home-hov.png') ?>');
	}

	function unhover(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
	}

	function hover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
	}

	function unhover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
	}
	// brand
	function hover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'brand-hov.png') ?>');
	}

	function unhover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
	}
	// competition
	function hover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
		$('.hov').show();
		$('.hov-1').hide();

	}
	$(document).ready(function() {
		$('.hov').mouseleave(function() {
			$('.hov').hide();
		});
	});

	// join
	function hover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
		$('.hov-1').show();
		$('.hov').hide();
	}

	$(document).ready(function() {
		$('.hov-1').mouseleave(function() {
			$('.hov-1').hide();
		});
	});

	function unhover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
	}



	// login

	function hover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN-1.svg') ?>');
	}

	function unhover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN.svg') ?>');
	}

	// register

	function hover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>');
	}

	function unhover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER.svg') ?>');
	}

	$(document).ready(function() {
		/* Store the element in el */
		let el = document.getElementById('tilt')

		/* Get the height and width of the element */
		const height = el.clientHeight
		const width = el.clientWidth

		/*
		 * Add a listener for mousemove event
		 * Which will trigger function 'handleMove'
		 * On mousemove
		 */
		el.addEventListener('mousemove', handleMove)

		/* Define function a */
		function handleMove(e) {
			/*
			 * Get position of mouse cursor
			 * With respect to the element
			 * On mouseover
			 */
			/* Store the x position */
			const xVal = e.layerX
			/* Store the y position */
			const yVal = e.layerY

			/*
			 * Calculate rotation valuee along the Y-axis
			 * Here the multiplier 20 is to
			 * Control the rotation
			 * You can change the value and see the results
			 */
			const yRotation = 20 * ((xVal - width / 2) / width)

			/* Calculate the rotation along the X-axis */
			const xRotation = -20 * ((yVal - height / 2) / height)

			/* Generate string for CSS transform property */
			const string = 'perspective(500px) scale(1.1) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)'

			/* Apply the calculated transformation */
			el.style.transform = string
		}

		/* Add listener for mouseout event, remove the rotation */
		el.addEventListener('mouseout', function() {
			el.style.transform = 'perspective(500px) scale(1) rotateX(0) rotateY(0)'
		})

		/* Add listener for mousedown event, to simulate click */
		el.addEventListener('mousedown', function() {
			el.style.transform = 'perspective(500px) scale(0.9) rotateX(0) rotateY(0)'
		})

		/* Add listener for mouseup, simulate release of mouse click */
		el.addEventListener('mouseup', function() {
			el.style.transform = 'perspective(500px) scale(1.1) rotateX(0) rotateY(0)'
		})

		AOS.init();
		$("#header").mousemove(function(e) {
			parallaxHeader(e, "#img-header-1", -60);
			parallaxHeader(e, "#img-header-2", 60);
			parallaxHeader(e, "#img-header-3", -60);
		});

		function parallaxHeader(e, target, movement) {
			var $this = $("#header");
			var relX = e.pageX - $this.offset().left;
			var relY = e.pageY - $this.offset().top;

			TweenMax.to(target, 1, {
				x: (relX - $this.width() / 2) / $this.width() * movement,
				y: (relY - $this.height() / 2) / $this.height() * movement
			});
		}


	});
</script>

<!-- menu mobile -->
<script>
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>


<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" style="height:18px" /></a>
				<br>
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" style="height:18px" /></a>
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
				<!-- sub menu -->
				<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					
					<li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>

				</ul>

				<!-- sub menu -->
				<!-- competition -->
				<a href="<?php echo base_url('winner/general') ?>"><img src="<?php echo base_url(IMG3 . 'win-menu.png') ?>"  class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a href="<?php echo base_url('stay-tuned') ?>"><img src="<?php echo base_url(IMG3 . 'stay_active.png') ?>" class="img-fluid px-2" style="height:29px" /></a>
			</div>

		</div>
	</div>
</div>


<!-- HEADER -->
<div class="bg_com_new">
	<div class="container-fluid header pt-4 px-0" id="header">

		<div class="container">
			<div class="row text-center banner">

				<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>


				<!-- menu -->
				<div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
					<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" style="height:18px" /></a>
					<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" style="height:18px" /></a>
					<a href="#"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>
					<a href="<?php echo base_url('winner/general') ?>"><img src="<?php echo base_url(IMG3 . 'win-menu.png') ?>" onmouseover="hov_win(this);" onmouseout="unhov_win(this);" class="img-fluid px-2" style="height:18px" /></a>
					<img src="<?php echo base_url(IMG3 . 'stay_active.png') ?>" class="img-fluid px-2" style="height:25px" />
				</div>

				<!-- hover -->
				<div class="hov" style="position:relative;display: none;">
				<div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
					<ul class="list-group text-right  d-flex justify-content-end">
						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
						
						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>

					</ul>
				</div>
			</div>
				<!-- join -->

				<div class="col-md-6 col-xs-12 text-right stay_bg" data-aos="fade-right">
					<div style="position: relative;">
						<div id="tilt">
						</div>
					</div>
					<img src=" <?php echo base_url(IMG3 . 'bg_win.png') ?>" class="img_bg_win" />
				</div>

				<div class="col-md-6 col-xs-12 text-left" style="position: relative;">
					<img id="img-header-3" src="<?php echo base_url(IMG3 . 'stay_banner.png') ?>" class="stay_banner" data-aos="zoom-in-left" />
				</div>

			</div> 
		</div>
		<!-- brush -->
		<div style="position:relative">
			<div style="position:absolute;bottom:140px;left:0px;z-index:1000" class="d-none d-md-block">
				<img src="<?php echo base_url(IMG3 . 'yellow_brush.png') ?>" class="img-fluid" style="width:170px">
			</div>
			<div style="position:absolute;bottom: 75px;left:0px;z-index:1000" class="d-none d-md-block">
				<img src="<?php echo base_url(IMG3 . 'green_brush.png') ?>" class="img-fluid" style="width:170px">
			</div>
			<div style="position:absolute;bottom:150px;right:0px;z-index:1000" class="d-none d-md-block">
				<img src="<?php echo base_url(IMG3 . 'blue_brush.png') ?>" class="img-fluid" style="width:230px">
			</div>
			<div style="position:absolute;bottom:20px;right:0px;z-index:1000" class="d-none d-md-block">
				<img src="<?php echo base_url(IMG3 . 'purple_bursh.png') ?>" class="img-fluid" style="width:230px">
			</div>
		</div>
		<!-- brush -->
	</div>
</div>


<!-- FOOTER -->
<div class="container-fluid footer py-2" style="margin-top: 0rem !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>