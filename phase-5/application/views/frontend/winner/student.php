<?php
$this->data['isuser'] = $this->session->userdata('id');

?>
<link rel="stylesheet" href="<?php echo base_url(CSS . 'glightbox.css') ?>">
<style>
	.modal-dialog {
		min-height: calc(100vh - 60px);
		display: flex;
		flex-direction: column;
		justify-content: center;
	}

	@media(max-width: 768px) {
		.modal-dialog {
			min-height: calc(100vh - 20px);
		}
	}
</style>
<script>
	// stay
	function hov_stay(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'stay-menu-hov.png') ?>');
	}

	function unhov_stay(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'stay-menu.png') ?>');
	}

	function hover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
	}

	function unhover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
	}

	function ig_hov(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'Instagram.png') ?>');
	}

	function ig_unhov(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'Instagram-1.png') ?>');
	}
	// home
	function hover(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'home-hov.png') ?>');
	}

	function unhover(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
	}

	function hover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'brand-hov.png') ?>');
	}

	function unhover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
	}

	// competition
	function hover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
		$('.hov').show();
		$('.hov-1').hide();
	}
	$(document).ready(function() {
		$('.hov').mouseleave(function() {
			$('.hov').hide();
		});

	});

	function unhover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'COMPETITION.png') ?>');
	}
	// join
	function hover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
		$('.hov-1').show();
		$('.hov').hide();
	}

	$(document).ready(function() {
		$('.hov-1').mouseleave(function() {
			$('.hov-1').hide();
		});
		$('.slider').slick({
			dots: false,
			autoplay: true,
			dots: false,
			prevArrow: false,
			nextArrow: false,
			autoplaySpeed: 2000,
			infinite: false,
			speed: 500
		});
		$('.slide-winner').slick({
			dots: false,
			autoplay: true,

			prevArrow: false,
			nextArrow: false,
			autoplaySpeed: 2000,
			infinite: false,
			speed: 500
		});
		$('.slide-video').slick({
			draggable: true,
			infinite: false,
			slidesToShow: 2,
			slidesToScroll: 2,
			dots: false,
			prevArrow: '<button class="slide-arrow prev-arrow"></button>',
			nextArrow: '<button class="slide-arrow next-arrow"></button>',
			responsive: [{
					breakpoint: 1024,
					settings: {
						draggable: true,
						slidesToShow: 2,
						slidesToScroll: 2,
						infinite: false
					}
				},
				{
					breakpoint: 600,
					settings: {
						autoplaySpeed: 1000,
						speed: 500,
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots:false,
						autoplay: true,
						autoplaySpeed: 2000,
						infinite: false,
						speed: 500
					}
				}
			]

		});
	});

	function unhover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
	}

	// about competition

	function hover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>');
	}

	function unhover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition.svg') ?>');
	}



	$(document).ready(function() {
		AOS.init();
	});
</script>
<!-- menu mobile -->
<script>
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>

<!-- gallery vote -->
<script>

	$(document).ready(function() {
		var btns = document.getElementsByClassName("btn-reg");
		for (var i = 0; i < btns.length; i++) {
			btns[i].addEventListener("click", function() {
				var current = document.getElementsByClassName("active");

				if (current.length > 0) {
					current[0].className = current[0].className.replace(" active", "");
				}
				this.className += " active";
			});
		}
	});
</script>


<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" style="height:18px" /></a><br>
				
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" style="height:18px" /></a>
				<!-- competition -->
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
				<!-- sub menu -->
				<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					
					<li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
				</ul>
				<!-- sub menu -->
				<!-- competition -->
				<!-- join -->
				<a href="<?php echo base_url('winner/general') ?>"><img src="<?php echo base_url(IMG3 . 'win-active.png') ?>"  class="img-fluid px-2 mbm" style="height:29px" /></a> </br>
				<a href="<?php echo base_url('stay-tuned') ?>"><img src="<?php echo base_url(IMG3 . 'stay-menu.png') ?>" class="img-fluid px-2" style="height:18px" /></a>

			</div>

		</div>
	</div>
</div>


<!-- HEADER -->
<div class="judge-info" style="background-repeat: repeat; background-position: bottom;">
	<div class="container-fluid header pt-4 px-0">
		<div class="container">
			<div class="row text-center mb-3">
				<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>
				<!-- menu -->
				<div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
					<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" style="height:18px" /></a>
					<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid mt-4 mbm" style="height:18px" /></a>
					<a href="#"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>
					<img src="<?php echo base_url(IMG3 . 'win-active.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
					<a href="<?php echo base_url('stay-tuned') ?>"><img src="<?php echo base_url(IMG3 . 'stay-menu.png') ?>" onmouseover="hov_stay(this);" onmouseout="unhov_stay(this);" class="img-fluid px-2" style="height:18px" /></a>

				</div>
				<!-- hover -->
				<div class="hov" style="position:relative;display: none;">
					<div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
						<ul class="list-group text-right  d-flex justify-content-end">
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
						</ul>
					</div>
				</div>


			</div>
		</div>

		<div class="container-fluid" style="margin-top: 0px !important; padding-bottom:0px !important;">
			<div class="container">
				<div class="row text-center">
					<!-- title  -->
					<div class="col-md-12 mb-5 d-none d-md-block" style="z-index: -1 !important;" data-aos="zoom-in-up">
						<img src="<?php echo base_url(IMG3 . 'express the change.png') ?>" class="img-fluid" style="width: 655px;" />
					</div>

					<div class="col-md-12 d-block d-md-none" style="z-index: -1 !important; margin-bottom: 2.5rem;" data-aos="zoom-in-up">
						<img src="<?php echo base_url(IMG3 . 'express the change.png') ?>" class="img-fluid" style="width: 350px;" />
					</div>

					<div class=" col-md-6 col-md-6  mt-3" data-aos="zoom-out-down">
						<a href="<?php base_url()?>general" class="btn-reg">
							<img id="general_button" src="<?php echo base_url(IMG3 . 'general.png') ?>" class="img-btn-reg a a">
						</a>
					</div>
					<div class="col-md-6 col-md-6  mt-3" data-aos="zoom-out-up">
						<a href="<?php base_url()?>student" class="btn-reg active">

							<img id="student_button" src="<?php echo base_url(IMG3 . 'student-active.png') ?>" class="img-btn-reg b a">
						</a>
					</div>
					<!-- button mobile -->

					<div style="position:relative">
						<div style="position:absolute;top:20px;right:0px" class="d-none d-md-block">
							<img src="<?php echo base_url(IMG . 'green brush.png') ?>" class="img-fluid" style="width:120px">
						</div>
					</div>

					<div class="col-md-12 align-self-center text-center" data-aos="zoom-out-right" id="general">
						<!-- juara 1 general -->
						<?php foreach ($data1ststu as $d1) : ?>
							<div class="row d-flex justify-content-center vote-mb-info">
								<!-- desktop -->
								<div class="col-md-1 mb-3 d-md-block d-none"></div>
								<div class="col-md-3 mb-3 d-md-block d-none" style="margin-top: 6rem;">
									<div class="row">
										<div class="col-md-12 mb-5">
											<img id="img-prize-2" src="<?php echo base_url(IMG3 . '1st.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
										<a href="<?php echo base_url('/about-competition/detail/') ?><?php echo $d1->uuid_sub; ?>">
												<div class="title-gallery-win">
													<?php echo $d1->name; ?>
												</div>
											</a>
											<div class="loc-gallery-vote-win">
												<img src="<?php echo base_url(IMG3 . 'art.png') ?>" class="img-fluid"> <?php echo $d1->concept_name; ?>
											</div>

										</div>
									</div>
								</div>
								<div class="col-md-1 mb-3 d-md-block d-none"></div>
								<div class="col-md-6 mb-3 d-md-block d-none">
									<div class="row">
										<div class="col-md-12 align-self-center text-center">
											<div class="row slider">
												<?php foreach ($gallery1ststu as $g1) : ?>
													<div class="col py-3 px-3">
														<div class="card card-reg card-gallery" style="margin-bottom: 1rem !important;">
															<div class="card-body card-reg d-flex justify-content-center">
																<a class="glightbox" href="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g1->gallery_name; ?>"><img class="img-fluid" src="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g1->gallery_name; ?>"></a>
															</div>
														</div>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									</div>
								</div>

								<!-- mobile -->
								<div class="col-md-12 d-md-none d-block">
									<div class="row">
										<div class="col-md-12 mb-3">
											<img id="img-prize-2" src="<?php echo base_url(IMG3 . '1st.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
											<a href="<?php echo base_url('/about-competition/detail/') ?><?php echo $d1->uuid_sub; ?>">
												<div class="title-gallery-win">
													<?php echo $d1->name; ?>
												</div>
											</a>
											<div class="loc-gallery-vote-win">
												<img src="<?php echo base_url(IMG3 . 'art.png') ?>" class="img-fluid"> <?php echo $d1->concept_name; ?>
											</div>

										</div>
										<div class="col-md-12">
											<div class="row slider">

												<?php foreach ($gallery1ststu as $g1) : ?>
													<div class="col py-3 px-3">
														<div class="card card-reg card-gallery" style="margin-bottom: 1rem !important;">
															<div class="card-body card-reg d-flex justify-content-center">
																<a class="glightbox" href="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g1->gallery_name; ?>"><img class="img-fluid" src="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g1->gallery_name; ?>"></a>
															</div>
														</div>
													</div>
												<?php endforeach; ?>

											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>

					</div>
				</div>
			</div>
		</div>


	</div>
</div>

<!-- bg-blue -->
<div class="mt-5" style="position:relative" id="gen">
	<div class="d-md-block d-none" style="position:absolute;bottom:-100px;z-index:-1">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
	<div class="d-md-none d-block" style="position:absolute;bottom:-18px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
</div>
<div class="mt-5" style="position:relative; display:none;" id="stu">
	<div class="d-md-block d-none" style="position:absolute;bottom:-100px;z-index:-1">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
	<div class="d-md-none d-block" style="position:absolute;bottom:-18px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
</div>
<!-- general -->
<div class="container-fluid participant-cat" style="padding-bottom:0px !important" id="gen1">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<?php foreach ($data2ndstu as $d2) : ?>
				<div class="col-md-5 mb-3 mt-5">
					<div class="row mb-5">
						<div class="col-md-3 col-3"></div>
						<div class="col-md-6 col-6">
							<img src="<?php echo base_url(IMG3 . '2nd.png') ?>" style="width:100%">
						</div>
						<div class="col-md-3 col-3"></div>
					</div>

					<div class="slider">
						<?php foreach ($gallery2ndstu as $g2) : ?>
							<div class="card card-reg card-gall card-det" style="margin-right: 0.5rem; margin-bottom: 0.5rem;">
								<div class="card-body card-reg d-flex justify-content-center">
									<a class="glightbox" href="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g2->gallery_name; ?>"><img class="img-fluid" src="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g2->gallery_name; ?>"></a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>

					<a href="<?php echo base_url('/about-competition/detail/') ?><?php echo $d2->uuid_sub; ?>">
						<div class="title-gallery-bg">
							<?php echo $d2->name; ?>

						</div>
					</a>
					<div class="loc-gallery-vote-bg">
						<img src="<?php echo base_url(IMG3 . 'art-white.png') ?>" class="img-fluid"> <?php echo $d2->concept_name; ?>
					</div>

				</div>
			<?php endforeach; ?>
			<?php foreach ($data3rdstu as $d3) : ?>
				<div class="col-md-5 mb-3 mt-5">
					<div class="row mb-5">
						<div class="col-md-3 col-3"></div>
						<div class="col-md-6 col-6">
							<img src="<?php echo base_url(IMG3 . '3rd.png') ?>" style="width:100%">
						</div>
						<div class="col-md-3 col-3"></div>
					</div>
					<div class="slider">
						<?php foreach ($gallery3rdstu as $g3) : ?>
							<div class="card card-reg card-gall card-det" style="margin-right: 0.5rem; margin-bottom: 0.5rem;">
								<div class="card-body card-reg d-flex justify-content-center">
									<a class="glightbox" href="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g3->gallery_name; ?>"><img class="img-fluid" src="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $g3->gallery_name; ?>"></a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>

					<a href="<?php echo base_url('/about-competition/detail/') ?><?php echo $d3->uuid_sub; ?>">
						<div class="title-gallery-bg">
							<?php echo $d3->name; ?>
						</div>
					</a>
					<div class="loc-gallery-vote-bg">
						<img src="<?php echo base_url(IMG3 . 'art-white.png') ?>" class="img-fluid"> <?php echo $d3->concept_name; ?>
					</div>

				</div>
			<?php endforeach; ?>

		</div>
	</div>

	<div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom-vote">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
	<div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile-toc">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
</div>
<!-- ca & sp gen-->
<div class="container-fluid gen2 judge-info" id="gen2">

	<div class="row">
		<div class="col-md-12 align-self-center text-center">
			<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'choice.png') ?>" class="img-fluid" data-aos="zoom-out" />
		</div>
	</div>

	<!-- choice award -->
	<div class="container">
		<div class="row d-flex justify-content-center">
			<?php foreach ($dataCAstu as $dca) : ?>
				<div class="col-md-2 col-6 mt-5" data-aos="zoom-in">
				<a href="<?php echo base_url('/about-competition/detail/') ?><?php echo $dca->uuid_sub; ?>">
						<div class="card card-reg card-gall cardca">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url("assets/"); ?>candidate/<?php echo $dca->thumb; ?>" class="img-fluid">
							</div>

						</div>

						<div class="title-gallery com">
						<div class="name-candidate">
							<?php echo $dca->name; ?>
						</div>
						</div>
					</a>
					<div class="loc-gallery-vote konsep">
					<div class="detail-konsep">
						<img src="<?php echo base_url(IMG3 . 'art.png') ?>" class="img-fluid"> <?php echo $dca->concept_name; ?>
						</div>
					</div>


				</div>
			<?php endforeach; ?>

		</div>
	</div>

	<div class="row">
		<div class="col-md-12 align-self-center text-center mt-5">
			<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'special.png') ?>" class="img-fluid" data-aos="zoom-out" />
		</div>
	</div>
	<!-- special prize -->
	<div class="container">
		<div class="row d-flex justify-content-center">
			<?php foreach ($dataSPstu as $dsp) : ?>
				<div class="col-md-2 col-6 mt-5">
				<a href="<?php echo base_url('/about-competition/detail/') ?><?php echo $dsp->uuid_sub; ?>">
						<div class="card card-reg card-gall cardca">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url("assets/"); ?>candidate/<?php echo $dsp->thumb; ?>" class="img-fluid">
							</div>

						</div>

						<div class="title-gallery com">
						<div class="name-candidate">
							<?php echo $dsp->name; ?>
						</div>
						</div>
					</a>
					<div class="loc-gallery-vote konsep">
					<div class="detail-konsep">
						<img src="<?php echo base_url(IMG3 . 'art.png') ?>" class="img-fluid"> <?php echo $dsp->concept_name; ?>


					</div>
					</div>


				</div>
			<?php endforeach; ?>
		</div>
	</div>

	<div style="position:relative">
		<div style="position:absolute;bottom:-40px;left:-16px;z-index:1000" class="d-none d-md-block">
			<img src="<?php echo base_url(IMG3 . 'blue-win.png') ?>" class="img-fluid" style="width:170px">
		</div>
		<div style="position:absolute;bottom:-150px;right:-16px;z-index:1000" class="d-none d-md-block">
			<img src="<?php echo base_url(IMG3 . 'purple-win.png') ?>" class="img-fluid" style="width:230px">
		</div>
	</div>
	
</div>

<!-- greeting from esse -->
<div class="container-fluid  participant-cat bg-slide-win">
<div style="position:relative">
			<div style="position:absolute;bottom:-1px;z-index:-1; left:-1rem; right:-1rem;">
			<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
			</div>

	</div>
	<div class="container">

		<div class="row text-center">
			<div class="col-md-12 align-self-center text-center d-none d-md-block mb-5">
				<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'meet-win.png') ?>" class="img-fluid" data-aos="zoom-out" />
			</div>
		</div>
		<div class="row text-center slide-video">
			

				<!-- 2 -->
				<div class="col-md-6 col-12">
					<div class="card home-greeting-card" data-aos="zoom-in-up" style="left:0px !important;">
						<div class="card-body">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/EpKDdcepLRI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-12">
					<div class="row mb-4">
						<div class="col-3"></div>
						<img src="<?php echo base_url(IMG3 . '2ndslide.png') ?>" class="img-fluid col-6 mb-3 d-flex justify-content-center" data-aos="zoom-in-down" />
						<div class="col-md-12" style="z-index:1000">
							<img src="<?php echo base_url(IMG3 . 'artikel2.png') ?>" class="img-fluid mb-3" data-aos="fade-down" />
						</div>
					</div>
				</div>
				<div class="col-md-6 col-12">
					<div class="card home-greeting-card" data-aos="zoom-in-up" style="left:0px !important;">
						<div class="card-body">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/o7s8uvOoyF8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-12 mt-5">
					<div class="row mb-4">
						<div class="col-3"></div>
						<img src="<?php echo base_url(IMG3 . '1stwin.png') ?>" class="img-fluid col-6 mb-3 d-flex justify-content-center mt-5" data-aos="zoom-in-down" />
						<div class="col-md-12 mt-3" style="z-index:1000">
							<img src="<?php echo base_url(IMG3 . 'artikel.png') ?>" class="img-fluid mb-3" data-aos="fade-down" />
						</div>
					</div>
				</div>
				<!-- 3 -->
				<div class="col-md-6 col-12">
					<div class="card home-greeting-card" data-aos="zoom-in-up" style="left:0px !important;">
						<div class="card-body">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/LLINlP-ny1k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-12">
					<div class="row mb-4">
						<div class="col-3"></div>
						<img src="<?php echo base_url(IMG3 . '3rdslide.png') ?>" class="img-fluid col-6 mb-3 d-flex justify-content-center" data-aos="zoom-in-down" />
						<div class="col-md-12" style="z-index:1000">
							<img src="<?php echo base_url(IMG3 . 'artikel3.png') ?>" class="img-fluid mb-3" data-aos="fade-down" />
						</div>
					</div>
				</div>
		</div>
	</div>


	<div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom-home-win">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
	<div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile-new-win">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
</div>



<div class="container-fluid  py-5 ig-story" id="footer1">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid" style="width:350px" /></a>
			</div>

		</div>
	</div>
</div>


<!-- FOOTER -->
<div class="container-fluid footer py-2" style="margin-top: 0px !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>

<script src="<?php echo base_url(JS . 'glightbox.js') ?>"></script>
<script>
	var lightbox = GLightbox({
		'selector': 'glightbox'
	});
</script>