<?php
$this->data['isuser'] = $this->session->userdata('id');
?>
<script>
	// win hov
	function hov_win(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'win-menu-hov.png') ?>');
	}

	function unhov_win(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'win-menu.png') ?>');
	}

	// stay
	function hov_stay(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'stay-menu-hov.png') ?>');
	}

	function unhov_stay(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'stay-menu.png') ?>');
	}

	function hov_gall(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery-hov.png') ?>');
	}

	function unhov_gall(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery.png') ?>');
	}

	function hover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
	}

	function unhover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
	}

	function ig_hov(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'Instagram.png') ?>');
	}

	function ig_unhov(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'Instagram-1.png') ?>');
	}
	// home
	function hover(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'home-hov.png') ?>');
	}

	function unhover(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
	}

	// competition
	function hover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
		$('.hov').show();
		$('.hov-1').hide();
	}
	$(document).ready(function() {
		$('.hov').mouseleave(function() {
			$('.hov').hide();
		});
	});

	function unhover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'COMPETITION.png') ?>');
	}
	// join
	function hover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
		$('.hov-1').show();
		$('.hov').hide();
	}

	$(document).ready(function() {
		$('.hov-1').mouseleave(function() {
			$('.hov-1').hide();
		});
	});

	function unhover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
	}

	// about competition

	function hover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>');
	}

	function unhover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition.svg') ?>');
	}

	// upload

	function hover_upload(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'upload-blue.svg') ?>');
	}

	function unhover_upload(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'UPLOAD.svg') ?>');
	}


	// login

	function hover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN-1.svg') ?>');
	}

	function unhover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN.svg') ?>');
	}

	// register

	function hover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>');
	}

	function unhover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER.svg') ?>');
	}
	$(document).ready(function() {
		AOS.init();
		$('.slider').slick({
			dots: true,
			autoplay: true,
			autoplaySpeed: 1000,
			infinite: true,
			speed: 500
		});
		$("#product").mousemove(function(e) {
			parallaxProduct(e, "#product-1", -120);
			parallaxProduct(e, "#product-2", 120);
			parallaxProduct(e, "#product-3", -120);
			parallaxProduct(e, "#product-4", 120);
		});

		function parallaxProduct(e, target, movement) {
			var $this = $("#product");
			var relX = e.pageX - $this.offset().left;
			var relY = e.pageY - $this.offset().top;

			TweenMax.to(target, 1, {
				x: (relX - $this.width() / 2) / $this.width() * movement,
				y: (relY - $this.height() / 2) / $this.height() * movement
			});
		}
	});
</script>

<!-- menu mobile -->
<script>
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>

<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" style="height:18px" /></a>
				
				<img src="<?php echo base_url(IMAGES . 'brand.png') ?>" class="img-fluid px-2 mbm" style="height:30px" />
				<!-- competition -->
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
				<!-- sub menu -->
				<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					

					<li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>

				</ul>
				<a href="<?php echo base_url('winner/general') ?>"><img src="<?php echo base_url(IMG3 . 'win-menu.png') ?>"  class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a href="<?php echo base_url('stay-tuned') ?>"><img src="<?php echo base_url(IMG3 . 'stay-menu.png') ?>" class="img-fluid px-2" style="height:18px" /></a>
				<!-- sub menu -->
				<!-- competition -->
			</div>

		</div>
	</div>
</div>


<!-- HEADER -->
<div class="container-fluid header pt-4 px-0">
	<div class="container">
		<div class="row text-center mb-5">

			<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
				<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
			</div>


			<!-- menu -->
			<div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" style="height:18px" /></a>
				<img src="<?php echo base_url(IMAGES . 'brand.png') ?>" class="img-fluid px-2" style="height:30px" />
				<a href="#"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>
				<a href="<?php echo base_url('winner/general') ?>"><img src="<?php echo base_url(IMG3 . 'win-menu.png') ?>" onmouseover="hov_win(this);" onmouseout="unhov_win(this);" class="img-fluid px-2" style="height:18px" /></a>
				<a href="<?php echo base_url('stay-tuned') ?>"><img src="<?php echo base_url(IMG3 . 'stay-menu.png') ?>" onmouseover="hov_stay(this);" onmouseout="unhov_stay(this);" class="img-fluid px-2" style="height:18px" /></a>
				
			</div>

			<!-- hover -->
			<div class="hov" style="position:relative;display: none;">
				<div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
					<ul class="list-group text-right  d-flex justify-content-end">
						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
						

						<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>

					</ul>
				</div>
			</div>


		</div>
	</div>

	<div class="container-fluid brand-story">
		<div class="container">
			<div class="row text-center">

				<div class="col-md-6 d-none d-md-block align-self-center text-left" data-aos="zoom-out-right">
					<div class="slider">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'double.jpg') ?>" alt="Double">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'applemint.jpg') ?>" alt="Apple Mint">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'grape.jpg') ?>" alt="Grape">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'juicy.jpg') ?>" alt="Juice">
					</div>
				</div>

				<div class="col-md-6 d-block d-md-none mb-5 pl-4 pr-4" data-aos="zoom-out-right">
					<div class="slider">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'double.jpg') ?>" alt="Double">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'applemint.jpg') ?>" alt="Apple Mint">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'grape.jpg') ?>" alt="Grape">
						<img class="img-fluid" src="<?php echo base_url(IMAGES . 'juicy.jpg') ?>" alt="Juice">
					</div>
				</div>

				<div class="col-md-6 d-none d-md-block" data-aos="zoom-out-left">
					<img src="<?php echo base_url(IMGS . 'artikelnew.svg') ?>" class="img-fluid" />
				</div>

				<div class="col-md-6 d-block d-md-none">
					<img src="<?php echo base_url(IMAGES . 'artikel-m-1.svg') ?>" class="img-fluid" style="width: 300px;">
				</div>

			</div>
		</div>
	</div>


</div>




<!-- <div class="d-none d-md-block" style="position:relative">
	<div style="position:absolute;bottom:-230px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>"  style="width:100%">
	</div>
</div>
<div class="d-block d-md-none" style="position:relative">
	<div style="position:absolute;bottom:-100px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>"  style="width:100%">
	</div>
</div> -->

<div class="d-none d-md-block container-fluid competition-criteria " style="margin-top: 28px;">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="card" style="margin-top: 200px !important; left:0px !important;">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>

			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<!-- <div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>"  style="width:100%">
		</div>
	</div> -->
</div>

<div class="d-block d-md-none container-fluid competition-criteria ">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<div class="card" style="margin-top: 100px !important; left:0px !important;">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- <div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>"  style="width:100%">
		</div>
	</div> -->
</div>



<div class="container-fluid  py-5 ig-story">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid" style="width:350px" /></a>
			</div>

		</div>
	</div>
</div>


<!-- FOOTER -->
<div class="container-fluid footer py-2" style="margin-top: 0px !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>