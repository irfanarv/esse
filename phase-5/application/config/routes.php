<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Home';
$route['brandstory'] = 'Story';
$route['vote-now'] = 'Vote';
$route['stay-tuned'] = 'Stay';
// direct and show popup after account verified
$route['votenow'] = 'Vote/berhasil';
// 
$route['vote-now/detail/(:any)'] = 'Vote/detail/$1';
$route['about-competition'] = 'Competition';
$route['about-competition/detail/(:any)'] = 'Competition/detail/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['signout'] = 'home/logout';

// dashboard
$route['dashboard'] = 'backend/dashboard';
$route['dashboard/participants'] = 'backend/participants';
$route['dashboard/submission'] = 'backend/submission';
$route['dashboard/candidates'] = 'backend/candidates';
$route['dashboard/voters'] = 'backend/voter';
$route['dashboard/cadidates-mockup/(:any)'] = 'backend/candidates/gallery/$1';
$route['dashboard/login'] = 'backend/dashboard/login';
$route['logout'] = 'backend/dashboard/logout';
