-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 27 Apr 2021 pada 20.32
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_esse`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `participans`
--

CREATE TABLE `participans` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `image` varchar(254) DEFAULT NULL,
  `password` varchar(254) DEFAULT NULL,
  `idcard` varchar(254) DEFAULT '',
  `city` varchar(254) DEFAULT NULL,
  `email` varchar(254) DEFAULT NULL,
  `dob_day` varchar(254) DEFAULT '',
  `dob_month` varchar(254) DEFAULT NULL,
  `dob_year` varchar(254) DEFAULT NULL,
  `category` varchar(254) DEFAULT NULL,
  `date_register` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `participans`
--

INSERT INTO `participans` (`id`, `name`, `image`, `password`, `idcard`, `city`, `email`, `dob_day`, `dob_month`, `dob_year`, `category`, `date_register`) VALUES
(1, 'Irfan Arifin', 'e5c5e5fa86161741c1c0728011c869a6.png', '$2y$10$LUTU714C6yUQJBrLFgqpQeMtMZIYuu/NdE1g9c7FKESRJtzCP0Pfm', '123456789', 'Bogor', 'ir.irfan.arifin@gmail.com', '1', '5', '2015', 'STUDENTS', '2021-04-19 13:10:38'),
(3, 'Hanan', '828c39022d7d6dae5d886648f8e3c914.png', '$2y$10$jp7KGlG0QXW7azZ28C5pkuar7ulr4jK4xDsxr.cPttOlEepJ9xQYO', '123456789', 'Bogor', 'ir.irfan.arifin@gmail.com', '30', '1', '2003', 'GENERAL', '2021-04-27 13:11:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `password`) VALUES
(1, 'Administrator', 'esse@makechange.id', '8cb2237d0679ca88db6464eac60da96345513964');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visitors`
--

CREATE TABLE `visitors` (
  `pengunjung_id` int(11) NOT NULL,
  `pengunjung_tanggal` timestamp NULL DEFAULT current_timestamp(),
  `pengunjung_ip` varchar(40) DEFAULT NULL,
  `pengunjung_perangkat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `visitors`
--

INSERT INTO `visitors` (`pengunjung_id`, `pengunjung_tanggal`, `pengunjung_ip`, `pengunjung_perangkat`) VALUES
(1, '2021-04-26 17:25:41', '::1', 'Chrome');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visitors_downloads`
--

CREATE TABLE `visitors_downloads` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `file_type` int(2) NOT NULL COMMENT '1: Guide 2: Template'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `visitors_downloads`
--

INSERT INTO `visitors_downloads` (`id`, `ip_address`, `date`, `file_type`) VALUES
(1, '0', '2021-04-25 17:00:00', 1),
(2, '0', '2021-04-26 17:00:00', 1),
(3, '::1', '2021-04-26 17:00:00', 1),
(4, '::1', '2021-04-26 17:00:00', 1),
(5, '::1', '2021-04-25 17:00:00', 2),
(6, '::1', '2021-04-26 17:00:00', 1),
(7, '::1', '2021-04-26 17:00:00', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `participans`
--
ALTER TABLE `participans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`pengunjung_id`);

--
-- Indeks untuk tabel `visitors_downloads`
--
ALTER TABLE `visitors_downloads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `participans`
--
ALTER TABLE `participans`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `visitors`
--
ALTER TABLE `visitors`
  MODIFY `pengunjung_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `visitors_downloads`
--
ALTER TABLE `visitors_downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
