
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Hello <?php echo $this->session->userdata('nama'); ?> ,
                            <?php
                                date_default_timezone_set('Asia/Jakarta');
                                $Hour = date('G');
                                if ( $Hour >= 5 && $Hour <= 11 ) {
                                    echo "Selamat Pagi";
                                } else if ( $Hour >= 12 && $Hour <= 15 ) {
                                    echo "Selamat Siang";
                                } else if ( $Hour >= 16 && $Hour <= 18 ) {
                                    echo "Selamat Sore";
                                }else if ( $Hour >= 19 || $Hour <= 4 ) {
                                    echo "Selamat Malam";
                                }
                                ?>
                                👋
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-sm-12">
                <div class="card user-profile-list">
                    <div class="card-body">

                        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active text-uppercase" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="home" aria-selected="true">All</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="profile" aria-selected="false">General</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="student-tab" data-toggle="tab" href="#student" role="tab" aria-controls="contact" aria-selected="false">Students</a>
							</li>
						</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_all" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Identity Number</th>
                                                <th>Name</th>
                                                <th>City</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Date of birth</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
							</div>
							<div class="tab-pane fade" id="general" role="tabpanel" aria-labelledby="general-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_general" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>KTP Number</th>
                                                <th>Name</th>
                                                <th>City</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Date of birth</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
							</div>
							<div class="tab-pane fade" id="student" role="tabpanel" aria-labelledby="student-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_student" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Identity Number</th>
                                                <th>Name</th>
                                                <th>City</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Date of birth</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
							</div>
						</div>
                    </div>
                </div>
            </div>
                      
        </div>
    </div>
</div>


<div class="modal fade" id="modal_details" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
            <div class="modal-header">
                <h5 class="modal-title mt-0"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" id="isiModal">
                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script src="<?= base_url(); ?>assets/dashboard/assets/js/plugins/bootstrap.min.js"></script>
<script src="<?= base_url();?>assets/dashboard/assets/js/plugins/jquery.dataTables.min.js"></script>
<script src="<?= base_url();?>assets/dashboard/assets/js/plugins/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>

<script>

  var table_all;
  var table_general;
  var table_student;
  
    $(document).ready(function() {
        $.noConflict();
        function newexportaction(e, dt, button, config) {
         var self = this;
         var oldStart = dt.settings()[0]._iDisplayStart;
         dt.one('preXhr', function (e, s, data) {
             // Just this once, load all data from the server...
             data.start = 0;
             data.length = 2147483647;
             dt.one('preDraw', function (e, settings) {
                 // Call the original action function
                 if (button[0].className.indexOf('buttons-copy') >= 0) {
                     $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                     $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                     $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                     $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                         $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                         $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                 } else if (button[0].className.indexOf('buttons-print') >= 0) {
                     $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                 }
                 dt.one('preXhr', function (e, s, data) {
                     // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                     // Set the property to what it was before exporting.
                     settings._iDisplayStart = oldStart;
                     data.start = oldStart;
                 });
                 // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                 setTimeout(dt.ajax.reload, 0);
                 // Prevent rendering of the full data to the DOM
                 return false;
             });
         });
         // Requery the server with the new one-time export settings
         dt.ajax.reload();
        }

        table_all = $('#table_all').DataTable({ 
          
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('backend/Participants/all')?>",
                "type": "POST"
            },
            dom: 'Blfrtip',
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          buttons: [
              {
                  extend: 'excelHtml5',
                  "action": newexportaction,
                  exportOptions: {
                        stripHtml: false
                    }
                  
              },
              {
                  extend: 'print',
                  "action": newexportaction,
                  exportOptions: {
                        stripHtml: false
                    }
                  
              },
              {
                  extend: 'pdfHtml5',
                  exportOptions: {
                        stripHtml: false
                    },
                  "action": newexportaction
              }
            ],

        });

        // 

        table_general = $('#table_general').DataTable({ 
          
          "processing": true, 
          "serverSide": true, 
          "order": [], 
          "ajax": {
              "url": "<?php echo site_url('backend/Participants/general')?>",
              "type": "POST"
          },
          dom: 'Blfrtip',
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
            {
                extend: 'excelHtml5',
                "action": newexportaction,
                exportOptions: {
                      stripHtml: false
                  }
                
            },
            {
                extend: 'print',
                "action": newexportaction,
                exportOptions: {
                      stripHtml: false
                  }
                
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                      stripHtml: false
                  },
                "action": newexportaction
            }
          ],

      });

    //   

    table_student = $('#table_student').DataTable({ 
          
          "processing": true, 
          "serverSide": true, 
          "order": [], 
          "ajax": {
              "url": "<?php echo site_url('backend/Participants/students')?>",
              "type": "POST"
          },
          dom: 'Blfrtip',
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
            {
                extend: 'excelHtml5',
                "action": newexportaction,
                exportOptions: {
                      stripHtml: false
                  }
                
            },
            {
                extend: 'print',
                "action": newexportaction,
                exportOptions: {
                      stripHtml: false
                  }
                
            },
            {
                extend: 'pdfHtml5',
                exportOptions: {
                      stripHtml: false
                  },
                "action": newexportaction
            }
          ],

      });
    //   


    });
    function detail_participants(id)
        { 
            $.ajax({
                url:"<?php echo site_url('backend/Participants/details')?>/"+ id,
                method:"POST",
                success:function(data){
                    $('#modal_details').modal('show'); 
                    $('.modal-title').text('Detail Participants'); 
                    $('#isiModal').html(data);
                    }
                });
        }

</script>