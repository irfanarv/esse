<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Required meta tags -->
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no,maximum-scale=1">
  

  <meta name="description" content="Please join the most innovative, modern and high-tech product in bringing out your greatest potential and bringing a CHANGE to our world!" />
  <meta name="keywords" content="esse, esse change, esse makechange, makechange, esse make change, make change" />
  <meta name="author" content="Esse" />

  <meta name="og:title" content="<?php echo WEB_TITLE." - ".$page_title ?>"/>
  <meta name="og:type" content="event"/>
  <meta name="og:url" content="<?php echo current_url() ?>"/>
  <meta name="og:image" content="<?php echo base_url(IMG."main-banner.png")  ?>"/>
  <meta name="og:site_name" content="Esse Make Change"/>
  <meta name="og:description" content="Please join the most innovative, modern and high-tech product in bringing out your greatest potential and bringing a CHANGE to our world!"/>

  <title><?php echo WEB_TITLE." - ".$page_title ?></title>

  <!-- Favicon links -->
  <link rel="shortcut icon" href="<?php echo base_url(IMG.'/logo.png') ?>" type="image/png">

  <!-- Theme CSS -->
  <link rel="stylesheet" href="<?php echo base_url(JS.'bootstrap-4.1.3/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url(CSS.'app.css') ?>?t=<?php echo time() ?>">   
  <link rel="stylesheet" href="<?php echo base_url(CSS.'side-modal.css') ?>?t=<?php echo time() ?>">   
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
  <!-- JS -->
  <script src="<?php echo base_url(JS.'jquery-3.5.1.min.js') ?>" ></script>  
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="<?php echo base_url(JS.'bootstrap-4.1.3/js/bootstrap.min.js') ?>" ></script>
  <script src="<?php echo base_url(JS.'libs/ScrollMagic.min.js') ?>" ></script>
  <script src="<?php echo base_url(JS.'libs/debug.addIndicators.min.js') ?>" ></script>
  <script src="<?php echo base_url(JS.'libs/TweenMax.min.js') ?>" ></script>
  <script src="<?php echo base_url(JS.'libs/animation.gsap.min.js') ?>" ></script>
  <script src="<?php echo base_url(JS.'app.js') ?>" ></script>
  <!-- Fancy Box -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.4.0/dist/jquery.fancybox.min.css" />
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.4.0/dist/jquery.fancybox.min.js"></script>
  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</head>

<body oncontextmenu="return false" onselectstart="return false" ondragstart="return false">

<?php 
  // load content
  $this->load->view("frontend/".$content_view); 
?>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</body>
</html>