<style>
.modal-dialog {
    min-height: calc(100vh - 60px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    /* overflow: auto; */
}
@media(max-width: 768px) {
  .modal-dialog {
    min-height: calc(100vh - 20px);
  }
} 
</style>

<script>
    // home
    function hover(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'home-hov.png') ?>');
    }

    function unhover(element) {
    element.setAttribute('src', '<?php echo base_url(IMG.'HOME.png') ?>');
    }
    // brand
    function hover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'brand-hov.png') ?>');
    }

    function unhover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMG.'BRAND STORY.png') ?>');
    }
   
$(document).ready(function () {

  AOS.init();
  $("#header").mousemove(function(e) {
    parallaxHeader(e, "#img-header-1", -60);
    parallaxHeader(e, "#img-header-2", 60);
    parallaxHeader(e, "#img-header-3", -60);
  });

  function parallaxHeader(e, target, movement) {
    var $this = $("#header");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }


  $("#participant-info").mousemove(function(e) {
    parallaxParticipant(e, "#general-bt", -60);
    parallaxParticipant(e, "#students-bt", 60);
  });

  function parallaxParticipant(e, target, movement) {
    var $this = $("#participant-info");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }


  $("#prize").mousemove(function(e) {
    parallaxPrize(e, "#img-prize-1", -10);
    parallaxPrize(e, "#img-prize-2", 40);
    parallaxPrize(e, "#img-prize-3", -40);
  });

  function parallaxPrize(e, target, movement) {
    var $this = $("#prize");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }


  $("#judge-info").mousemove(function(e) {
    parallaxJudge(e, "#judge-1", -100);
    parallaxJudge(e, "#judge-2", 100);
    parallaxJudge(e, "#judge-3", -100);
    parallaxJudge(e, "#judge-4", 100);
  });

  function parallaxJudge(e, target, movement) {
    var $this = $("#judge-info");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }


  $("#youridea").mousemove(function(e) {
    parallaxYourIdea(e, "#img-youridea-1", -40);
    parallaxYourIdea(e, "#img-youridea-2", 20);
    parallaxCriteria(e, "#img-youridea-3", -20);
    parallaxCriteria(e, "#img-youridea-4", 40);
    parallaxCriteria(e, "#img-youridea-5", -20);
    parallaxCriteria(e, "#img-youridea-6", 40);
  });

  function parallaxYourIdea(e, target, movement) {
    var $this = $("#youridea");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }


  $("#criteria").mousemove(function(e) {
    parallaxCriteria(e, "#img-criteria-1", -40);
    parallaxCriteria(e, "#img-criteria-2", 20);
    parallaxCriteria(e, "#img-criteria-3", -20);
    parallaxCriteria(e, "#img-criteria-4", 40);
  });

  function parallaxCriteria(e, target, movement) {
    var $this = $("#criteria");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }

  $("#pengumuman").mousemove(function(e) {
    parallaxCriteria(e, "#img-pengumuman-1", -40);
    parallaxCriteria(e, "#img-pengumuman-2", 20);
    parallaxCriteria(e, "#img-pengumuman-3", -20);
    parallaxCriteria(e, "#img-pengumuman-4", 40);
  });

  function parallaxCriteria(e, target, movement) {
    var $this = $("#pengumuman");
    var relX = e.pageX - $this.offset().left;
    var relY = e.pageY - $this.offset().top;

    TweenMax.to(target, 1, {
      x: (relX - $this.width() / 2) / $this.width() * movement,
      y: (relY - $this.height() / 2) / $this.height() * movement
    });
  }

  $('#judge-1').mouseover(function(){
      $('.buble').show();
  });

  $('.buble').mouseleave(function(){
      $('.buble').hide();
  });

  $('#judge-2').mouseover(function(){
      $('.buble2').show();
  });

  $('.buble2').mouseleave(function(){
      $('.buble2').hide();
  });

  $('#judge-3').mouseover(function(){
      $('.buble3').show();
  });

  $('.buble').mouseleave(function(){
      $('.buble3').hide();
  });


});
</script>


<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="justify-content-start text-left">
                        <img src="<?php echo base_url(IMAGES.'logo.png') ?>" class="img-fluid side-logo" alt=""/>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-right pt-5">
                    <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG.'HOME.png') ?>"onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" alt="" style="height:18px" /></a><br>
                    <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" alt=""   style="height:18px" /></a>
                    <img src="<?php echo base_url(IMG.'Group 3288.png') ?>" class="img-fluid px-2 mbm" alt=""   style="height:30px" />
                        <img src="<?php echo base_url(IMAGES.'man 1.png') ?>" alt="" style="position:absolute; width:193px;left:10px;bottom:0" >
                        <img src="<?php echo base_url(IMAGES.'brush-nav.png') ?>" alt="" style="position:absolute; right:0;top:220px" >
                </div>
                
            </div>
        </div>
    </div>
    

<!-- HEADER -->
<div class="container-fluid header pt-4 px-0"  id="header" >
  <div class="container">
    <div class="row text-center banner">

      <div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
      <button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG.'Burger.png') ?>" class="img-fluid" alt="" /></button>
      </div>


      <div class="col-md-6 offset-md-6 mb-5 text-right d-none d-md-block">

          <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG.'HOME.png') ?>"onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" alt="" style="height:18px" /></a>
          <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" alt=""   style="height:18px" /></a>
          <img src="<?php echo base_url(IMG.'Group 3288.png') ?>" class="img-fluid px-2" alt=""   style="height:26px"  />

      </div>
      
      <div class="col-md-12 mb-5  d-none d-md-block" data-aos="zoom-in">
        <img  src="<?php echo base_url(IMG.'header.png') ?>" class="img-fluid" alt="" />
      </div>

      <div class="col-12 mb-3  d-md-none d-block" data-aos="zoom-in">
        <img src="<?php echo base_url(IMG.'express the change.png') ?>" class="img-fluid" alt="" />
      </div>
      

      <div class="col-md-4 text-left d-none d-md-block" style="position: relative;" data-aos="zoom-out-right">
        <img  src="<?php echo base_url(IMG.'hand.gif') ?>" class="hand-new" alt="" style="width:450px " />
      </div>

      <div class="col-6 offset-3 col-md-4 offset-md-0 mt-0 mt-md-5" data-aos="zoom-out">
        <img   src="<?php echo base_url(IMG.'change packaging.png') ?>" class="img-fluid" alt="" />
      </div>

      <div class="col-6 col-md-4 text-left d-block d-md-none" data-aos="zoom-out-right">
        <img src="<?php echo base_url(IMG.'hand.gif') ?>" class="hand-new" alt="" style="width:250px" />
      </div>

      <div class="col-6 col-md-4 text-right" data-aos="zoom-out">
        <img id="img-header-3"  src="<?php echo base_url(IMG.'pack.gif') ?>" class="img-fluid" alt="" style="width:330px" />
      </div>

    </div>
  </div>
  <div style="position:relative">
      <div style="position:absolute;bottom:-160px;right:0px;z-index:1000" class="d-none d-md-block">
        <img src="<?php echo base_url(IMG.'yellow bruh.png') ?>" alt="" class="img-fluid" style="width:300px" >
      </div>

      <div style="position:absolute;bottom:-50px;right:0px;z-index:1000" class="d-block d-md-none">
        <img src="<?php echo base_url(IMG.'yellow bruh.png') ?>" alt="" class="img-fluid" style="width:100px" >
      </div>

      <div style="position:absolute;bottom:-1px;z-index:999">
        <img src="<?php echo base_url(IMG.'participant-bg-top.png') ?>" alt="" style="width:100%" >
      </div>

  </div>
</div>


<!-- PARTICIPANT CATEGORIES -->
<div class="container-fluid participant-cat  pb-5" id="participant-info">
  <div class="container">
    <div class="row text-center">
      
      <div class="col-md-12 mb-5 pb-3" data-aos="zoom-out">
        <img src="<?php echo base_url(IMG.'PARTICIPANT-CATEGORIES.png') ?>" class="img-fluid" alt="" />
      </div>

      <div class="col-6 col-md-4 offset-md-2" >
        <div class="row mb-4" data-aos="zoom-in-right">
          <div class="col-md-12"  style="z-index:1000">
          <a href="<?php echo base_url('register?cat=general') ?>"><img id="general-bt" src="<?php echo base_url(IMG.'General.png') ?>" class="img-fluid" alt="" style="width:250px" /></a>
          </div>
        </div>
        <div class="row mt-5" data-aos="zoom-in">
          <div class="col-md-12"  style="z-index:999">
          <img src="<?php echo base_url(IMG.'general-text-new.png') ?>" class="img-fluid" alt="" />
          </div>
        </div>
      </div>

      <div class="col-6 col-md-4">
       <div class="row  mb-4" data-aos="zoom-out-right">
          <div class="col-md-12"  style="z-index:1000" >
          <a href="<?php echo base_url('register?cat=students') ?>"><img id="students-bt" src="<?php echo base_url(IMG.'Students.png') ?>" class="img-fluid" alt="" style="width:250px" /></a>
          </div>
        </div>
        <div class="row mt-5" data-aos="zoom-out">
          <div class="col-md-12" style="z-index:999" >
          <img src="<?php echo base_url(IMG.'students-text-new.png') ?>" class="img-fluid" alt=""   />
          </div>
        </div>
      </div>

    </div>
  </div>
  <div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom">
			<img src="<?php echo base_url(IMG.'participant-bg-bottom.png') ?>" alt="" style="width:100%">
		</div>
	</div>
  <div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile">
			<img src="<?php echo base_url(IMG.'participant-bg-bottom.png') ?>" alt="" style="width:100%">
		</div>
	</div>
</div>




<!-- IMPORTANT DATES -->
<div class="container-fluid important-dates py-5">
  <div class="container">
    <div class="row text-center">
      
      <div class="col-md-12 mb-md-5 mb-4" data-aos="zoom-out">
        <img src="<?php echo base_url(IMG.'important-date-new.png') ?>" class="img-fluid" alt="" />
      </div>

      <div class="col-md-12 mb-5" id="pengumuman">
        <div class="row">
          <div class="col-6 col-md-3 mb-4" data-aos="zoom-in-right">
            <img id="img-pengumuman-1" src="<?php echo base_url(IMG.'submit design.png') ?>" class="img-fluid" alt=""/>
          </div>
          <div class="col-6 col-md-3 mb-4" data-aos="zoom-out">
            <img id="img-pengumuman-2" src="<?php echo base_url(IMG.'begin voting.png') ?>" class="img-fluid" alt=""/>
          </div>
          <div class="col-6 col-md-3" data-aos="zoom-in">
            <img id="img-pengumuman-3" src="<?php echo base_url(IMG.'20 finalist.png') ?>" class="img-fluid" alt=""/>
          </div>
          <div class="col-6 col-md-3" data-aos="zoom-out-right">
            <img id="img-pengumuman-4" src="<?php echo base_url(IMG.'winner.png') ?>" class="img-fluid" alt=""/>
          </div>
        </div>
      </div>

      <div class="col-8 offset-2 col-md-4 offset-md-4">
      <a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_new">

      <img src="<?php echo base_url(IMG.'Instagram.png') ?>" class="img-fluid" alt="" /></a>
      </div>

    </div>
  </div>
</div>


<!-- PRIZES -->
<div class="container-fluid prizes" id="prize">
  <div class="container">
    <div class="row text-center">
      
      <div class="col-md-12  d-md-block d-none" data-aos="zoom-out-up">
        <img src="<?php echo base_url(IMG.'prize.png') ?>" class="img-fluid" alt="" />
      </div>

      <div class="col-12 mt-5 mb-4  d-block d-md-none" data-aos="zoom-out-up">
        <img src="<?php echo base_url(IMG.'prize-mobile.png') ?>" class="img-fluid" alt="" />
      </div>


     

      <div class="col-12 d-md-none d-block">
        <div class="row mb-4">
          <div class="col-6" data-aos="zoom-in-right">
          <img src="<?php echo base_url(IMG.'Group 3247.png') ?>" class="img-fluid" alt=""/>
          </div>
          <div class="col-6" data-aos="zoom-in">
          <img src="<?php echo base_url(IMG.'Group 3247-2.png') ?>" class="img-fluid" alt=""/>
          </div>
        </div>
      </div>

      <div class="col-12 col-md-9" data-aos="fade-in">
        <img id="img-prize-1" src="<?php echo base_url(IMG.'winner 1.png') ?>" class="img-fluid" alt=""/>
      </div>

    

      <div class="col-md-3 mt-5 d-md-block d-none">
        <div class="row">
          <div class="col-md-12 mb-5" data-aos="fade-in">
          <img id="img-prize-2" src="<?php echo base_url(IMG.'hadiah.png') ?>" class="img-fluid" alt=""/>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" data-aos="fade-in">
          <img id="img-prize-3" src="<?php echo base_url(IMG.'hadiah-1.png') ?>" class="img-fluid" alt=""/>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- JUDGE INFO -->
<div class="container-fluid judge-info  py-md-5 pt-5 pb-0" id="judge-info">
  <div class="container">
    <div class="row text-center">
      
      <div class="col-md-12 mb-md-5 mb-0" data-aos="zoom-in">
        <img src="<?php echo base_url(IMG.'judge-info-new.png') ?>" class="img-fluid" alt="" />
      </div>


      <div class="col-md-12 mb-5 py-5 d-none d-md-block">
        <div class="row">
          <div class="col-4 col-md-4" >
            <div style="position: relative;">
          </div>
              <img src="<?php echo base_url(IMAGES.'buble-1.png') ?>" class="img-fluid buble" alt=""/>
              <div style="position: relative;" class="buble">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#mandy-profile">
                  <img src="<?php echo base_url(IMAGES.'see-profile.png') ?>" class="img-fluid" style="position:absolute;bottom:6rem;left:7rem;">
                </a>
              </div>
              <img  src="<?php echo base_url(IMG.'Group 3282.png') ?>" class="img-fluid " id="judge-1" alt="" data-aos="zoom-in-right"/>
            </div>
          <div class="col-4 col-md-4" >
              <img src="<?php echo base_url(IMAGES.'buble-2.png') ?>" class="img-fluid buble2" alt=""/>
              <div style="position: relative;" class="buble2">
                      <a href="javascript:void(0)" data-toggle="modal" data-target="#muklay-profile">
                        <img src="<?php echo base_url(IMAGES.'see-2.png') ?>" class="img-fluid" style="position:absolute;bottom:5.5rem;left:7rem;width:150px;">
                      </a>
              </div>
              <img  src="<?php echo base_url(IMG.'Group 3280.png') ?>" class="img-fluid" id="judge-2" alt="" data-aos="zoom-in"/>
              
          </div>
          <div class="col-4 col-md-4">
            <img src="<?php echo base_url(IMG.'Group 3289.png') ?>" class="img-fluid buble3" alt=""/>
             <div style="position: relative;" class="buble3">
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#gofar-profile">
                    <img src="<?php echo base_url(IMAGES.'see-2.png') ?>" class="img-fluid" style="position:absolute;bottom:4rem;left:5rem;">
                  </a>
              </div>
              <img src="<?php echo base_url(IMG.'Group 3281.png') ?>" class="img-fluid" id="judge-3" alt="" data-aos="zoom-in"/>
           
          </div>
        </div>
      </div>

      <div class="col-md-12 mb-5 py-md-5 py-4  d-md-none d-block ">
        <div class="row">
          <div class="col-4 col-md-4 p-1 pt-5" style="position: relative;">
            <img src="<?php echo base_url(IMG.'Group 3282.png') ?>" class="img-fluid" alt="" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-out-right"/>
          </div>
          <div class="col-8 col-md-8 p-1 ">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#mandy-profile"><img src="<?php echo base_url(IMAGES.'buble-1-mob.png') ?>" class="img-fluid" alt="" data-aos="zoom-out"/></a>
          </div>

          <div class="col-8 col-md-8 p-1 pt-5" >
            <a href="javascript:void(0)" data-toggle="modal" data-target="#muklay-profile"><img src="<?php echo base_url(IMAGES.'buble-2-mob.png') ?>" class="img-fluid" alt="" data-aos="zoom-out"/></a>
          </div>
          <div class="col-4 col-md-4 p-1 " style="position: relative;">
            <img src="<?php echo base_url(IMG.'Group 3280.png') ?>" class="img-fluid" alt="" style="position: absolute; top:7rem; left:0rem;" data-aos="zoom-out-right"/>
          </div>

          <div class="col-4 col-md-4 p-1 pt-5" style="position: relative;">
            <img src="<?php echo base_url(IMG.'Group 3281.png') ?>" class="img-fluid" alt="" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-out-right"/>
          </div>
          <div class="col-8 col-md-8 p-1 ">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#gofar-profile"><img src="<?php echo base_url(IMAGES.'buble-3-mob.png') ?>" class="img-fluid" alt="" data-aos="zoom-out"/></a>
          </div>
        </div>
        
      </div>


    </div>
  </div>
</div>


<!-- YOUR IDEA -->
<div class="container-fluid your-canvas-your-idea py-md-5 py-0 px-0" id="youridea" >
<div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom-can-top" >
			<!-- <img src="<?php echo base_url(IMG.'participant-bg-top.png') ?>" alt="" style="width:100%"> -->
		</div>
	</div>
  <div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile-can-top">
			<!-- <img src="<?php echo base_url(IMG.'participant-bg-top.png') ?>" alt="" style="width:100%"> -->
		</div>
	</div>
  <div style="position:relative">
      <div style="position:absolute;top:20px;right:0px" class="d-none d-md-block">
        <img src="<?php echo base_url(IMG.'green brush.png') ?>" alt="" class="img-fluid" style="width:120px" >
      </div>
  </div>
  <div class="container">
    <div class="row text-center">
      
      <div class="col-md-12 mb-md-5 mb-3" data-aos="zoom-in-up">
        <img src="<?php echo base_url(IMG.'your-canvas-your-idea.png') ?>" class="img-fluid" alt="" />
      </div>


      <div class="col-md-7">
        <div class="row">
          <div class="col-md-12  mb-4" data-aos="zoom-in-right">
            <img id="img-youridea-1" src="<?php echo base_url(IMG.'your-canvas-your-idea-text.png') ?>" class="img-fluid" alt=""/>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12  mb-3" data-aos="zoom-out-right">
          <a href="<?php echo base_url(DOCS.'Esse-Change-Guide-Competition.zip') ?>"><img  id="img-youridea-2" src="<?php echo base_url(IMG.'Join now.png') ?>" class="img-fluid" alt="" style="width:350px" /></a>
          </div>
        </div>
      </div>

      <div class="col-md-5" data-aos="zoom-in">
        <img src="<?php echo base_url(IMG.'pruduct.png') ?>" class="img-fluid" alt="" style="width:370px"  />
      </div>

    </div>
  </div>

  <div style="position:relative">
      <div style="position:absolute;bottom:-250px;right:0px"  class="d-none d-md-block">
        <img src="<?php echo base_url(IMG.'purple brush.png') ?>" alt="" class="img-fluid" style="width:230px" >
      </div>

      <div style="position:absolute;bottom:-80px;left:0px"  class="d-none d-md-block">
        <img src="<?php echo base_url(IMG.'red brush.png') ?>" alt="" class="img-fluid" style="width:230px" >
      </div>
  </div>
  <div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom-can">
			<!-- <img src="<?php echo base_url(IMG.'participant-bg-bottom.png') ?>" alt="" style="width:100%"> -->
		</div>
	</div>
  <div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile-can">
			<!-- <img src="<?php echo base_url(IMG.'participant-bg-bottom.png') ?>" alt="" style="width:100%"> -->
		</div>
	</div>
</div>


<!-- COMPETITION CRITERIA -->
<div class="container-fluid competition-criteria pb-5"  >
  <div class="container">
    <div class="row text-center">
      
      <div class="col-md-6 d-none d-md-block" >
        <div class="row">
          <div class="col-md-12  mb-4" style="margin-top:10rem;" data-aos="zoom-in-right">
            <img id="img-criteria-1" src="<?php echo base_url(IMG.'Group 3252.png') ?>" class="img-fluid" alt="" style="width:350px" />
          </div>
        </div>
        <div class="row">
          <div class="col-md-12  mb-3" data-aos="zoom-out-right">
          <a href="<?php echo base_url('register') ?>"><img id="img-criteria-2" src="<?php echo base_url(IMG.'register.png') ?>" class="img-fluid" alt="" style="width:350px" /></a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12" data-aos="zoom-in-right">
          <a href="<?php echo base_url(DOCS.'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>" target="_new"><img id="img-criteria-3" src="<?php echo base_url(IMG.'download.png') ?>" class="img-fluid" alt="" style="width:350px" /></a>
          </div>
        </div>
      </div>

      <div class="col-md-6" style="margin-top:10rem;" data-aos="zoom-in">
        <img id="img-criteria-4" src="<?php echo base_url(IMG.'comp-criteria-text.png') ?>" class="img-fluid" alt=""/>
      </div>

      <div class="col-md-6 d-block d-md-none">
        <div class="row">
          <div class="col-8 offset-2 col-md-12 offset-md-0  mb-4" data-aos="zoom-in-right">
            <img src="<?php echo base_url(IMG.'Group 3252.png') ?>" class="img-fluid" alt="" style="width:350px" />
          </div>
        </div>
        <div class="row">
          <div class="col-8 offset-2 col-md-12 offset-md-0 mb-3" data-aos="zoom-out-right">
          <a href="<?php echo base_url('register') ?>"><img src="<?php echo base_url(IMG.'register.png') ?>" class="img-fluid" alt="" style="width:350px" /></a>
          </div>
        </div>
        <div class="row">
          <div class="col-8 offset-2 col-md-12 offset-md-0 " data-aos="zoom-up-right"> 
          <a href="#"><img src="<?php echo base_url(IMG.'download.png') ?>" class="img-fluid" alt="" style="width:350px" /></a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- MAKE CHANGE -->
<div class="container-fluid make-change py-5">
  <div class="container">
    <div class="row text-center">

      <div class="col-md-8 mb-3 text-left" data-aos="zoom-out">
        <img src="<?php echo base_url(IMG.'makechange-text.png') ?>" class="img-fluid" alt=""/>
      </div>
      <div class="col-md-4">   
      </div>

      <div class="col-6 offset-3 col-md-8 offset-md-2" data-aos="zoom-in">
      <a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_blank"><img src="<?php echo base_url(IMG.'Instagram-1.png') ?>" class="img-fluid" alt="" style="width:250px" /></a>
      </div>

    </div>
  </div>
</div>


<!-- FOOTER -->
<div class="container-fluid footer py-2">
  <div class="container">
    <div class="row">

      <div class="col-4 col-md-6 text-left">
        <img src="<?php echo base_url(IMG.'esse-logo-footer.png') ?>" class="img-fluid" alt=""/>
      </div>
   
      <div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
        <img src="<?php echo base_url(IMG.'alr-footer.png') ?>" class="img-fluid" alt=""/>
      </div>

    </div>
  </div>
</div>



<!-- Muklay Profile Modal -->
<div class="modal fade" id="muklay-profile" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center mb-5" >
        <img src="<?php echo base_url(IMG.'muklay-profile-new.png') ?>" alt="" class="img-fluid" />
      </div>
    </div>
  </div>
</div>

<!-- Mandy Profile Modal -->
<div class="modal fade" id="mandy-profile" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center mb-5" >
        <img src="<?php echo base_url(IMG.'mandy-profile-new.png') ?>" alt="" class="img-fluid" />
      </div>
    </div>
  </div>
</div>

<!-- Gofar Profile Modal -->
<div class="modal fade" id="gofar-profile" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center mb-5" >
        <img src="<?php echo base_url(IMG.'gofar-profile-new.png') ?>" alt="" class="img-fluid" />
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$('#img-youridea-2').click(function(){
  $.ajax({
        type: "POST",
        dataType: "html",
        url:'<?php echo site_url('backend/dashboard/countDownload')?>',
        data: { file_type:+'1' },
        beforeSend: function() {},
        success: function(msg){}
  });
}); 

$('#img-criteria-3').click(function(){
  $.ajax({
        type: "POST",
        dataType: "html",
        url:'<?php echo site_url('backend/dashboard/countDownload')?>',
        data: { file_type:+'2' },
        beforeSend: function() {},
        success: function(msg){}
  });
}); 
</script>