<style>
    .modal-dialog {
        min-height: calc(100vh - 60px);
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
    @media(max-width: 768px) { 
    .modal-dialog {
        min-height: calc(100vh - 20px);
    }
    } 
</style>
<script>
    // brand
    function hover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'brand-hov.png') ?>');
    }

    function unhover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMG.'BRAND STORY.png') ?>');
    }
    // competition
    function hover_com(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'com-hov.png') ?>');
    }

    function unhover_com(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'COMPETITION.png') ?>');
    }

    $(document).ready(function() {
        $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        })

        
        $('#myModal').modal('show');

        month = $("#dob_month").val();
        year = $("#dob_year").val();
        day = $("#dob_day").val();

        if (month != '' && year != '' && day != '')
            $("#submit_bt").removeAttr("disabled");
        else
            $("#submit_bt").attr("disabled", "disabled");
        $('[data-fancybox]').fancybox({
            buttons: [
                "zoom",
                "thumbs",
                "close"
            ],
            hideScrollbar: false,
        });



        $("#dob_month").on("change", function() {
            v = $(this).val();
            year = $("#dob_year").val();
            day = $("#dob_day").val();
            if (v != '' && year != '' && day != '') {
                validateAge();
            }
        });


        $("#dob_year").on("change", function(event) {
            v = $(this).val();
            month = $("#dob_month").val();
            day = $("#dob_day").val();
            if (v != '' && month != '' && day != '') {
                validateAge();
            }
        });

        $("#dob_day").on("change", function(event) {
            v = $(this).val();
            year = $("#dob_year").val();
            month = $("#dob_month").val();
            if (v != '' && month != '' && year != '') {
                validateAge();
            }
        });

        // function parallaxParticipant(e, target, movement) {
        //     var $this = $("#banner-new");
        //     var relX = e.pageX - $this.offset().left;
        //     var relY = e.pageY - $this.offset().top;

        //     TweenMax.to(target, 1, {
        //     x: (relX - $this.width() / 1) / $this.width() * movement,
        //     y: (relY - $this.height() / -1) / $this.height() * movement
        //     });
        // }

        // $("#banner-new").mousemove(function(e) {
        //     parallaxParticipant(e, "#btn-reg", -1);
        // });

    });        
</script>

<script>
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    

    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            playerVars: {
                rel:0,
                autoplay:1,
                enablejsapi: 1,
                disablekb: 1,
                showinfo: 0,
                controls: 0,
                fs: 0,
                
            },
            height: '100%',
            width: '100%',
            videoId: 'dyMe8-MKXag',
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange
            }
        });
    }
    function onPlayerReady(event) {
        event.target.playVideo();
    }
    var done = false;
    function onPlayerStateChange(event) {
        if(event.data === YT.PlayerState.ENDED) {          
                stopVideo();
        }
    }
    function stopVideo() {
        player.stopVideo();
        $('#vtc').modal('hide');
    }

    function validateAge() {

        var month = parseInt($("#dob_month").val());
        var year = parseInt($("#dob_year").val());
        var age = 18;
        var setDate = new Date(year + age, month - 1, 1);
        var currdate = new Date();

        if (currdate >= setDate) {
            $('#myModal').modal('hide');
            $('#vtc').modal('show');
			setCookie();
			player.playVideo();
            

        } else {
            $('#validate-age').hide();
            $('#sorry-message').show();
        }
    };



    function close_window() {
        window.open('', '_parent', '');
        window.close();
    }

    function setCookie()
    {   
        url = "<?php echo site_url('Home/SetCookie')?>";
        $.ajax({
            url : url,
            type: "POST",
            dataType: "JSON", 
            success: function(data)
            {
                console.log();
            },
            
        });
    }

</script>


<!-- CONFIRM AGE -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body text-center p-3 pt-4 pb-5">

				<div class="row" id="validate-age">
					<div class="col-md-12 mb-4 mt-3">
						<img src="<?= base_url(); ?>assets/images/welcome.png" class="img-welcome" /> <br>
						<img src="<?= base_url(); ?>assets/images/text-age.png" class="img-fluid mt-3" alt="">
					</div>

					<div class="col-12 col-md-12 mb-2 text-center">
						<select name="dob_day" id="dob_day" required>
							<option value="">Date</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>
						</select>
						<select name="dob_month" id="dob_month" required>
							<option value="">Month</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>

						<select name="dob_year" id="dob_year" required>
							<option value="">Year</option>
							<option value="2003">2003</option>
							<option value="2002">2002</option>
							<option value="2001">2001</option>
							<option value="2000">2000</option>
							<option value="1999">1999</option>
							<option value="1998">1998</option>
							<option value="1997">1997</option>
							<option value="1996">1996</option>
							<option value="1995">1995</option>
							<option value="1994">1994</option>
							<option value="1993">1993</option>
							<option value="1992">1992</option>
							<option value="1991">1991</option>
							<option value="1990">1990</option>
							<option value="1989">1989</option>
							<option value="1988">1988</option>
							<option value="1987">1987</option>
							<option value="1986">1986</option>
							<option value="1985">1985</option>
							<option value="1984">1984</option>
							<option value="1983">1983</option>
							<option value="1982">1982</option>
							<option value="1981">1981</option>
							<option value="1980">1980</option>
							<option value="1979">1979</option>
							<option value="1978">1978</option>
							<option value="1977">1977</option>
							<option value="1976">1976</option>
							<option value="1975">1975</option>
							<option value="1974">1974</option>
							<option value="1973">1973</option>
							<option value="1972">1972</option>
							<option value="1971">1971</option>
							<option value="1970">1970</option>
							<option value="1969">1969</option>
							<option value="1968">1968</option>
							<option value="1967">1967</option>
							<option value="1966">1966</option>
							<option value="1965">1965</option>
							<option value="1964">1964</option>
							<option value="1963">1963</option>
							<option value="1962">1962</option>
							<option value="1961">1961</option>
							<option value="1960">1960</option>
							<option value="1959">1959</option>
							<option value="1958">1958</option>
							<option value="1957">1957</option>
							<option value="1956">1956</option>
							<option value="1955">1955</option>
							<option value="1954">1954</option>
							<option value="1953">1953</option>
							<option value="1952">1952</option>
							<option value="1951">1951</option>
							<option value="1950">1950</option>
							<option value="1949">1949</option>
							<option value="1948">1948</option>
							<option value="1947">1947</option>
							<option value="1946">1946</option>
							<option value="1945">1945</option>
							<option value="1944">1944</option>
							<option value="1943">1943</option>
							<option value="1942">1942</option>
							<option value="1941">1941</option>
							<option value="1940">1940</option>
						</select>
					</div>

				</div>

				<div class="row" id="sorry-message" style="display:none">
					<div class="col-md-12">
						<img src="assets/images/text-age.png" class="img-fluid" />
					</div>
					<div class="col-md-12 mt-4">
						<a type="button" href="#" onclick="close_window();return false;"
							class="btn btn-primary">Close</a>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<!-- MODAL VTC -->
<div class="modal  fade" id="vtc" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content modal-vtc">

			<div class="modal-body vtc text-center ">

				<div class="embed-responsive embed-responsive-16by9">
					<div id="player"></div>
					
				</div>
			</div>
		</div>
	</div>
</div>


<!-- MOBILE MENU -->
<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES.'logo.png') ?>" class="img-fluid side-logo" alt="" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<img src="<?php echo base_url(IMAGES.'HOME.png') ?>" class="img-fluid px-3 mbm" alt=""
					style="height:30px" />
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>"
						onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm"
						alt="" style="height:18px" /></a>
				<a href="<?php echo base_url('competition') ?>"><img
						src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>" onmouseover="hover_com(this);"
						onmouseout="unhover_com(this);" class="img-fluid px-2 mbm" alt="" style="height:18px" /></a>
				<img src="<?php echo base_url(IMAGES.'man 1.png') ?>" alt=""
					style="position:absolute; width:193px;left:10px;bottom:0">
				<img src="<?php echo base_url(IMAGES.'brush-nav.png') ?>" alt=""
					style="position:absolute; right:0;top:220px">
			</div>

		</div>
	</div>
</div>

<!-- HEADER -->
<div class="container-fluid header  head pt-4 px-0" id="header">
	<div class="container">
		<div class="row text-center">

			<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
				<button style="background:transparent" type="button" class="btn" data-toggle="modal"
					data-target="#right_modal_sm"><img src="<?php echo base_url(IMG.'Burger.png') ?>" class="img-fluid"
						alt="" /></button>
			</div>
			<!-- menu -->
			<div class="col-md-6 offset-md-6 mb-5 text-right d-none d-md-block">
				<img src="<?php echo base_url(IMAGES.'HOME.png') ?>" class="img-fluid px-3" alt=""
					style="height:30px" />
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>"
						onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" alt=""
						style="height:18px" /></a>
				<a href="<?php echo base_url('competition') ?>"><img
						src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>" onmouseover="hover_com(this);"
						onmouseout="unhover_com(this);" class="img-fluid px-2" alt="" style="height:18px" /></a>
			</div>
		</div>
	</div>
</div>

<!-- home -->
<div class="home">
	<div class="d-none d-md-block">
		<img src="<?php echo base_url(IMAGES.'Home9.gif') ?>" class="img-fluid banner-home" />
		<div style="position:relative">
			<div class="join-new d-none d-md-block">
				<a href="<?php echo base_url('register') ?>"> <img src="<?php echo base_url(IMAGES.'join.png') ?>"
						class="img-fluid" id="btn-reg" alt="" style="width:230px" /></a>
			</div>
		</div>
	</div>
	<div class="col-md-12 d-block d-md-none text-center  d-flex justify-content-center">
		<img src="<?php echo base_url(IMAGES.'home-mob.gif') ?>" class="ban-mobile img-fluid" alt="" />
	</div>
	<div style="position:relative">
		<div class="join-mob d-block d-md-none">
			<a href="<?php echo base_url('register') ?>"> <img src="<?php echo base_url(IMAGES.'join.png') ?>"
					class="img-fluid reg" alt="" /></a>
		</div>
		<div class="milion d-block d-md-none text-center  justify-content-center" style="margin-bottom: 2rem;">
			<img src="<?php echo base_url(IMAGES.'p.png') ?>" class="img-fluid" alt="" width="150px" />
		</div>
		<div style="position:absolute;bottom:33px;left:-40px;z-index:1000" class="d-block d-md-none">
		<img src="<?php echo base_url(IMAGES.'brush-mob.gif') ?>" alt="" class="img-fluid" style="width:200px">
		</div>
	</div>

	<div style="position:relative">
		<div style="position:absolute;bottom:-24px;right:0px;z-index:-1" class="d-block d-md-none">
			<img src="<?php echo base_url(IMAGES.'woman 1.png') ?>" alt="" class="img-fluid" style="width: 88px;">
		</div>
	</div>
</div>


<!-- footer -->
<div class="container-fluid footer-peringatan" style="z-index:1">

	<div class="row">
		<div class="col-md-2 col-sm-2 p-0 text-left">
			<img src="<?php echo base_url(IMAGES.'bolong.png') ?>" class="img-fluid footer-bolong" alt="" />
		</div>
		<div class="col-md-8 col-sm-8 text-center  d-flex justify-content-center">
			<img src="<?php echo base_url(IMAGES.'peringatan.svg') ?>" class="img-fluid footer-center" alt="" />
		</div>
		<div class="col-md-2 col-sm-2 text-right  d-flex justify-content-end">
			<img src="<?php echo base_url(IMAGES.'18+.svg') ?>" class="img-fluid footer-18" alt="" />
		</div>
	</div>

</div>

<div class="container-fluid footer py-2">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG.'esse-logo-footer.png') ?>" class="img-fluid fl" alt="" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG.'alr-footer.png') ?>" class="img-fluid fr" alt="" />
			</div>

		</div>
	</div>
</div>