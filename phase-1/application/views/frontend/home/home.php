<script>
    // brand
    function hover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'brand-hov.png') ?>');
    }

    function unhover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMG.'BRAND STORY.png') ?>');
    }
    // competition
    function hover_com(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'com-hov.png') ?>');
    }

    function unhover_com(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES.'COMPETITION.png') ?>');
    }    
	$(document).ready(function () {
	AOS.init();
	});  
</script>
<!-- MOBILE MENU -->
<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES.'logo.png') ?>" class="img-fluid side-logo" alt="" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<img src="<?php echo base_url(IMAGES.'HOME.png') ?>" class="img-fluid px-3 mbm" alt=""
					style="height:30px" />
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>"
						onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm"
						alt="" style="height:18px" /></a>
				<a href="<?php echo base_url('competition') ?>"><img
						src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>" onmouseover="hover_com(this);"
						onmouseout="unhover_com(this);" class="img-fluid px-2 mbm" alt="" style="height:18px" /></a>
				<img src="<?php echo base_url(IMAGES.'man 1.png') ?>" alt=""
					style="position:absolute; width:193px;left:10px;bottom:0">
				<img src="<?php echo base_url(IMAGES.'brush-nav.png') ?>" alt=""
					style="position:absolute; right:0;top:220px">
			</div>

		</div>
	</div>
</div>
<!-- HEADER -->
<div class="container-fluid header head pt-4 px-0" id="header">
	<div class="container">
		<div class="row text-center">

			<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
				<button style="background:transparent" type="button" class="btn" data-toggle="modal"
					data-target="#right_modal_sm"><img src="<?php echo base_url(IMG.'Burger.png') ?>" class="img-fluid"
						alt="" /></button>
			</div>
			<!-- menu -->
			<div class="col-md-6 offset-md-6 mb-5 text-right d-none d-md-block">
				<img src="<?php echo base_url(IMAGES.'HOME.png') ?>" class="img-fluid px-3" alt=""
					style="height:30px" />
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>"
						onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" alt=""
						style="height:18px" /></a>
				<a href="<?php echo base_url('competition') ?>"><img
						src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>" onmouseover="hover_com(this);"
						onmouseout="unhover_com(this);" class="img-fluid px-2" alt="" style="height:18px" /></a>
			</div>
		</div>
	</div>
</div>

<!-- home -->
<div class="home">
	<div class="d-none d-md-block">
		<img src="<?php echo base_url(IMAGES.'Home9.gif') ?>" class="img-fluid banner-home" />
		<div style="position:relative">
			<div class="join-new d-none d-md-block">
				<a href="<?php echo base_url('register') ?>"> <img src="<?php echo base_url(IMAGES.'join.png') ?>"
						class="img-fluid" id="btn-reg" alt="" style="width:230px" /></a>
			</div>
		</div>
	</div>
	<div class="col-md-12 d-block d-md-none text-center  d-flex justify-content-center" data-aos="zoom-in">
		<img src="<?php echo base_url(IMAGES.'home-mob.gif') ?>" class="ban-mobile img-fluid" alt="" />
	</div>
	<div style="position:relative">
		<div class="join-mob d-block d-md-none" data-aos="zoom-in-up">
			<a href="<?php echo base_url('register') ?>"> <img src="<?php echo base_url(IMAGES.'join.png') ?>"
					class="img-fluid reg" alt="" /></a>
		</div>
		<div class="milion d-block d-md-none text-center  justify-content-center" style="margin-bottom: 2rem;">
			<img src="<?php echo base_url(IMAGES.'p.png') ?>" class="img-fluid" alt="" width="150px" />
		</div>
		<div style="position:absolute;bottom:33px;left:-40px;z-index:1000" class="d-block d-md-none">
			<img src="<?php echo base_url(IMAGES.'brush-mob.gif') ?>" alt="" class="img-fluid" style="width:200px">
		</div>
	</div>

	<div style="position:relative">
		<div style="position:absolute;bottom:-24px;right:0px;z-index:-1" class="d-block d-md-none" >
			<img src="<?php echo base_url(IMAGES.'woman 1.png') ?>" alt="" class="img-fluid" style="width: 88px;">
		</div>
	</div>
</div>


<!-- footer -->
<div class="container-fluid footer-peringatan" style="z-index:1">

	<div class="row">
		<div class="col-md-2 col-sm-2 p-0 text-left">
			<img src="<?php echo base_url(IMAGES.'bolong.png') ?>" class="img-fluid footer-bolong" alt="" />
		</div>
		<div class="col-md-8 col-sm-8 text-center  d-flex justify-content-center">
			<img src="<?php echo base_url(IMAGES.'peringatan.svg') ?>" class="img-fluid footer-center" alt="" />
		</div>
		<div class="col-md-2 col-sm-2 text-right  d-flex justify-content-end">
			<img src="<?php echo base_url(IMAGES.'18+.svg') ?>" class="img-fluid footer-18" alt="" />
		</div>
	</div>

</div>

<div class="container-fluid footer py-2">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG.'esse-logo-footer.png') ?>" class="img-fluid fl" alt="" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG.'alr-footer.png') ?>" class="img-fluid fr" alt="" />
			</div>

		</div>
	</div>
</div>
