      <style>
        .modal-dialog {
            min-height: calc(100vh - 60px);
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        @media(max-width: 768px) {
          .modal-dialog {
            min-height: calc(100vh - 20px);
          }
        } 
      </style>
      <script>
        // home 
        function hover(element) {
        element.setAttribute('src', '<?php echo base_url(IMAGES.'home-hov.png') ?>');
        }

        function unhover(element) {
        element.setAttribute('src', '<?php echo base_url(IMG.'HOME.png') ?>');
        }
        // brand
        function hover_brand(element) {
        element.setAttribute('src', '<?php echo base_url(IMAGES.'brand-hov.png') ?>');
        }

        function unhover_brand(element) {
        element.setAttribute('src', '<?php echo base_url(IMG.'BRAND STORY.png') ?>');
        }
        // competition
        function hover_com(element) {
        element.setAttribute('src', '<?php echo base_url(IMAGES.'com-hov.png') ?>');
        }

        function unhover_com(element) {
        element.setAttribute('src', '<?php echo base_url(IMAGES.'COMPETITION.png') ?>');
        }
        function addGeneral() {
            var url = window.history.pushState( {} , '', '?cat=general' );
            document.getElementById("cat").value = "GENERAL";
            document.getElementById("uploadID").textContent = "UPLOAD KTP CARD";
            document.getElementById("idNum").textContent = "KTP REGISTRATION NUMBER";
        }
        function addStudent() {
            var url = window.history.pushState( {} , '', '?cat=students' );
            document.getElementById("cat").value = "STUDENTS";
            document.getElementById("uploadID").textContent = "UPLOAD STUDENT CARD";
            document.getElementById("idNum").textContent = "STUDENT CARD NUMBER";
        }
        $( document ).ready(function() {
          var url = window.location.search;
          url = url.replace("?cat=", '');
          document.getElementById("cat").value = url.toUpperCase();
          
        });
        $(document).ready(function () {
          AOS.init();
        });
        
      </script>

    <div class="modal fade" id="successModal">
        <div class="modal-dialog">
            <div class="modal-content">
        
                <div class="modal-body text-center p-3 pt-4 pb-5">



                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <h6>Registration succeed</h6>
                        </div>
                        <div class="col-md-12 mt-4">
                            <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMAGES.'back-home.png') ?>" class="img-fluid px-3 mbm" alt="" /> </a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="failedmodal">
        <div class="modal-dialog">
            <div class="modal-content">
        
                <div class="modal-body text-center p-3 pt-4 pb-5">



                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <h6>Opps... your email has ben registered</h6>
                        </div>
                        <div class="col-md-12 mt-4">
                            <a href="<?php echo base_url('register') ?>">Reload </a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="justify-content-start text-left">
                        <img src="<?php echo base_url(IMAGES.'logo.png') ?>" class="img-fluid side-logo" alt=""/>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-right pt-5">
                    <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG.'HOME.png') ?>"onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" alt="" style="height:18px" /></a><br>
                    <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" alt=""   style="height:18px" /></a>
                    <a href="<?php echo base_url('competition') ?>"><img src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);"  class="img-fluid px-2 mbm" alt=""   style="height:18px"  /></a>
                        <img src="<?php echo base_url(IMAGES.'man 1.png') ?>" alt="" style="position:absolute; width:193px;left:10px;bottom:0" >
                        <img src="<?php echo base_url(IMAGES.'brush-nav.png') ?>" alt="" style="position:absolute; right:0;top:220px" >
                </div>
                
            </div>
        </div>
    </div>
    

    <!-- HEADER -->
    <div class="container-fluid header pt-4 px-0" >
      <div class="container">
        <div class="row text-center">

          <div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
          <button type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG.'Burger.png') ?>" class="img-fluid" alt="" /></button>
          </div>


          <div class="col-md-6 offset-md-6 mb-5 text-right d-none d-md-block">

          <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG.'HOME.png') ?>" class="img-fluid px-3" alt="" style="height:18px" /></a>
            <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG.'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" alt=""   style="height:18px" /></a>
            <a href="<?php echo base_url('competition') ?>"><img src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);"  class="img-fluid px-2" alt=""   style="height:18px"  /></a>

          </div>
          
          <div class="col-md-12 mb-5  d-none d-md-block">
            <div class="mb-3">
              <img src="assets/images/register.png" class="img-fluid" alt="" style="width: 180px;" data-aos="zoom-in">
            </div>
            <img src="assets/images/participan.png" class="img-fluid" alt="" style="width: 320px;" data-aos="zoom-in-up">
          </div>

          <div class="col-12 mb-3 d-md-none d-block">
            <div class="mb-2">
              <img src="assets/images/register.png" class="img-fluid" alt="" style="width: 120px;" data-aos="zoom-in">
            </div>
            <img src="assets/images/participan.png" class="img-fluid" alt="" style="width: 250px;" data-aos="zoom-in-up">
          </div>
          

        </div>
      </div>
      <div style="position:relative">
          <div style="position:absolute;bottom:-250px;z-index:1000" class="d-none d-md-block">
            <img src="<?php echo base_url(IMAGES.'purple.png') ?>" alt="" class="img-fluid" style="width:200px" >
          </div>
          <div style="position:absolute;bottom:-400px;" class="d-block d-md-none">
            <img src="<?php echo base_url(IMAGES.'purple.png') ?>" alt="" class="img-fluid" style="width:120px" >
          </div>

          <div style="position:absolute;bottom:-67.5rem;z-index:1000" class="d-none d-md-block">
            <img src="<?php echo base_url(IMAGES.'man-reg.png') ?>" alt="" class="img-fluid" style="width:265px" >
          </div>

          <div style="position:absolute;right:0px;bottom:-60rem;z-index:999 "class="d-none d-md-block">
            <img src="<?php echo base_url(IMAGES.'color.png') ?>" alt="" style="width:380px" >
          </div>
          <div style="position:absolute;right:0px;bottom:-51rem;z-index:-1 "class="d-block d-md-none">
            <img src="<?php echo base_url(IMAGES.'color.png') ?>" alt="" style="width:180px" >
          </div>

      </div>
    </div>


    <div class="container-fluid" data-aos="zoom-out">
      <div class="container">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8  mb-5">
              <div class="card card-reg" >
                <div class="card-body card-reg d-flex justify-content-center">
                    <div class="row card-reg">
                        <div class="col-md-6 col-md-6 d-flex justify-content-end mt-2">
                            <a href="javascript:void(0);" onclick="addGeneral()" class="btn-reg">
                                <img src="assets/images/general.png" class="img-btn-reg a" data-toggle="tooltip" title="Anyone other than students. Amateur or professional artists & designers">
                            </a>
                        </div>
                        <div class="col-md-6 col-md-6 d-flex justify-content-start mt-2">
                            <a href="javascript:void(0);" onclick="addStudent()" class="btn-reg">
                                <img src="assets/images/student.png" class="img-btn-reg b" alt="" data-toggle="tooltip" title="Anyone who is enrolled in a university / college above the age of 18, not working in a professional field">
                            </a>
                        </div>
                        
                        <form id="submit" enctype="multipart/form-data">

                            <div class="form-group">
                                <label>COMPLETE NAME </label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                                
                            </div>
                            <div class="form-group">
                                <label>PASSWORD</label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <label id="idNum">IDENTITY NUMBER</label>
                                <input type="text" class="form-control" name="idcard" id="idcard" placeholder="33XXXXXXXXXXXXXX" required>
                            </div>
                            <div class="form-group">
                                <label>CITY</label>
                                <input type="text" name="city" id="city" class="form-control" placeholder="City" required>
                            </div>
                            <div class="form-group">
                                <label>EMAIL</label>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <label>PHONE NUMBER</label>
                                <input type="number" name="nohp" id="nohp" class="form-control" placeholder="Phone Number" required>
                            </div>
                            <div class="form-group">
                                <label>DATE OF BIRTH</label>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-3 col-sm-3">
                                    <select name="dob_day" class="day" id="dob_day" required>
                                        <option value="">Date</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-sm-3">
                                    <select name="dob_month" id="dob_month"  class="month" required>
                                        <option value="">Month</option>
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-sm-3">
                                    <select name="dob_year" id="dob_year"  class="year" required>
                                        <option value="">Year</option>
                                        <option value="2003">2003</option>
                                        <option value="2002">2002</option>
                                        <option value="2001">2001</option>
                                        <option value="2000">2000</option>
                                        <option value="1999">1999</option>
                                        <option value="1998">1998</option>
                                        <option value="1997">1997</option>
                                        <option value="1996">1996</option>
                                        <option value="1995">1995</option>
                                        <option value="1994">1994</option>
                                        <option value="1993">1993</option>
                                        <option value="1992">1992</option>
                                        <option value="1991">1991</option>
                                        <option value="1990">1990</option>
                                        <option value="1989">1989</option>
                                        <option value="1988">1988</option>
                                        <option value="1987">1987</option>
                                        <option value="1986">1986</option>
                                        <option value="1985">1985</option>
                                        <option value="1984">1984</option>
                                        <option value="1983">1983</option>
                                        <option value="1982">1982</option>
                                        <option value="1981">1981</option>
                                        <option value="1980">1980</option>
                                        <option value="1979">1979</option>
                                        <option value="1978">1978</option>
                                        <option value="1977">1977</option>
                                        <option value="1976">1976</option>
                                        <option value="1975">1975</option>
                                        <option value="1974">1974</option>
                                        <option value="1973">1973</option>
                                        <option value="1972">1972</option>
                                        <option value="1971">1971</option>
                                        <option value="1970">1970</option>
                                        <option value="1969">1969</option>
                                        <option value="1968">1968</option>
                                        <option value="1967">1967</option>
                                        <option value="1966">1966</option>
                                        <option value="1965">1965</option>
                                        <option value="1964">1964</option>
                                        <option value="1963">1963</option>
                                        <option value="1962">1962</option>
                                        <option value="1961">1961</option>
                                        <option value="1960">1960</option>
                                        <option value="1959">1959</option>
                                        <option value="1958">1958</option>
                                        <option value="1957">1957</option>
                                        <option value="1956">1956</option>
                                        <option value="1955">1955</option>
                                        <option value="1954">1954</option>
                                        <option value="1953">1953</option>
                                        <option value="1952">1952</option>
                                        <option value="1951">1951</option>
                                        <option value="1950">1950</option>
                                        <option value="1949">1949</option>
                                        <option value="1948">1948</option>
                                        <option value="1947">1947</option>
                                        <option value="1946">1946</option>
                                        <option value="1945">1945</option>
                                        <option value="1944">1944</option>
                                        <option value="1943">1943</option>
                                        <option value="1942">1942</option>
                                        <option value="1941">1941</option>
                                        <option value="1940">1940</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>CATEGORY</label>
                                  <select class="custom-select mr-sm-2" name="cat" id="cat" required >
                                  <option value="STUDENTS">Student</option>
                                  <option value="GENERAL">General</option>
                                  
                                  </select>
                            </div>

                            <div class="form-group">
                                <label id="uploadID">UPLOAD IDENTITY CARD</label>
                                <input id="image" type="file" name="image" style="border: 0px solid #ced4da;" class="form-control-file" required>
                                
                            </div>

                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="check" id="check" value="aggre" required>
                                <label class="form-check-label" for="check">
                                I agree to the Terms and Conditions and the Privacy Policy
                                </label>
                            </div>
                            <br>
                            <button type="submit"  class="btn-primary-outline"> <img src="assets/images/reg-btn.png" class="img-fluid"> </button>
                            <div class="alert alert-danger print-error-msg" style="display:none">
                        </form>
                        
                    </div>
                </div>
              </div>
          </div>
          <div class="col-md-2"></div>
          

        </div>
      </div>
    </div>

    <!-- FOOTER -->
    <div class="container-fluid footer py-2">
      <div class="container">
        <div class="row">

          <div class="col-4 col-md-6 text-left">
            <img src="<?php echo base_url(IMG.'esse-logo-footer.png') ?>" class="img-fluid" alt=""/>
          </div>
      
          <div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
            <img src="<?php echo base_url(IMG.'alr-footer.png') ?>" class="img-fluid" alt=""/>
          </div>

        </div>
      </div>
    </div>

    <script type="text/javascript"> 
        $('#submit').submit(function(e){
            e.preventDefault(); 
            $.ajax({
                url:'<?php echo site_url('Register/save')?>',
                type:"post",
                data:new FormData(this),
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function(data) {
                  if($.isEmptyObject(data.error)){
	                	$('#successModal').modal('show');
	                }else{
                    $('#failedmodal').modal('show');
						        
	                }
                
                }

                
            });
          });  

    </script>

    <script>
      $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip({
            trigger : 'hover'
        }) 
      });
    </script>