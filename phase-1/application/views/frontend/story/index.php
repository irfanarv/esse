
<script>    
function hover(element) {
  element.setAttribute('src', '<?php echo base_url(IMAGES.'home-hov.png') ?>');
}
function unhover(element) {
  element.setAttribute('src', '<?php echo base_url(IMG.'HOME.png') ?>');
}
// competition
function hover_com(element) {
  element.setAttribute('src', '<?php echo base_url(IMAGES.'com-hov.png') ?>');
}

function unhover_com(element) {
  element.setAttribute('src', '<?php echo base_url(IMAGES.'COMPETITION.png') ?>');
}
$(document).ready(function () {
	AOS.init();
	$('.slider').slick({
		dots: true,
		autoplay: true,
		autoplaySpeed: 1000,
		infinite: true,
		speed: 500
	});
    $("#product").mousemove(function(e) {
        parallaxProduct(e, "#product-1", -120);
        parallaxProduct(e, "#product-2", 120);
        parallaxProduct(e, "#product-3", -120);
        parallaxProduct(e, "#product-4", 120);
    });
    function parallaxProduct(e, target, movement) {
        var $this = $("#product");
        var relX = e.pageX - $this.offset().left;
        var relY = e.pageY - $this.offset().top;

        TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
        });
    }
});
</script>


<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES.'logo.png') ?>" class="img-fluid side-logo" alt="" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG.'HOME.png') ?>" onmouseover="hover(this);"
						onmouseout="unhover(this);" class="img-fluid px-3 mbm" alt="" style="height:18px" /></a>
				<img src="<?php echo base_url(IMAGES.'brand.png') ?>" class="img-fluid px-2 mbm" alt="" style="height:30px" />
				<a href="<?php echo base_url('competition') ?>"><img src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>"
						onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2 mbm" alt=""
						style="height:18px" /></a>
				<img src="<?php echo base_url(IMAGES.'man 1.png') ?>" alt=""
					style="position:absolute; width:193px;left:10px;bottom:0">
				<img src="<?php echo base_url(IMAGES.'brush-nav.png') ?>" alt="" style="position:absolute; right:0;top:220px">
			</div>

		</div>
	</div>
</div>


<!-- HEADER -->
<div class="container-fluid header pt-4 px-0">
	<div class="container">
		<div class="row text-center mb-5">

			<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
				<button style="background:transparent" type="button" class="btn" data-toggle="modal"
					data-target="#right_modal_sm"><img src="<?php echo base_url(IMG.'Burger.png') ?>" class="img-fluid"
						alt="" /></button>
			</div>


			<div class="col-md-6 offset-md-6 mb-5 text-right d-none d-md-block">

				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG.'HOME.png') ?>" onmouseover="hover(this);"
						onmouseout="unhover(this);" class="img-fluid px-3" alt="" style="height:18px" /></a>
				<img src="<?php echo base_url(IMAGES.'brand.png') ?>" class="img-fluid px-2" alt="" style="height:30px" />
				<a href="<?php echo base_url('competition') ?>"><img src="<?php echo base_url(IMAGES.'COMPETITION.png') ?>"
						onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" alt=""
						style="height:18px" /></a>
			</div>
		</div>
	</div>

	<div class="container-fluid brand-story">
		<div class="container">
			<div class="row text-center">

				<div class="col-md-6 d-none d-md-block align-self-center text-left" data-aos="zoom-out-right">
					<div class="slider">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'product-double.svg') ?>" alt="Double">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'applemint.svg') ?>" alt="Apple Mint">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'grape.svg') ?>" alt="Grape">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'juicy.svg') ?>" alt="Juice">
					</div>
				</div>

				<div class="col-md-6 d-block d-md-none mb-5 pl-4 pr-4" data-aos="zoom-out-right" >
					<div class="slider">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'product-double.svg') ?>" alt="Double">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'applemint.svg') ?>" alt="Apple Mint">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'grape.svg') ?>" alt="Grape">
						<img class="img-fluid" src="<?php echo base_url(IMAGES.'juicy.svg') ?>" alt="Juice">
					</div>
				</div>

				<div class="col-md-6 d-none d-md-block" data-aos="zoom-out-left">
					<img src="<?php echo base_url(IMAGES.'artikel.svg') ?>" class="img-fluid" alt="" />
				</div>

				<div class="col-md-6 d-block d-md-none" >
					<img src="<?php echo base_url(IMAGES.'artikel-m.svg') ?>" class="img-fluid" alt="" style="width: 300px;">
				</div>

			</div>
		</div>
	</div>


</div>




<!-- <div class="d-none d-md-block" style="position:relative">
	<div style="position:absolute;bottom:-230px;z-index:999">
		<img src="<?php echo base_url(IMG.'participant-bg-top.png') ?>" alt="" style="width:100%">
	</div>
</div>
<div class="d-block d-md-none" style="position:relative">
	<div style="position:absolute;bottom:-100px;z-index:999">
		<img src="<?php echo base_url(IMG.'participant-bg-top.png') ?>" alt="" style="width:100%">
	</div>
</div> -->

<div class="d-none d-md-block container-fluid competition-criteria " style="margin-top: 28px;">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<div class="card" style="margin-top: 300px !important; left:0px !important;">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dyMe8-MKXag" frameborder="0"
								allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen></iframe>
						</div>
					</div>
				</div>
				
			</div>

		</div>
	</div>
	<!-- <div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom">
			<img src="<?php echo base_url(IMG.'participant-bg-bottom.png') ?>" alt="" style="width:100%">
		</div>
	</div> -->
</div>

<div class="d-block d-md-none container-fluid competition-criteria ">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-12">
				<div class="card" style="margin-top: 100px !important; left:0px !important;">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dyMe8-MKXag" frameborder="0"
								allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
								allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- <div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile">
			<img src="<?php echo base_url(IMG.'participant-bg-bottom.png') ?>" alt="" style="width:100%">
		</div>
	</div> -->
</div>



<div class="container-fluid  py-5 ig-story">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img
						src="<?php echo base_url(IMG.'Instagram-1.png') ?>" class="img-fluid" alt="" style="width:250px" /></a>
			</div>

		</div>
	</div>
</div>


<!-- FOOTER -->
<div class="container-fluid footer py-2">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG.'esse-logo-footer.png') ?>" class="img-fluid" alt="" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG.'alr-footer.png') ?>" class="img-fluid" alt="" />
			</div>

		</div>
	</div>
</div>
