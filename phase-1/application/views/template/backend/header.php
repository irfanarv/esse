<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">			
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        <a href="<?= base_url('dashboard');?>" class="b-brand">
            <img src="<?php echo base_url(IMAGES.'logo.png') ?>" alt="" class="img-fluid" style="width: 80px;">
        </a>
        <a href="#!" class="mob-toggler">
            <i class="feather icon-more-vertical"></i>
        </a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
            <li class="mr-3">
                <a href="<?= base_url();?>" target="_new" class="dud-logout" title="Visit Website">
                    <i class="feather icon-external-link"></i>
                </a>
            </li>
            
            <li>
                <a href="<?= base_url('logout');?>" class="dud-logout" title="Logout">
                    <i class="feather icon-log-out"></i>
                </a>
            </li>
        </ul>
    </div>
</header>