<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Story extends CI_Controller 
{
	
	private $ctrl = "story";
	private $title = "Brand Story";
	private $menu = "story";
	
	
	function __construct(){
		
		parent::__construct();	
		$this->load->model('VisitorsM');
		$this->VisitorsM->count_visitor(); 
			
			
	}

	
	public function index()
	{				
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/index.php";
		$this->load->view(FRONTEND_LAYOUT, $data);
	}	
	
}
