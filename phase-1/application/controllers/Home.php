<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	
	private $ctrl = "home";
	private $title = "Home";
	private $menu = "home";
	
	
	function __construct(){
		
		parent::__construct();	
		$this->data['isvalid'] = $this->session->userdata('validate');
		$this->load->model('VisitorsM');
		$this->VisitorsM->count_visitor(); 
			
	}

	
	public function index()
	{				
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;

		if ($this->data['isvalid']) {
			$data['content_view'] = "$this->ctrl/home.php";
		} else {
			$data['content_view'] = "$this->ctrl/index.php";
		}
		

		$this->load->view(FRONTEND_LAYOUT, $data);
	}	

	public function SetCookie(){
		$this->session->set_userdata('validate', '18+');
	}



	
}
