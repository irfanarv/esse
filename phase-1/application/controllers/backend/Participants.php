<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Participants extends CI_Controller
{
    private $ctrl = "participants";
	private $title = "Participants";
	private $menu = "participants";
	public $data = array();
    public $userid; 
	
	
	function __construct()
	{
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('id');
		$this->load->model('VisitorsM');
        $this->load->model('RegistM');
		$this->load->model('AuthM');	
	}

	function index() 
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;

		if ($this->validadmin()) {

			$this->backend->display('backend/participantsV', $data);
		} else {
			$this->data['pagetitle'] = 'Esse - Make Change | Login';
			$this->load->view('backend/loginV', $this->data);
		}
	}

    public function all()
    {
        $list = $this->RegistM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $participants) {
            if($participants->dob_month == 1){
                $date = "Jan";
            }elseif($participants->dob_month == 2) {
                $date = 'Feb';
            }elseif($participants->dob_month == 3){
				$date = 'Mar';
			}elseif($participants->dob_month == 4){
				$date = 'Apr';
			}elseif($participants->dob_month == 5){
				$date = 'May';
			}elseif($participants->dob_month == 6){
				$date = 'Jun';
			}elseif($participants->dob_month == 7){
				$date = 'Jul';
			}elseif($participants->dob_month == 8){
				$date = 'Aug';
			}elseif($participants->dob_month == 9){
				$date = 'Sept';
			}elseif($participants->dob_month == 10){
				$date = 'Oct';
			}elseif($participants->dob_month == 11){
				$date = 'Nov';
			}elseif($participants->dob_month == 12){
				$date = 'Dec';
			}
            $no++;
            $row = array();
            // $row[] = "
            // <div class='thumbnail'>
            //     <div class='thumb'>
            //         <a href='../assets/ktp/$participants->image' data-lightbox='1'>
            //             <img src='../assets/ktp/$participants->image' class='zoom img-fluid' style='height:50px;width:50px;align:middle;'>
            //         </a>
            //     </div>
            // </div>";
            
            $row[] = $participants->idcard;
            $row[] = $participants->name;
            $row[] = $participants->city;
            $row[] = $participants->email;
            $row[] = $participants->phone;
            $row[] = $participants->dob_day .'  '. $date .'  '. $participants->dob_year;
            $row[] = '<button class="btn btn-light has-ripple" onClick="detail_participants('."'".$participants->id."'".')">Details</button>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->RegistM->count_all(),
                        "recordsFiltered" => $this->RegistM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }


    public function general()
    {
        $list = $this->RegistM->get_datatables2();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $participants) {
            if($participants->dob_month == 1){
                $date = "Jan";
            }elseif($participants->dob_month == 2) {
                $date = 'Feb';
            }elseif($participants->dob_month == 3){
				$date = 'Mar';
			}elseif($participants->dob_month == 4){
				$date = 'Apr';
			}elseif($participants->dob_month == 5){
				$date = 'May';
			}elseif($participants->dob_month == 6){
				$date = 'Jun';
			}elseif($participants->dob_month == 7){
				$date = 'Jul';
			}elseif($participants->dob_month == 8){
				$date = 'Aug';
			}elseif($participants->dob_month == 9){
				$date = 'Sept';
			}elseif($participants->dob_month == 10){
				$date = 'Oct';
			}elseif($participants->dob_month == 11){
				$date = 'Nov';
			}elseif($participants->dob_month == 12){
				$date = 'Dec';
			}
            $no++;
            $row = array();
            // $row[] = "
            // <div class='thumbnail'>
            //     <div class='thumb'>
            //         <a href='../assets/ktp/$participants->image' data-lightbox='1'>
            //             <img src='../assets/ktp/$participants->image' class='zoom img-fluid' style='height:50px;width:50px;align:middle;'>
            //         </a>
            //     </div>
            // </div>";
            
            $row[] = $participants->idcard;
            $row[] = $participants->name;
            $row[] = $participants->city;
            $row[] = $participants->email;
            $row[] = $participants->phone;
            $row[] = $participants->dob_day .'  '. $date .'  '. $participants->dob_year;
            $row[] = '<button class="btn btn-light has-ripple" onClick="detail_participants('."'".$participants->id."'".')">Details</button>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->RegistM->count_all2(),
                        "recordsFiltered" => $this->RegistM->count_filtered2(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function students()
    {
        $list = $this->RegistM->get_datatables3();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $participants) {
            if($participants->dob_month == 1){
                $date = "Jan";
            }elseif($participants->dob_month == 2) {
                $date = 'Feb';
            }elseif($participants->dob_month == 3){
				$date = 'Mar';
			}elseif($participants->dob_month == 4){
				$date = 'Apr';
			}elseif($participants->dob_month == 5){
				$date = 'May';
			}elseif($participants->dob_month == 6){
				$date = 'Jun';
			}elseif($participants->dob_month == 7){
				$date = 'Jul';
			}elseif($participants->dob_month == 8){
				$date = 'Aug';
			}elseif($participants->dob_month == 9){
				$date = 'Sept';
			}elseif($participants->dob_month == 10){
				$date = 'Oct';
			}elseif($participants->dob_month == 11){
				$date = 'Nov';
			}elseif($participants->dob_month == 12){
				$date = 'Dec';
			}
            $no++;
            $row = array();
            // $row[] = "
            // <div class='thumbnail'>
            //     <div class='thumb'>
            //         <a href='../assets/ktp/$participants->image' data-lightbox='1'>
            //             <img src='../assets/ktp/$participants->image' class='zoom img-fluid' style='height:50px;width:50px;align:middle;'>
            //         </a>
            //     </div>
            // </div>";
            
            $row[] = $participants->idcard;
            $row[] = $participants->name;
            $row[] = $participants->city;
            $row[] = $participants->email;
            $row[] = $participants->phone;
            $row[] = $participants->dob_day .'  '. $date .'  '. $participants->dob_year;
            $row[] = '<button class="btn btn-light has-ripple" onClick="detail_participants('."'".$participants->id."'".')">Details</button>';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->RegistM->count_all3(),
                        "recordsFiltered" => $this->RegistM->count_filtered3(),
                        "data" => $data,
                );
        echo json_encode($output);
    }

    public function details($id){
        $datapartisipan = $this->RegistM->get_by_id($id);
		$data = array();
		foreach ($datapartisipan as $dt) {
			$row = array();
			if($dt->dob_month == 1){
                $date = "Jan";
            }elseif($dt->dob_month == 2) {
                $date = 'Feb';
            }elseif($dt->dob_month == 3){
				$date = 'Mar';
			}elseif($dt->dob_month == 4){
				$date = 'Apr';
			}elseif($dt->dob_month == 5){
				$date = 'May';
			}elseif($dt->dob_month == 6){
				$date = 'Jun';
			}elseif($dt->dob_month == 7){
				$date = 'Jul';
			}elseif($dt->dob_month == 8){
				$date = 'Aug';
			}elseif($dt->dob_month == 9){
				$date = 'Sept';
			}elseif($dt->dob_month == 10){
				$date = 'Oct';
			}elseif($dt->dob_month == 11){
				$date = 'Nov';
			}elseif($dt->dob_month == 12){
				$date = 'Dec';
			}
            $row[] = $dt->image;
            $row[] = $dt->idcard;
            $row[] = $dt->name;
            $row[] = $dt->city;
            $row[] = $dt->email;
            $row[] = $dt->category;
            $row[] = $dt->date_register;
            $row[] = $dt->dob_day .'  '. $date .'  '. $dt->dob_year;
			$data[] = $row;
		}
        $output = '';
		
                foreach ($datapartisipan as $dt) {
                    $output .= '
                    <div class="row">
                            <div class="col-lg-5">
                                <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <img src="../assets/ktp/'.$dt->image.'" class="d-block w-100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 mt-5">
                                <h5 class="mb-2">KTP Number : '.$dt->idcard.'</h5>
                                <h5 class="mb-2">Name :'.$dt->name.'</h5>
                                <h5 class="mb-2">Email : '.$dt->email.'</h5>
                                <h5 class="mb-2">City : '.$dt->city.'</h5>
                                <h5 class="mb-2">Date Of Brith :'.$dt->dob_day .'  '. $date .'  '. $dt->dob_year.'</h5>
                                <h5 class="mb-2">Register Category : '.$dt->category.'</h5>
                                <h5 class="mb-2">Register Date : '.date('d M Y', strtotime($dt->date_register)).'</h5>
                            </div>
                            
                        </div>
                    ';
                }
        
        echo $output;
    }


    function validadmin() {

        if (!empty($this->data['isadmin'])) {
            return true;
        } else {
            return false;
        }
    }


    

}