<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Home';
$route['brandstory'] = 'Story';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// dashboard
$route['dashboard'] = 'backend/dashboard';
$route['dashboard/participants'] = 'backend/participants';
$route['dashboard/login'] = 'backend/dashboard/login';
$route['logout'] = 'backend/dashboard/logout';
