(function($) {
  
  "use strict";
  
  /* Page Loader active
  ========================================================*/
  $('#preloader').fadeOut();


  /* 
  CounterUp
  ========================================================================== */
  $('.counter').counterUp({
    time: 500
  });  
  

  /* 
   Back Top Link
   ========================================================================== */
    var offset = 200;
    var duration = 500;
    $(window).scroll(function() {
      if ($(this).scrollTop() > offset) {
        $('.back-to-top').fadeIn(400);
      } else {
        $('.back-to-top').fadeOut(400);
      }
    });

    $('.back-to-top').on('click',function(event) {
      event.preventDefault();
      $('html, body').animate({
        scrollTop: 0
      }, 600);
      return false;
    })


  /* 
   One Page Navigation & wow js
   ========================================================================== */
    //Initiat WOW JS
    new WOW().init();

    $(window).on('load', function() {
       
        $('body').scrollspy({
            target: '.navbar-collapse',
            offset: 195
        });

        $(window).on('scroll', function() {
            if ($(window).scrollTop() > 100) {
                $('.fixed-top').addClass('menu-bg');
                $('.big-logo').addClass('small-logo');
            } else {
                $('.fixed-top').removeClass('menu-bg');
                $('.big-logo').removeClass('small-logo');
            }
        });

    });


  /* Auto Close Responsive Navbar on Click
  ========================================================*/
  function close_toggle() {
      if ($(window).width() <= 768) {
          $('.navbar-collapse a').on('click', function () {
              $('.navbar-collapse').collapse('hide');
          });
      }
      else {
          $('.navbar .navbar-inverse a').off('click');
      }
  }
  close_toggle();
  $(window).resize(close_toggle);

  
  /* Product content
  ========================================================*/
  $(".flyaway").hover(function(){
    $(".flyaway-content").fadeIn("fast");
  }, function(){
    $(".flyaway-content").fadeOut("fast");
  });

  $(".funwap").hover(function(){
    $(".funwap-content").fadeIn("fast");
  }, function(){
    $(".funwap-content").fadeOut("fast");
  });

  $(".ustadku").hover(function(){
    $(".ustadku-content").fadeIn("fast");
  }, function(){
    $(".ustadku-content").fadeOut("fast");
  });



}(jQuery));

