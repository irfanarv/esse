$(document).ready(function () {


    var controller = new ScrollMagic.Controller();



    /* HOW TO - SHOW US YOUR POP
    ========================================================*/

     // --- appear music --- //
     var imageAppearTimeline = new TimelineMax();
     imageAppearTimeline
         .from("#music-comp", 1, {
             y: -50,
             autoAlpha: 0
         })
         .to("#music-comp", 1, {
             y: 0,
         })
 
     new ScrollMagic.Scene({
             triggerElement: "#howto-scene",
 
         })
         .duration('100%')
         .setTween(imageAppearTimeline)
         .addTo(controller);

    // --- appear music  mobile --- //
    var imageAppearTimeline = new TimelineMax();
    imageAppearTimeline
        .from("#music-mobile-comp", 1, {
            y: -100,
            autoAlpha: 0
        })
        .to("#music-mobile-comp", 1, {
            y: 0,
        })
 
    new ScrollMagic.Scene({
            triggerElement: "#howto-scene",

        })
        .duration('100%')
        .setTween(imageAppearTimeline)
        .addTo(controller);
         
    // --- appear bee --- //
    var imageAppearTimeline = new TimelineMax();
    imageAppearTimeline
        .from("#bee-comp", 1, {
            y: 100,
            x: 100,
            scale:0,
            autoAlpha: 0
        })
        .to("#bee-comp", 1, {
            y: 0,
        })

    new ScrollMagic.Scene({
            triggerElement: "#howto-scene",

        })
        .duration('100%')
        .setTween(imageAppearTimeline)
        .addTo(controller);


    // --- appear star right --- //
    var imageAppearTimeline = new TimelineMax();
    imageAppearTimeline
        .from("#star-right-comp", 1, {
            scale:0,
            autoAlpha: 0
        })
        .to("#star-right-comp", 1, {
            y: 0,
        })

    new ScrollMagic.Scene({
            triggerElement: "#howto-scene",
            offset: 200,

        })
        .duration('100%')
        .setTween(imageAppearTimeline)
        .addTo(controller);


    // --- appear star left --- //
    var imageAppearTimeline = new TimelineMax();
    imageAppearTimeline
        .from("#star-left-comp", 1, {
            scale:0,
            autoAlpha: 0
        })
        .to("#star-left-comp", 1, {
            y: 0,
        })

    new ScrollMagic.Scene({
            triggerElement: "#howto-scene",
            offset: 100,

        })
        .duration('100%')
        .setTween(imageAppearTimeline)
        .addTo(controller);



    /* PRIZE
    ========================================================*/

    // --- appear star left 1--- //
    var imageAppearTimeline = new TimelineMax();
    imageAppearTimeline
        .from("#prize-star-left1-comp", 1, {
            scale:0,
            autoAlpha: 0
        })
        .to("#prize-star-left1-comp", 1, {
            y: 0,
        })

    new ScrollMagic.Scene({
            triggerElement: "#prize-scene",
            offset: 100,

        })
        .duration('100%')
        .setTween(imageAppearTimeline)
        .addTo(controller);

    // --- appear star left 2--- //
    var imageAppearTimeline = new TimelineMax();
    imageAppearTimeline
        .from("#prize-star-left2-comp", 1, {
            scale:0,
            autoAlpha: 0
        })
        .to("#prize-star-left2-comp", 1, {
            y: 0,
        })

    new ScrollMagic.Scene({
            triggerElement: "#prize-scene",
        })
        .duration('100%')
        .setTween(imageAppearTimeline)
        .addTo(controller);


    // --- appear star right--- //
    var imageAppearTimeline = new TimelineMax();
    imageAppearTimeline
        .from("#prize-star-right-comp", 1, {
            scale:0,
            autoAlpha: 0
        })
        .to("#prize-star-right-comp", 1, {
            y: 0,
        })

    new ScrollMagic.Scene({
            triggerElement: "#prize-scene",
            offset: 150,
        })
        .duration('100%')
        .setTween(imageAppearTimeline)
        .addTo(controller);


});