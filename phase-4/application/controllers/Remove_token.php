<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Remove_token extends CI_Controller 
{
	
	private $ctrl = "remove_token";
	private $title = "Logout";
	private $menu = "logout";
	
	
	/**== Construct ==**/
	function __construct(){
		
		parent::__construct();	
		
		$this->load->model('VisitorsM');
		$this->VisitorsM->count_visitor(); 
			
	}

	
	/**== Index Page ==**/
	public function index()
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/index.php";
				
		// load view
		$this->load->view(FRONTEND_LAYOUT, $data);
    }
}