<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Voter extends CI_Controller
{
    private $ctrl = "voters";
	private $title = "Voters";
	private $menu = "voter";
	public $data = array();
    public $userid; 
	
	
	function __construct()
	{
		parent::__construct();
		$this->data['isadmin'] = $this->session->userdata('id');
		$this->load->model('VisitorsM');
        $this->load->model('VoterM');
		$this->load->model('AuthM');	
        $this->load->helper('download');
	}

	function index() 
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;

		if ($this->validadmin()) {

			$this->backend->display('backend/VoterV', $data);
		} else {
			$this->data['pagetitle'] = 'Esse - Make Change | Login';
			$this->load->view('backend/loginV', $this->data);
		}
	}

    public function listVoters()
    {
        $list = $this->VoterM->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $voter) {
            
            $no++;
            $row = array();
            $row[] = "
            
            <td class='align-middle sorting_1'>
                <img src='$voter->picture' class='rounded mr-3' height='48'>
                <p class='m-0 d-inline-block align-middle font-16'>
                $voter->first_name $voter->last_name
                </p>
            </td>
            ";
            $row[] = $voter->first_name . $voter->last_name;
            $row[] = $voter->valid_email;
            $row[] = "$voter->vote_win x Vote";
            $row[] = "$voter->vote_gen x Vote";
            $row[] = "$voter->vote_student x Vote";
            $row[] = date('d M Y H:i', strtotime($voter->modified)).'';
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->VoterM->count_all(),
                        "recordsFiltered" => $this->VoterM->count_filtered(),
                        "data" => $data,
                );
        echo json_encode($output);
    }


    function validadmin() {

        if (!empty($this->data['isadmin'])) {
            return true;
        } else {
            return false;
        }
    }


    

}