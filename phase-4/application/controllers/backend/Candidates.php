<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Candidates extends CI_Controller
{
    private $ctrl = "candidates";
    private $title = "Candidates";
    private $menu = "candidates";
    public $data = array();
    public $userid;


    function __construct()
    {
        parent::__construct();
        $this->data['isadmin'] = $this->session->userdata('id');
        $this->load->model('VisitorsM');
        $this->load->model('SubmissionM');
        $this->load->model('CandidatesM');
        $this->load->model('AuthM');
        $this->load->helper('download');
    }

    function index()
    {
        $data['menu'] = $this->menu;
        $data['sub_menu'] = "";
        $data['ctrl'] = $this->ctrl;
        $data['page_title'] = $this->title;

        if ($this->validadmin()) {
            $this->backend->display('backend/candidatesV', $data);
        } else {
            $this->data['pagetitle'] = 'Esse - Make Change | Login';
            $this->load->view('backend/loginV', $this->data);
        }
    }

    public function win()
    {
        $list = $this->CandidatesM->get_datatables(); 
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $candidates) {
            if($candidates->thumb == NULL){
                $image = 'https://via.placeholder.com/150';
             }else{
                $image = '../assets/candidate/'.$candidates->thumb;
            }

            $no++;
            $row = array();
            $row[] = $candidates->set_win;

            $row[] = "
            <div class='thumbnail'>
                <div class='thumb'>
                    <a href='$image' data-lightbox='1'>
                        <img src='$image' class='zoom img-fluid' style='height:50px;width:50px;align:middle;'>
                    </a>
                </div>
            </div>";
            $row[] = $candidates->name;
            $row[] = $candidates->concept_name;
            $row[] = $candidates->instagram;
            $row[] = '#'.$candidates->tag1 .' '. '#'.$candidates->tag2;
            $row[] = $candidates->category;
            $row[] = $candidates->email;
            $row[] = $candidates->phone;
            $row[] = $candidates->idcard; 
            $row[] = $candidates->city; 
            $row[] = "$candidates->vote_total Votes";
            $row[] = '
            <td class="table-action">
                <a href="' . base_url('dashboard/cadidates-mockup/') . $candidates->id_sub . '" class="btn btn-icon btn-outline-warning"><i class="feather icon-image"></i></a>
                <a href="javascript:void(0)" onclick="editCandidate(' . "'" . $candidates->id_sub . "'" . ')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteCandidate(' . "'" . $candidates->id_sub . "'" . ')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->CandidatesM->count_all(),
            "recordsFiltered" => $this->CandidatesM->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    
    public function gen()
    {
        $list = $this->CandidatesM->get_datatables1();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $candidates) {
            if($candidates->thumb == NULL){
                $image = 'https://via.placeholder.com/150';
             }else{
                $image = '../assets/candidate/'.$candidates->thumb;
            }
            $no++;
            $row = array();
            $row[] = "
            <div class='thumbnail'>
                <div class='thumb'>
                    <a href='$image' data-lightbox='1'>
                        <img src='$image' class='zoom img-fluid' style='height:50px;width:50px;align:middle;'>
                    </a>
                </div>
            </div>";
            $row[] = $candidates->name;
            $row[] = $candidates->concept_name;
            $row[] = $candidates->instagram;
            $row[] = '#'.$candidates->tag1 .' '. '#'.$candidates->tag2;
            $row[] = $candidates->category;
            $row[] = $candidates->email;
            $row[] = $candidates->phone;
            $row[] = $candidates->idcard;
            $row[] = $candidates->city;
            
            $row[] = "$candidates->vote_total Votes";
            $row[] = '
            <td class="table-action">
                <a href="' . base_url('dashboard/cadidates-mockup/') . $candidates->id_sub . '" class="btn btn-icon btn-outline-warning"><i class="feather icon-image"></i></a>
                <a href="javascript:void(0)" onclick="editCandidate(' . "'" . $candidates->id_sub . "'" . ')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteCandidate(' . "'" . $candidates->id_sub . "'" . ')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->CandidatesM->count_all1(),
            "recordsFiltered" => $this->CandidatesM->count_filtered1(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    // student
    public function student()
    {
        $list = $this->CandidatesM->get_datatables2();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $candidates) {
            if($candidates->thumb == NULL){
                $image = 'https://via.placeholder.com/150';
             }else{
                $image = '../assets/candidate/'.$candidates->thumb;
            }
            $no++;
            $row = array();
            $row[] = "
            <div class='thumbnail'>
                <div class='thumb'>
                    <a href='$image' data-lightbox='1'>
                        <img src='$image' class='zoom img-fluid' style='height:50px;width:50px;align:middle;'>
                    </a>
                </div>
            </div>";
            $row[] = $candidates->name;
            $row[] = $candidates->concept_name;
            $row[] = $candidates->instagram;
            $row[] = '#'.$candidates->tag1 .' '. '#'.$candidates->tag2;
            $row[] = $candidates->category;
            $row[] = $candidates->email;
            $row[] = $candidates->phone;
            $row[] = $candidates->idcard;
            $row[] = $candidates->city;
            $row[] = "$candidates->vote_total Votes";
            $row[] = '
            <td class="table-action">
                <a href="' . base_url('dashboard/cadidates-mockup/') . $candidates->id_sub . '" class="btn btn-icon btn-outline-warning"><i class="feather icon-image"></i></a>
                <a href="javascript:void(0)" onclick="editCandidate(' . "'" . $candidates->id_sub . "'" . ')" class="btn btn-icon btn-outline-success"><i class="feather icon-edit"></i></a>
                <a href="javascript:void(0)" onclick="deleteCandidate(' . "'" . $candidates->id_sub . "'" . ')" class="btn btn-icon btn-outline-danger"><i class="feather icon-trash-2"></i></a>
            </td>
            ';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->CandidatesM->count_all2(),
            "recordsFiltered" => $this->CandidatesM->count_filtered2(),
            "data" => $data,
        );
        echo json_encode($output);
    }



    public function dataParticipant()
    {
        $json = [];

        if (!empty($this->input->get("q"))) {
            $this->db->like('email', $this->input->get("q"));
            $query = $this->db->select('id,email as text')
                ->get("participans");
            $json = $query->result();
        }


        echo json_encode($json);
    }

    public function candidateDataEdit($id){
        $data = $this->CandidatesM->get_by_idEdit($id); 
        echo json_encode($data);
    }



    function validadmin()
    {

        if (!empty($this->data['isadmin'])) {
            return true;
        } else {
            return false;
        }
    }


    public function candidateAdd()
    {

        $config['upload_path'] = './assets/candidate/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;

        $this->upload->initialize($config);

        if (!empty($_FILES['image']['name'])) {
            if ($this->upload->do_upload('image')) {
                $img = $this->upload->data();

                $image                    = $img['file_name'];
                $participant              = $this->input->post('candidate_id', true);
                $concept                  = $this->input->post('concept', true);
                $ig                       = $this->input->post('ig', true);
                $ig_uri                   = $this->input->post('ig_uri', true);
                $set_win                  = $this->input->post('set_win', true);
                $data = [
                    'participants_id'   => $participant,
                    'concept_name'      => $concept,
                    'instagram'         => $ig,
                    // 'set_win'           => $set_win,
                    'thumb'             => $image,
                    'ig_uri'            => $ig_uri

                ];
                $this->db->set('uuid_sub', 'UUID()', FALSE);
                $result = $this->CandidatesM->save($data);
                echo json_encode($result);
            } else {
                echo json_encode('Gagal');
            }

        }else{
                $participant              = $this->input->post('candidate_id', true);
                $concept                  = $this->input->post('concept', true);
                $ig                       = $this->input->post('ig', true);
                $ig_uri                   = $this->input->post('ig_uri', true);
                $set_win                  = $this->input->post('set_win', true);
                $data = [
                    'participants_id'   => $participant,
                    'concept_name'      => $concept,
                    'instagram'         => $ig,
                    // 'set_win'           => $set_win,
                    'ig_uri'            => $ig_uri

                ];
                $this->db->set('uuid_sub', 'UUID()', FALSE);
                $result = $this->CandidatesM->save($data);
                echo json_encode($result);
        }
    }

    public function candidateUpdate()
    {

        $config['upload_path'] = './assets/candidate/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $config['remove_spaces'] = TRUE;

        $this->upload->initialize($config);

        if (!empty($_FILES['image']['name'])) {
            if ($this->upload->do_upload('image')) {
                $img = $this->upload->data();

                $image                    = $img['file_name'];
                $concept                  = $this->input->post('concept', true);
                $ig                       = $this->input->post('ig', true);
                $ig_uri                   = $this->input->post('ig_uri', true);
                $tag1                       = $this->input->post('tag1', true);
                $tag2                       = $this->input->post('tag2', true);
                $data = [
                    'concept_name'      => $concept,
                    'instagram'         => $ig,
                    'tag1'              => $tag1,
                    'tag2'              => $tag2,
                    'thumb'             => $image,
                    'ig_uri'            => $ig_uri

                ];
                $this->CandidatesM->update(array('id_sub' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
            } else {
                echo json_encode('Gagal');
            }

        }else{
                $concept                  = $this->input->post('concept', true);
                $ig                       = $this->input->post('ig', true);
                $ig_uri                   = $this->input->post('ig_uri', true);
                // $set_win                  = $this->input->post('set_win', true);
                $tag1                       = $this->input->post('tag1', true);
                $tag2                       = $this->input->post('tag2', true);
                $data = [
                    'concept_name'      => $concept,
                    'instagram'         => $ig,
                    // 'set_win'           => $set_win,
                    'tag1'              => $tag1,
                    'tag2'              => $tag2,
                    'ig_uri'            => $ig_uri

                ];
                $this->CandidatesM->update(array('id_sub' => $this->input->post('id')), $data);
                echo json_encode(array("status" => TRUE));
        }
    }

    // gallery
    public function addPhoto()
    {
        $config['upload_path'] = './assets/candidate/gallery/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 8000;
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);

        if ($this->upload->do_upload('userfile')) {
            $nama = $this->upload->data('file_name');
            $id = $this->input->post('id');
            $token = $this->input->post('token_foto');
            $data = [
                'gallery_name'      => $nama,
                'submission_id'        => $id,
                'gallery_token'     => $token
            ];
            $result = $this->CandidatesM->saveImage($data);
            echo json_encode($result);
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode(['error' => $error]);
        }
    }


    public function gallery($id)
    {
        $data['menu'] = $this->menu;
        $data['sub_menu'] = "";
        $data['ctrl'] = $this->ctrl;
        $data['page_title'] = $this->title;
        if ($this->validadmin()) {
            $data['datagallery'] = $this->CandidatesM->getMockup($id);
            $data['data'] = $this->CandidatesM->get_by_id($id);
            $this->backend->display('backend/galleryV', $data);
        } else {
            $this->data['pagetitle'] = 'Esse - Make Change | Login';
            $this->load->view('backend/loginV', $this->data);
        }
    }

    public function photoDelete($id)
    {

        $foto = $this->db->get_where('gallery', array('id_gallery' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->gallery_name;
            if (file_exists($file = './assets/candidate/gallery/' . $nama_foto)) {
                unlink($file);
            }
            $this->CandidatesM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }

    public function deleteCandidate($id)
    {

        $foto = $this->db->get_where('submission', array('id_sub' => $id));
        $thumb = $this->db->get_where('gallery', array('id_gallery' => $id));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->thumb;
            if (file_exists($file = './assets/candidate/' . $nama_foto)) {
                unlink($file);
            }
            $this->CandidatesM->delete($id);
        }
        if ($thumb->num_rows() > 0) {
            $hasil = $thumb->row();
            $nama_foto = $hasil->gallery_name;
            if (file_exists($file = './assets/candidate/gallery/' . $nama_foto)) {
                unlink($file);
            }
            $this->CandidatesM->delete_by_id($id);
        }
        echo json_encode(array("status" => TRUE));
    }

}
