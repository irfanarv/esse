<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vote extends CI_Controller
{

	private $ctrl = "vote";
	private $title = "Vote Your Favorite Artist";
	private $menu = "vote";


	function __construct()
	{

		parent::__construct();
		$this->load->model('VisitorsM');
		$this->load->model('CandidatesM');
		$this->load->model('AuthM');
		$this->VisitorsM->count_visitor();
	}


	public function index()
	{

		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/index.php";
		$data['datastudent'] = $this->CandidatesM->getStudent();
		$data['datageneral'] = $this->CandidatesM->getGeneral();
		$data['datawinner'] = $this->CandidatesM->getWinner();
		if ($this->session->userdata('oauth_uid')) {
			// $data['verified'] = $this->session->userdata('oauth_uid');
			// $data['validEmail'] = $this->session->userdata('email_valid');
			$this->load->view(FRONTEND_LAYOUT, $data);
		} else {
			$client = new Google_Client();
			// $client->setClientId('968536272215-r8d9v097fbuqnrio0ab5f93pqjn03k22.apps.googleusercontent.com');
			// $client->setClientSecret('Jk7XZyHUzOoWEYhjleoQx1yn');
			// $client->setRedirectUri('https://webly.id/esse-4/vote-now');
			// $client->setRedirectUri('https://makechange.id/vote-now');
			// $client->setRedirectUri('http://localhost/esse/phase-4/vote-now');
			// $client->addScope('email');
			// $client->addScope('profile');

			// if (isset($_GET["code"])) {
			// 	$token = $client->fetchAccessTokenWithAuthCode($_GET["code"]);
			// 	if (!isset($token["error"])) {
			// 		$client->setAccessToken($token['access_token']);
			// 		$this->session->set_userdata('access_token', $token['access_token']);
			// 		$google_service = new Google_Service_Oauth2($client);
			// 		$data = $google_service->userinfo->get();
			// 		$current_datetime = date('Y-m-d H:i:s');
			// 		if ($this->AuthM->google_already($data['id'])) {
			// 			$user_data = array(
			// 				'first_name' => $data['given_name'],
			// 				'last_name'  => $data['family_name'],
			// 				'valid_email' => $data['email'],
			// 				'picture' => $data['picture'],
			// 				'modified' => $current_datetime
			// 			);
			// 			$this->AuthM->update_google_user($user_data, $data['id']);
			// 		} else {
			// 			$user_data = array(
			// 				'oauth_provider' => ('google'),
			// 				'oauth_uid' => $data['id'],
			// 				'first_name'  => $data['given_name'],
			// 				'last_name'   => $data['family_name'],
			// 				'valid_email'  => $data['email'],
			// 				'picture' => $data['picture'],
			// 				'created'  => $current_datetime
			// 			);
			// 			$this->AuthM->insert_google_user($user_data);
			// 		}
			// 		$this->session->set_userdata('user_data', $user_data);
			// 		$this->session->set_userdata('oauth_uid', $data['id']);
			// 		$this->session->set_userdata('email_valid', $data['email']);
			// 		echo $this->session->set_flashdata('msg', 'sukses');
			// 	}
			// 	redirect('vote-now');
			// }

			// $data['loginGoogle'] = $client->createAuthUrl();
			$this->load->view(FRONTEND_LAYOUT, $data);
		}
	}


	public function detail($id)
	{
		$data['menu'] = $this->menu;
		$data['sub_menu'] = "";
		$data['ctrl'] = $this->ctrl;
		$data['page_title'] = $this->title;
		$data['content_view'] = "$this->ctrl/detail.php";
		$data['datacandidate'] = $this->CandidatesM->getDetail($id);
		$data['gallery'] = $this->CandidatesM->getGallery($id);
		// $data['verified'] = $this->session->userdata('oauth_uid');
		// $data['validEmail'] = $this->session->userdata('email_valid');
		if ($this->session->userdata('oauth_uid')) {
			// $data['verified'] = $this->session->userdata('oauth_uid');
			// $data['validEmail'] = $this->session->userdata('email_valid');
			$this->load->view(FRONTEND_LAYOUT, $data);
		} else {
			// $client = new Google_Client();
			// $client->setClientId('968536272215-r8d9v097fbuqnrio0ab5f93pqjn03k22.apps.googleusercontent.com');
			// $client->setClientSecret('Jk7XZyHUzOoWEYhjleoQx1yn');
			// $client->setRedirectUri('https://webly.id/esse-4/vote-now');
			// $client->setRedirectUri('https://makechange.id/vote-now');
			// $client->setRedirectUri('http://localhost/esse/phase-4/vote-now');
			// $client->addScope('email');
			// $client->addScope('profile');

			// if (isset($_GET["code"])) {
			// 	$token = $client->fetchAccessTokenWithAuthCode($_GET["code"]);
			// 	if (!isset($token["error"])) {
			// 		$client->setAccessToken($token['access_token']);
			// 		$this->session->set_userdata('access_token', $token['access_token']);
			// 		$google_service = new Google_Service_Oauth2($client);
			// 		$data = $google_service->userinfo->get();
			// 		$current_datetime = date('Y-m-d H:i:s');
			// 		if ($this->AuthM->google_already($data['id'])) {
			// 			$user_data = array(
			// 				'first_name' => $data['given_name'],
			// 				'last_name'  => $data['family_name'],
			// 				'valid_email' => $data['email'],
			// 				'picture' => $data['picture'],
			// 				'modified' => $current_datetime
			// 			);
			// 			$this->AuthM->update_google_user($user_data, $data['id']);
			// 		} else {
			// 			$user_data = array(
			// 				'oauth_provider' => ('google'),
			// 				'oauth_uid' => $data['id'],
			// 				'first_name'  => $data['given_name'],
			// 				'last_name'   => $data['family_name'],
			// 				'valid_email'  => $data['email'],
			// 				'picture' => $data['picture'],
			// 				'created'  => $current_datetime
			// 			);
			// 			$this->AuthM->insert_google_user($user_data);
			// 		}
			// 		$this->session->set_userdata('user_data', $user_data);
			// 		$this->session->set_userdata('oauth_uid', $data['id']);
			// 		$this->session->set_userdata('email_valid', $data['email']);
			// 		echo $this->session->set_flashdata('msg', 'sukses');
			// 	}
			// 	redirect('vote-now');
			// }

			// $data['loginGoogle'] = $client->createAuthUrl();
			$this->load->view(FRONTEND_LAYOUT, $data);
		}
	}

	public function voteWinners()
	{

		$account 	= $this->input->post('valid_email');
		$id_sub 	= $this->input->post('id_sub');
		if ($this->input->is_ajax_request()) {
			$countWin = $this->CandidatesM->winCount($account);
			$countLimit = $countWin['count_v'];
			if ($countLimit == 0) {
				$checkAccount = $this->CandidatesM->checkVote($account,$id_sub);
				$cek = $checkAccount;
				if($cek == ''){
					$this->CandidatesM->updateSub($id_sub);
					$this->CandidatesM->VoteWin($account);
					$data = [
						'account'	  => $account,
						'sub_id'      => $id_sub
	
					];
					$this->CandidatesM->saveVote($data);
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(['error' => 'You have already voted this artist, please choose another one !']);
				}
				
			} else {
				echo json_encode(['error' => 'You have voted for the winners category']);
			}
		}
	}

	public function voteGeneral()
	{

		$account 	= $this->input->post('valid_email');
		$id_sub 	= $this->input->post('id_sub');
		if ($this->input->is_ajax_request()) {
			$countWin = $this->CandidatesM->genCount($account);
			$countLimit = $countWin['count_v'];
			if ($countLimit < 3) { 
				$checkAccount = $this->CandidatesM->checkVote($account,$id_sub);
				$cek = $checkAccount;
				if($cek == ''){
					$this->CandidatesM->updateSub($id_sub);
					$this->CandidatesM->VoteGen($account);
					$data = [
						'account'	  => $account,
						'sub_id'      => $id_sub
	
					];
					$this->CandidatesM->saveVote($data);
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(['error' => 'You have already voted this artist, please choose another one !']);
				}
			} else {
				echo json_encode(['error' => 'You have exceeded the maximum number of votes on General Category']);
			}
		}
	}

	public function voteStudent()
	{

		$account 	= $this->input->post('valid_email');
		$id_sub 	= $this->input->post('id_sub');
		if ($this->input->is_ajax_request()) {
			$countWin = $this->CandidatesM->studentCount($account);
			$countLimit = $countWin['count_v'];
			if ($countLimit < 6) {
				$checkAccount = $this->CandidatesM->checkVote($account,$id_sub);
				$cek = $checkAccount;
				if($cek == ''){
					$this->CandidatesM->updateSub($id_sub);
					$this->CandidatesM->VoteStudent($account);
					$data = [
						'account'	  => $account,
						'sub_id'      => $id_sub
	
					];
					$this->CandidatesM->saveVote($data);
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(['error' => 'You have already voted this artist, please choose another one !']);
				}
				
			} else {
				echo json_encode(['error' => 'You have exceeded the maximum number of votes on Student Category']);
			}
		}
	}

	public function voteDesignDetail()
	{

		$account 	= $this->input->post('valid_email');
		$id_sub 	= $this->input->post('id_sub');
		if ($this->input->is_ajax_request()) {
			$countWin 	 = $this->CandidatesM->winCount($account);
			$countStudent = $this->CandidatesM->studentCount($account);
			$countGen = $this->CandidatesM->genCount($account);
			$countLimit1 = $countWin['count_v'];
			$countLimit2 = $countStudent['count_v'];
			$countLimit3 = $countGen['count_v'];
			if ($countLimit1 == 0) {
				$checkAccount = $this->CandidatesM->checkVote($account,$id_sub);
				$cek = $checkAccount;
				if($cek == ''){
					$this->CandidatesM->updateSub($id_sub);
					$this->CandidatesM->VoteWin($account);
					$data = [
						'account'	  => $account,
						'sub_id'      => $id_sub
	
					];
					$this->CandidatesM->saveVote($data);
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(['error' => 'You have already voted this artist, please choose another one !']);
				}

			} elseif($countLimit2 < 6){
				$checkAccount = $this->CandidatesM->checkVote($account,$id_sub);
				$cek = $checkAccount;
				if($cek == ''){
					$this->CandidatesM->updateSub($id_sub);
					$this->CandidatesM->VoteStudent($account);
					$data = [
						'account'	  => $account,
						'sub_id'      => $id_sub
	
					];
					$this->CandidatesM->saveVote($data);
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(['error' => 'You have already voted this artist, please choose another one !']);
				}

			} elseif($countLimit3 < 3){
				$checkAccount = $this->CandidatesM->checkVote($account,$id_sub);
				$cek = $checkAccount;
				if($cek == ''){
					$this->CandidatesM->updateSub($id_sub);
					$this->CandidatesM->VoteGen($account);
					$data = [
						'account'	  => $account,
						'sub_id'      => $id_sub
	
					];
					$this->CandidatesM->saveVote($data);
					echo json_encode(array("status" => TRUE));
				}else{
					echo json_encode(['error' => 'You have already voted this artist, please choose another one !']);
				}
			}else {
				echo json_encode(['error' => 'You have exceeded the maximum number of votes']);
			}
		}
	}
}
