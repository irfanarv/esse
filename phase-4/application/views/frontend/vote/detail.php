<?php
$this->data['isuser'] = $this->session->userdata('id');
?>
<link rel="stylesheet" href="<?php echo base_url(CSS . 'glightbox.css') ?>">

<style>
	.modal-dialog {
		min-height: calc(100vh - 60px);
		display: flex;
		flex-direction: column;
		justify-content: center;
	}

	@media(max-width: 768px) {
		.modal-dialog {
			min-height: calc(100vh - 20px);
		}
	}

	.toggledText span.trimmed {
		display: none;
	}

	.read-more .more:before {
		content: 'Show All';
	}

	.showAll .toggledText span.morePoints {
		display: none;
	}

	.showAll .toggledText span.trimmed {
		display: inline;
	}

	.showAll .read-more .more:before {
		content: 'Show Less';
	}
</style>
<script>
	function hover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'brand-hov.png') ?>');
	}

	function unhover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
	}

	function hover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
	}

	function unhover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
	}

	function ig_hov(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'Instagram.png') ?>');
	}

	function ig_unhov(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'Instagram-1.png') ?>');
	}
	// home
	function hover(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'home-hov.png') ?>');
	}

	function unhover(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
	}

	// competition
	function hover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
		$('.hov').show();
		$('.hov-1').hide();
	}
	$(document).ready(function() {
		$('.hov').mouseleave(function() {
			$('.hov').hide();
		});

		var charLimit = 446;

		var numberOfToggled = document.getElementsByClassName('toggledText');
		for (i = 0; i < numberOfToggled.length; i++) {

			var el = numberOfToggled[i];
			var elText = el.innerHTML.trim();

			if (elText.length > charLimit) {
				var showStr = elText.slice(0, charLimit);
				var hideStr = elText.slice(charLimit);
				el.innerHTML = showStr + '<span class="morePoints">...</span> <span class="trimmed">' + hideStr + '</span>';
				el.parentElement.innerHTML = el.parentElement.innerHTML + "<div class='read-more'><a href='#' class='more'></a>";
			}

		}

		window.onclick = function(event) {
			if (event.target.className == 'more') {
				event.preventDefault();
				event.target.parentElement.parentElement.classList.toggle('showAll');

			}
		}

	});
	$(document).ready(function() {

		$('.slider-detail').slick({
			dots: false,
			autoplay: true,
			dots: false,
			prevArrow: false,
			nextArrow: false,
			autoplaySpeed: 2000,
			infinite: false,
			speed: 500
		});

		// $('.slider').slick({
		// 	draggable: true,
		// 	autoplay: true,

		// 	autoplaySpeed: 2000,
		// 	infinite: true,
		// 	slidesToShow: 5,
		// 	slidesToScroll: 5,
		// 	prevArrow: '<button class="slide-arrow prev-arrow"></button>',
		// 	nextArrow: '<button class="slide-arrow next-arrow"></button>',
		// 	responsive: [{
		// 			breakpoint: 1024,
		// 			settings: {
		// 				slidesToShow: 3,
		// 				slidesToScroll: 3,
		// 				autoplaySpeed: 1000,
		// 				speed: 500,
		// 				infinite: true,
		// 				dots: true
		// 			}
		// 		},
		// 		{
		// 			breakpoint: 600,
		// 			settings: {
		// 				autoplaySpeed: 1000,
		// 				speed: 500,
		// 				slidesToShow: 2,
		// 				slidesToScroll: 2
		// 			}
		// 		},
		// 		{
		// 			breakpoint: 480,
		// 			settings: {
		// 				slidesToShow: 2,
		// 				slidesToScroll: 1
		// 			}
		// 		}
		// 	]
		// });
	});

	function unhover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'COMPETITION.png') ?>');
	}
	// join
	function hover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
		$('.hov-1').show();
		$('.hov').hide();
	}

	$(document).ready(function() {
		$('.hov-1').mouseleave(function() {
			$('.hov-1').hide();
		});
	});

	function unhover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
	}

	// about competition

	function hover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>');
	}

	function unhover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition.svg') ?>');
	}


	$(document).ready(function() {
		AOS.init();
	});
</script>

<!-- menu mobile -->
<script>
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip({
			trigger: 'hover'
		})
		$('[data-toggle="popover"]').popover({
			trigger: 'hover'
		})
		// vote win
		// $('.vote-btn-detail').click(function() {
		// 	var id_sub = $(this).data("idsub");
		// 	var valid_email = $(this).data("validemail");

		// 	$.ajax({
		// 		url: "<?php echo base_url(); ?>Vote/voteDesignDetail",
		// 		method: "POST",
		// 		data: {
		// 			id_sub: id_sub,
		// 			valid_email: valid_email
		// 		},
		// 		success: function(data) {
		// 			var resp = $.parseJSON(data);
		// 			console.log(resp);
		// 			if (!resp.status) {
		// 				$('#vote-failed').modal('show');
		// 				$(".print-error-msg").html(resp.error);

		// 			} else {
		// 				$('#vote-success').modal('show');
		// 			}
		// 		}
		// 	});
		// });
	});
</script>

<div class="modal fade" id="vote-failed">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center p-3 pt-4 pb-5 mb-3">

				<div class="row">
					<div class="col-md-12">
						<h6 class="print-error-msg"></h6>
					</div>
					<div class="col-md-12 mt-4">
						<a class="btn btn-primary" href="<?php echo base_url('vote-now') ?>">Reload
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="vote-success">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center p-3 pt-4 pb-5 mb-3">

				<div class="row">
					<div class="col-md-12">
						<h6>Vote Successful Thank You!</h6>
					</div>

				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" style="height:18px" /></a><br>
				<a href="<?php echo base_url('vote-now') ?>"><img src="<?php echo base_url(IMG3 . 'gallery-active.png') ?>" class="img-fluid px-2 mbm" style="height:30px" /></a><br>
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" style="height:18px" /></a>
				<!-- competition -->
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
				<!-- sub menu -->
				<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutvw" style="display: none;" href="<?php echo base_url('about-competition') ?>#virtual-workshop"><img src="<?php echo base_url(IMGS . 'vw-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutdg" style="display: none;" href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="abouttem" style="display: none;" href="<?php echo base_url(DOCS . 'ESSE_MAKECHANGE_CompetitionTemplate.pdf') ?>"><img src="<?php echo base_url(IMGS . 'dt-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutuplo" style="display: none;" href="<?php echo base_url('upload') ?>"><img src="<?php echo base_url(IMGS . 'upload-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
				</ul>
				<!-- sub menu -->
				<!-- competition -->
				<!-- join -->

			</div>

		</div>
	</div>
</div>


<!-- HEADER -->
<div class="bg_com_new">
	<div class="container-fluid header pt-4 px-0">
		<div class="container">
			<div class="row text-center">

				<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>


				<!-- menu -->
				<div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
					<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" style="height:18px" /></a>
					<a href="<?php echo base_url('vote-now') ?>"><img src="<?php echo base_url(IMG3 . 'gallery-active.png') ?>" class="img-fluid px-2" style="height:29px" /></a>
					<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid mt-4 mbm" style="height:18px" /></a>
					<a href="#"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>

				</div>

				<!-- hover -->
				<div class="hov" style="position:relative;display: none;">
					<div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
						<ul class="list-group text-right  d-flex justify-content-end">
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>

		<div class="container-fluid brand-story" style="margin-top: 0px !important;">
			<div class="container">

				<div class="row ">

					<?php foreach ($datacandidate as $ds) : ?>
						<?php if($ds->video == NULL) {?>
						<?php } else { ?>
						<div class="col-md-2 d-none d-md-block "></div>
						<div class="col-md-8 d-none d-md-block">
							<div class="col-md-12 mb-2 pl-3 pr-3">
								<div class="card card-reg card-gallery" style="margin-top:50px !important; margin-bottom:50px; left:0px !important;" data-aos="zoom-in">
									<div class="card-body">
										<div class="embed-responsive embed-responsive-16by9">
											<iframe class="embed-responsive-item" src="<?php echo $ds->video; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-2 d-none d-md-block"></div>
						<?php } ?>

						<div class="col-md-5" data-aos="zoom-out-left">
							<div class="title-gallery detail">
								<?php echo $ds->name; ?>
							</div>
							<div class="row px-3">

								<div class="col-md-6 col-6">
									<div class="loc-gallery"> 
										<img src="<?php echo base_url(IMG3 . 'art.png') ?>" class="img-fluid"> <?php echo $ds->concept_name; ?>
									</div>
								</div>

								<div class="col-md-6 col-6">
									<div class="loc-gallery">
										
										<img src="<?php echo base_url(IMG3 . 'ig.png') ?>" class="img-fluid"> <a href="<?php echo $ds->ig_uri; ?>" target="_blank"> <?php echo $ds->instagram; ?></a>

									</div>
								</div>

								<div class="col-md-6 col-6">
									<div class="loc-gallery">
										<img src="<?php echo base_url(IMG3 . 'maps.png') ?>" class="img-fluid"> <?php echo $ds->city; ?>
									</div>
								</div>

								<div class="col-md-6 col-6">
									<div class="loc-gallery">
										<img src="<?php echo base_url(IMG3 . 'loc.png') ?>" class="img-fluid"> <?php echo $ds->vote_total; ?> Votes
									</div>
								</div>
							</div>
							<div class="truncate d-none d-md-block">
								<div class="toggledText desc-gallery">
									<?php echo $ds->desc; ?>
								</div>
							</div>



							<br>
							<div class="row ">
								<div class="col-md-3"></div>
								<div class="col-md-6 d-md-block d-none">
									<!-- <?php if ($this->session->userdata('oauth_uid')) { ?>
										<button data-idsub="<?php echo $ds->id_sub; ?>" data-validemail="<?php echo $validEmail; ?>" class="vote-btn-detail btn-primary-outline">
											<img src="<?php echo base_url(IMG3 . 'votenow.png') ?>" class="img-fluid vot-detail" />
										</button>
									<?php } else { ?>
										<a href="<?php echo $loginGoogle ?> " class='btn-vote'>
											<img src="<?php echo base_url(IMG3 . 'votenow.png') ?>" class="img-fluid vot-detail" />
										</a>

									<?php } ?> -->

								</div>
								<div class="col-md-3"></div>
							</div>

						</div>
						<!-- video -->
						<?php if($ds->video == NULL) {?>
						<?php } else { ?>
						
						
							<div class="col-md-12 d-block d-md-none">
								<div class="card card-reg card-gallery" style="margin-top:50px !important; margin-bottom:50px; left:0px !important;" data-aos="zoom-in">
									<div class="card-body">
										<div class="embed-responsive embed-responsive-16by9">
											<iframe class="embed-responsive-item" src="<?php echo $ds->video; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
										</div>
									</div>
								</div>
							</div>
						
						
						<?php } ?>

						<div class="col-md-7 align-self-center text-center" data-aos="zoom-out-right">
							<div class="row slider-detail">
								<?php foreach ($gallery as $gl) : ?>
									<div class="col py-3 px-3">
										<div class="card card-reg card-gallery" style="margin-bottom: 1rem !important;">
											<div class="card-body card-reg d-flex justify-content-center">
												<a class="glightbox" href="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $gl->gallery_name; ?>" ><img class="img-fluid" src="<?php echo base_url("assets/"); ?>candidate/gallery/<?php echo $gl->gallery_name; ?>"></a>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="mb-3 col-md-12 align-self-center text-center">
								<a href="https://www.instagram.com/explore/tags/<?= $ds->tag1;?>" target="_blank">#<?= $ds->tag1;?></a> <a href="https://www.instagram.com/explore/tags/<?= $ds->tag2;?>" target="_blank">#<?= $ds->tag2;?></a>
							</div>
						</div>
						<div class="truncate d-block d-md-none px-3">
							<div class="toggledText desc-gallery">
								<?php echo $ds->desc; ?>
							</div>
						</div>
						<div class="col-md-3 d-md-none d-block">
							<?php if ($this->session->userdata('oauth_uid')) { ?>
								<button data-idsub="<?php echo $ds->id_sub; ?>" data-validemail="<?php echo $validEmail; ?>" class="vote-btn-detail btn-primary-outline">
									<img src="<?php echo base_url(IMG3 . 'votenow.png') ?>" class="img-fluid " style="width: 230px; align-items: center; margin-left: 50px; margin-top: 30px;"/>
								</button>
							<?php } else { ?>
								<a href="<?php echo $loginGoogle ?> " class='btn-vote'>
									<img src="<?php echo base_url(IMG3 . 'votenow.png') ?>" class="img-fluid" style="width: 230px; align-items: center; margin-left: 50px; margin-top: 30px;"/>
								</a>

								<?php } ?>

						</div>

						

					<?php endforeach; ?>
				</div>

			</div>
		</div>


	</div>





	<!-- <div class="container-fluid slider-bg " style="margin-top: 28px;">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-6 col-md-6 d-flex justify-content-end mt-5" data-aos="zoom-out-down">
					<a href="javascript:void(0);" onclick="addGeneral()" class="btn-reg">
						<img src="<?php echo base_url(IMAGES . 'general.png') ?>" class="img-btn-reg a a">
					</a>
				</div>
				<div class="col-md-6 col-md-6 d-flex justify-content-start mt-5" data-aos="zoom-out-up">
					<a href="javascript:void(0);" onclick="addStudent()" class="btn-reg">
						<img src="<?php echo base_url(IMAGES . 'student.png') ?>" class="img-btn-reg b a b">
					</a>
				</div>
			</div>
			<div class="col-md-12 align-self-center text-center mt-4" data-aos="zoom-out-right">

				<div class="row slider">
					<a href="<?php echo base_url('/gallery/detail') ?>" class="col-md-2">
						<div class="card card-reg card-gall card-det">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url(IMG3 . 'pack.png') ?>" class="img-fluid">
							</div>
						</div>
						<div class="title-gallery det">
							Ana Maria
						</div>
					</a>
					<a href="<?php echo base_url('/gallery/detail') ?>" class="col-md-2">
						<div class="card card-reg card-gall card-det">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url(IMG3 . 'pack.png') ?>" class="img-fluid">
							</div>
						</div>
						<div class="title-gallery det">
							Ana Maria
						</div>
					</a>
					<a href="<?php echo base_url('/gallery/detail') ?>" class="col-md-2">
						<div class="card card-reg card-gall card-det">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url(IMG3 . 'pack.png') ?>" class="img-fluid">
							</div>
						</div>
						<div class="title-gallery det">
							Ana Maria
						</div>
					</a>
					<a href="<?php echo base_url('/gallery/detail') ?>" class="col-md-2">
						<div class="card card-reg card-gall card-det">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url(IMG3 . 'pack.png') ?>" class="img-fluid">
							</div>
						</div>
						<div class="title-gallery det">
							Ana Maria
						</div>
					</a>
					<a href="<?php echo base_url('/gallery/detail') ?>" class="col-md-2">
						<div class="card card-reg card-gall card-det">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url(IMG3 . 'pack.png') ?>" class="img-fluid">
							</div>
						</div>
						<div class="title-gallery det">
							Ana Maria
						</div>
					</a>
					<a href="<?php echo base_url('/gallery/detail') ?>" class="col-md-2">
						<div class="card card-reg card-gall card-det">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url(IMG3 . 'pack.png') ?>" class="img-fluid">
							</div>
						</div>
						<div class="title-gallery det">
							Ana Maria
						</div>
					</a>
				</div>

			</div>
		</div>

	</div> -->




	<div class="container-fluid mb-5 ig-story">
		<div class="container">
			<div class="row text-center">

				<div class="col-6 offset-3 col-md-8 offset-md-2">
					<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid" /></a>
				</div>

			</div>
		</div>
	</div>


	<!-- FOOTER -->
	<div class="container-fluid footer py-2" style="margin-top: 0px !important;">
		<div class="container">
			<div class="row">

				<div class="col-4 col-md-6 text-left">
					<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
				</div>

				<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
					<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
				</div>

			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(JS . 'glightbox.js') ?>"></script>
<script>
var lightbox = GLightbox({'selector': 'glightbox'});
</script>