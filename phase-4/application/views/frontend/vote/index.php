<?php
$this->data['isuser'] = $this->session->userdata('id');

?>
<style>
	.modal-dialog {
		min-height: calc(100vh - 60px);
		display: flex;
		flex-direction: column;
		justify-content: center;
	}

	@media(max-width: 768px) {
		.modal-dialog {
			min-height: calc(100vh - 20px);
		}
	}
</style>
<script>
	function hover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
	}

	function unhover_logout(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
	}

	function ig_hov(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'Instagram.png') ?>');
	}

	function ig_unhov(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'Instagram-1.png') ?>');
	}
	// home
	function hover(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'home-hov.png') ?>');
	}

	function unhover(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
	}

	function hover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'brand-hov.png') ?>');
	}

	function unhover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
	}

	// competition
	function hover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
		$('.hov').show();
		$('.hov-1').hide();
	}
	$(document).ready(function() {
		$('.hov').mouseleave(function() {
			$('.hov').hide();
		});

	});

	function unhover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'COMPETITION.png') ?>');
	}
	// join
	function hover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
		$('.hov-1').show();
		$('.hov').hide();
	}

	$(document).ready(function() {
		$('.hov-1').mouseleave(function() {
			$('.hov-1').hide();
		});
	});

	function unhover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
	}

	// about competition

	function hover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>');
	}

	function unhover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition.svg') ?>');
	}



	$(document).ready(function() {
		AOS.init();
	});
</script>
<?php if ($this->session->flashdata('msg') == 'sukses') : ?>
	<script>
		$(window).on('load', function() {
			$('#isValidate').modal('show');

		});
	</script>
<?php else : ?>
<?php endif; ?>
<!-- menu mobile -->
<script>
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>

<!-- gallery vote -->
<script>

	function general() {
		$('#general_button').attr('src', '<?php echo base_url(IMG3 . 'general-active.png') ?>');
		$('#student_button').attr('src', '<?php echo base_url(IMG3 . 'student.png') ?>');
		$("#student").hide();
		$("#general").show();
		$("#gen").show();
		$("#gen1").show();
		$("#gen2").show();
		$("#footer1").show();
		$("#footer2").hide(); 
	}

	function student() {
		$('#general_button').attr('src', '<?php echo base_url(IMG3 . 'general.png') ?>');
		$('#student_button').attr('src', '<?php echo base_url(IMG3 . 'student-active.png') ?>');
		$("#student").show();
		$("#general").hide();
		$("#gen").hide();
		$("#gen1").hide();
		$("#gen2").hide();
		$("#footer1").hide();
		$("#footer2").show();
	}

	$(document).ready(function() {
		var btns = document.getElementsByClassName("btn-reg");
		for (var i = 0; i < btns.length; i++) {
			btns[i].addEventListener("click", function() {
				var current = document.getElementsByClassName("active");

				if (current.length > 0) {
					current[0].className = current[0].className.replace(" active", "");
				}
				this.className += " active";
			});
		}
	});
</script>

<!-- vote -->
<!-- <script type="text/javascript">
	$(document).ready(function() {
		
		$('.vote-win').click(function() {
			var id_sub = $(this).data("idsub");
			var valid_email = $(this).data("validemail");

			$.ajax({
				url: "<?php echo base_url(); ?>Vote/voteWinners", 
				method: "POST",
				data: {
					id_sub: id_sub,
					valid_email: valid_email
				},
				success: function(data) {
					var resp = $.parseJSON(data);
					console.log(resp);
					if (!resp.status) {
						$('#vote-failed').modal('show');
						$(".print-error-msg").html(resp.error);

					} else {
						$('#vote-success').modal('show');
						$("#dg"+id_sub).load(window.location.href + " #dg"+id_sub);
					} 
				}
			});
		});
	});
</script> -->

<!-- vote -->
<!-- <script type="text/javascript">
	$(document).ready(function() {
		
		$('.vote-gen').click(function() {
			var id_sub = $(this).data("idsub");
			var valid_email = $(this).data("validemail");

			$.ajax({
				url: "<?php echo base_url(); ?>Vote/voteGeneral",
				method: "POST",
				data: {
					id_sub: id_sub,
					valid_email: valid_email
				},
				success: function(data) {
					var resp = $.parseJSON(data);
					console.log(resp);
					if (!resp.status) {
						$('#vote-failed').modal('show');
						$(".print-error-msg").html(resp.error);

					} else {
						$('#vote-success').modal('show');
						$("#dg"+id_sub).load(window.location.href + " #dg"+id_sub);
					}
				}
			});
		});
	});
</script> -->

<!-- vote -->
<!-- <script type="text/javascript">
	$(document).ready(function() {
		
		
		$('.vote-student').click(function() {
			var id_sub = $(this).data("idsub");
			var valid_email = $(this).data("validemail");

			$.ajax({
				url: "<?php echo base_url(); ?>Vote/voteStudent",
				method: "POST",
				data: {
					id_sub: id_sub,
					valid_email: valid_email
				},
				success: function(data) {
					var resp = $.parseJSON(data);
					console.log(resp);
					if (!resp.status) {
						$('#vote-failed').modal('show');
						$(".print-error-msg").html(resp.error);

					} else {
						$('#vote-success').modal('show');
						$("#dg"+id_sub).load(window.location.href + " #dg"+id_sub);
					}
				}
			});
		});
		
	});
</script> -->

<div class="modal fade" id="isValidate" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center p-3 pt-4 pb-5 mb-3">

				<div class="row">
					<div class="col-md-12">
						Your are verified !
					</div>
					<div class="col-md-12 mt-4">
						<a type="button" href="<?php echo base_url(); ?>vote-now" class="btn btn-primary">Vote Now</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="vote-success">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center p-3 pt-4 pb-5 mb-3">

				<div class="row">
					<div class="col-md-12">
						<h6>Vote Successful Thank You!</h6>
					</div>
					<div class="col-md-12 mt-4">
						<a class="btn btn-primary" data-dismiss="modal" href="javascript:void(0);">Want to Vote Again?
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="vote-failed">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center p-3 pt-4 pb-5 mb-3">

				<div class="row">
					<div class="col-md-12">
						<h6 class="print-error-msg"></h6>
					</div>
					<div class="col-md-12 mt-4">
						<a class="btn btn-primary" href="<?php echo base_url('vote-now') ?>">Reload
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>


<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" style="height:18px" /></a><br>
				<img src="<?php echo base_url(IMG3 . 'gallery-active.png') ?>" class="img-fluid px-2 mbm" style="height:30px" /><br>
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" style="height:18px" /></a>
				<!-- competition -->
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
				<!-- sub menu -->
				<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutdg" style="display: none;" href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>

				</ul>
				<!-- sub menu -->
				<!-- competition -->
				<!-- join -->


			</div>

		</div>
	</div>
</div>


<!-- HEADER -->
<div class="judge-info" style="background-repeat: repeat; background-position: bottom;">
	<div class="container-fluid header pt-4 px-0">
		<div class="" style="position:absolute;z-index:-1;">
			<!-- <img src="<?php echo base_url(IMG3 . 'bg_com.png') ?>" class="img-fluid bg_com_new" /> -->
		</div>
		<div class="container">
			<div class="row text-center mb-3">
				<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>
				<!-- menu -->
				<div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
					<a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" style="height:18px" /></a>
					<img src="<?php echo base_url(IMG3 . 'gallery-active.png') ?>" class="img-fluid px-2" style="height:29px" />
					<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid mt-4 mbm" style="height:18px" /></a>
					<a href="#"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>

				</div>
				<!-- hover -->
				<div class="hov" style="position:relative;display: none;">
					<div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
						<ul class="list-group text-right  d-flex justify-content-end">
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
							<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
						</ul>
					</div>
				</div>


			</div>
		</div>

		<div class="container-fluid" style="margin-top: 0px !important; padding-bottom:0px !important;">
			<div class="container">
				<div class="row text-center">
					<div class="col-md-12 mb-5 d-none d-md-block" style="z-index: -1 !important;" data-aos="zoom-in-up">
						<img src="<?php echo base_url(IMG3 . 'shortlisted.png') ?>" class="img-fluid" style="width: 300px;" />
					</div>

					<div class="col-md-12 d-block d-md-none" style="z-index: -1 !important; margin-bottom: 2.5rem;" data-aos="zoom-in-up">
						<img src="<?php echo base_url(IMG3 . 'shortlisted.png') ?>" class="img-fluid" style="width: 150px;" />
					</div>

					<div class=" col-md-6 col-md-6  mt-3" data-aos="zoom-out-down">
						<a href="javascript:void(0);" onclick="general()" class="btn-reg active"> 
							<img id="general_button" src="<?php echo base_url(IMG3 . 'general-active.png') ?>" class="img-btn-reg a a">
						</a>
					</div>
					<div class="col-md-6 col-md-6  mt-3" data-aos="zoom-out-up">
						<a href="javascript:void(0);" onclick="student()" class="btn-reg">

							<img id="student_button" src="<?php echo base_url(IMG3 . 'student.png') ?>" class="img-btn-reg b a">
						</a>
					</div>
					<!-- button mobile -->

					<div style="position:relative">
						<div style="position:absolute;top:20px;right:0px" class="d-none d-md-block">
							<img src="<?php echo base_url(IMG . 'green brush.png') ?>" class="img-fluid" style="width:120px">
						</div>
					</div>

					<div class="col-md-12 align-self-center text-center" data-aos="zoom-out-right" id="general">

						<div class="row d-flex justify-content-center vote-mb-info">
							<!-- desktop -->
							<div class="col-md-1 mb-3 d-md-block d-none"></div>
							<div class="col-md-3 mb-3 d-md-block d-none">
								<div class="row">
									<div class="col-md-12 mb-5">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'hadiah.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<img id="img-prize-3" src="<?php echo base_url(IMGS . 'list-win-gen.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
									</div>
								</div>
							</div>
							<div class="col-md-1 mb-3 d-md-block d-none"></div>
							<div class="col-md-6 mb-3 d-md-block d-none">
								<div class="row">
									<div class="col-md-12 mb-5">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'vote-mec-gen.png') ?>" class="img-fluid" data-aos="zoom-out" />
									</div>
								</div>
							</div>
							<!-- mobile -->
							<div class="col-md-12 d-md-none d-block">
								<div class="row">
									<div class="col-md-12 mb-3" style="margin-top: -1.5rem;">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'vote-mec-gen-mob-1.png') ?>" class="img-fluid" data-aos="zoom-out" />
									</div>
									<div class="col-12 d-md-none d-block">
										<div class="row">
										<div class="col-6 mt-4">
											<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'hadiah.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
										</div>
										<div class="col-6">
											<img id="img-prize-3" src="<?php echo base_url(IMGS . 'list-win-gen.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
										</div>
										</div>
									</div>
									<div class="col-md-12 mt-3">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'vote-mec-gen-mob-2.png') ?>" class="img-fluid" data-aos="zoom-out" />
									</div>
								</div>
							</div>
						</div>

					</div>

					<div class="col-md-12 align-self-center text-center" data-aos="zoom-out-right" id="student" style="display: none;">

						<div class="row d-flex justify-content-center">
							<div class="col-md-1 mb-3 d-md-block d-none"></div>
							<div class="col-md-3 mb-3 d-md-block d-none">
								<div class="row">
									<div class="col-md-12 mb-5">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'hadiah-1.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<img id="img-prize-3" src="<?php echo base_url(IMGS . 'list-win-stu.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
									</div>
								</div>
							</div>
							<div class="col-md-1 mb-3 d-md-block d-none"></div>
							<div class="col-md-6 mb-3 d-md-block d-none">
								<div class="row">
									<div class="col-md-12 mb-5">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'vote-mec-stu.png') ?>" class="img-fluid" data-aos="zoom-out" />
									</div>
								</div>
							</div>

							<div class="col-md-12 d-md-none d-block">
								<div class="row">
									<div class="col-md-12 mb-3" style="margin-top: -1.5rem;">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'vote-mec-gen-mob-1.png') ?>" class="img-fluid" data-aos="zoom-out" />
									</div>
									<div class="col-12 d-md-none d-block">
										<div class="row">
										<div class="col-6 mt-4">
											<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'hadiah-1.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
										</div>
										<div class="col-6">
											<img id="img-prize-3" src="<?php echo base_url(IMGS . 'list-win-stu.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
										</div>
										</div>
									</div>
									<div class="col-md-12 mt-3">
										<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'vote-mec-stu-mob-2.png') ?>" class="img-fluid" data-aos="zoom-out" />
									</div>
								</div>
							</div>

						</div>

						<div class="row d-flex  justify-content-center">
							<div class="col-md-12 text-center mb-5 d-md-block d-none" style="z-index:1000">
								<img src="<?php echo base_url(IMG3 . 'vote-stu-title.png') ?>" class="img-fluid" data-aos="fade-up" />
							</div>
							<div class="col-md-12 text-center mb-3 mt-3 d-md-none d-block" style="z-index:1000">
								<img src="<?php echo base_url(IMG3 . 'vote-stu-title.png') ?>" class="img-fluid" data-aos="fade-up" />
							</div>
							<?php foreach ($datastudent as $ds) : ?>
								<div class="col-6 col-md-2 mb-3">
									<a href="<?php echo base_url('/vote-now/detail/') ?><?php echo $ds->uuid_sub; ?>">
										<div class="card card-reg card-gall">
											<div class="card-body card-reg d-flex justify-content-center">
												<img src="<?php echo base_url("assets/"); ?>candidate/<?php echo $ds->thumb; ?>" class="img-fluid">
											</div>

										</div>
									</a>
									<div class="title-gallery">
										<div class="name-candidate">
											<?php echo $ds->name; ?>
										</div>
									</div>
									<div class="loc-gallery-vote konsep">
									<div class="detail-konsep">
										<img src="<?php echo base_url(IMG3 . 'art.png') ?>" class="img-fluid"> <?php echo $ds->concept_name; ?>
									</div>
									</div>
									<div class="loc-gallery-vote" id="dg<?php echo $ds->id_sub; ?>">
										<img src="<?php echo base_url(IMG3 . 'loc.png') ?>" class="img-fluid"> <?php echo $ds->vote_total; ?> Votes
									</div>
									<!-- <?php if ($this->session->userdata('oauth_uid')) { ?>
										<button data-idsub="<?php echo $ds->id_sub; ?>" data-validemail="<?php echo $validEmail; ?>" class="vote-student btn-primary-outline">
											<img src="<?php echo base_url(IMG3 . 'vote-stud.png') ?>" class="img-fluid vote-btn" />
										</button>
									<?php } else { ?>
										<a href="<?php echo $loginGoogle ?> " class='btn-vote'>
											<img src="<?php echo base_url(IMG3 . 'vote-stud.png') ?>" class="img-fluid vote-btn" />
										</a>

									<?php } ?> -->



								</div>
							<?php endforeach; ?>
						</div>
					</div>

				</div>
			</div>
		</div>


	</div>
</div>

<!-- bg-blue -->
<div class="mt-5" style="position:relative" id="gen">
	<div class="d-md-block d-none" style="position:absolute;bottom:-35px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
	<div class="d-md-none d-block" style="position:absolute;bottom:-18px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
</div>

<div class="container-fluid participant-cat" style="padding-bottom:0px !important" id="gen1">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="col-md-12 text-center mb-5 d-md-block d-none" style="z-index:1000">
				<img src="<?php echo base_url(IMG3 . 'vote-title-win.png') ?>" class="img-fluid" data-aos="fade-up" />
			</div>
			<div class="col-md-12 text-center mb-3 mt-3 d-md-none d-block" style="z-index:1000">
				<img src="<?php echo base_url(IMG3 . 'vote-title-win-mob.png') ?>" class="img-fluid" data-aos="fade-up" />
			</div>

			
			<?php foreach ($datawinner as $dw) : ?>
				<div class="col-6 col-md-2 mb-3">
					<a href="<?php echo base_url('/vote-now/detail/') ?><?php echo $dw->uuid_sub; ?>">
						<div class="card card-reg card-gall card-det">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url("assets/"); ?>candidate/<?php echo $dw->thumb; ?>" class="img-fluid" style="width: 175px;">
							</div>

						</div>
					</a>
					<div class="title-gallery-bg">
						<div class="name-candidate">
							<?php echo $dw->name; ?>
						</div>
					</div>

					<div class="loc-gallery-vote-bg konsep">
						<div class="detail-konsep">
							<img src="<?php echo base_url(IMG3 . 'art-white.png') ?>" class="img-fluid"> <?php echo $dw->concept_name; ?>
						</div>
					</div>
					<div class="loc-gallery-vote-bg" id="dg<?php echo $dw->id_sub; ?>">
						<img src="<?php echo base_url(IMG3 . 'vote-white.png') ?>" class="img-fluid"> <?php echo $dw->vote_total; ?> Votes
					</div>
					<!-- <?php if ($this->session->userdata('oauth_uid')) { ?>
						<button data-idsub="<?php echo $dw->id_sub; ?>" data-validemail="<?php echo $validEmail; ?>" data-category="winner" class="vote-win btn-primary-outline">
							<img src="<?php echo base_url(IMG3 . 'vote-gen.png') ?>" class="img-fluid vote-btn" />
						</button>
					<?php } else { ?>
						<a href="<?php echo $loginGoogle ?> " class='btn-vote'>
							<img src="<?php echo base_url(IMG3 . 'vote-gen.png') ?>" class="img-fluid vote-btn" />
						</a>

					<?php } ?> -->

				</div>

			<?php endforeach; ?>

		</div>
	</div>

	<div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom-vote">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
	<div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile-toc">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
</div>

<div class="container-fluid gen2" id="gen2">
	<div class="row">
		<div class="col-md-12 align-self-center text-center d-none d-md-block">
			<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'title-gen.png') ?>" class="img-fluid" data-aos="zoom-out" />
		</div>
		<div class="col-md-12 align-self-center text-center d-block d-md-none">
			<img id="img-prize-2" src="<?php echo base_url(IMG3 . 'title-gen-mob.png') ?>" class="img-fluid" data-aos="zoom-out" />
		</div>
	</div>
	<div class="container">
		<div class="row d-flex justify-content-center">
			<?php foreach ($datageneral as $dg) : ?>
				<div class="col-6 col-md-2 mt-5">
					<a href="<?php echo base_url('/vote-now/detail/') ?><?php echo $dg->uuid_sub; ?>">
						<div class="card card-reg card-gall">
							<div class="card-body card-reg d-flex justify-content-center">
								<img src="<?php echo base_url("assets/"); ?>candidate/<?php echo $dg->thumb; ?>" class="img-fluid">
							</div>

						</div>
					</a>
					<div class="title-gallery">
						<div class="name-candidate">
						<?php echo $dg->name; ?>
						</div>
						
						
					</div>
					<div class="loc-gallery-vote konsep">
						<div class="detail-konsep">
						<img src="<?php echo base_url(IMG3 . 'art.png') ?>" class="img-fluid"> <?php echo $dg->concept_name; ?>
						</div>
						
					</div>
					<div class="loc-gallery-vote" id="dg<?php echo $dg->id_sub; ?>">
						<img src="<?php echo base_url(IMG3 . 'loc.png') ?>" class="img-fluid"> <?php echo $dg->vote_total; ?> Votes
					</div>
					<!-- <?php if ($this->session->userdata('oauth_uid')) { ?>
						<button data-idsub="<?php echo $dg->id_sub; ?>" data-validemail="<?php echo $validEmail; ?>" data-category="general" class="vote-gen btn-primary-outline">
							<img src="<?php echo base_url(IMG3 . 'vote-gen.png') ?>" class="img-fluid  vote-btn" />
						</button>
					<?php } else { ?>
						<a href="<?php echo $loginGoogle ?> " class='btn-vote'>
							<img src="<?php echo base_url(IMG3 . 'vote-gen.png') ?>" class="img-fluid  vote-btn" />
						</a>

					<?php } ?> -->

				</div>

			<?php endforeach; ?>

		</div>
	</div>
</div>




<div class="container-fluid  py-5 ig-story" id="footer1">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid" style="width:350px" /></a>
			</div>

		</div>
	</div>
</div>
<div class="container-fluid  py-5 ig-story" id="footer2" style="display: none;">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid" style="width:350px" /></a>
			</div>

		</div>
	</div>
</div>


<!-- FOOTER -->
<div class="container-fluid footer py-2" style="margin-top: 0px !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>