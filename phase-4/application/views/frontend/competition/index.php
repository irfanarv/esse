<?php
$this->data['isuser'] = $this->session->userdata('id');
?>
<style>
  .modal-dialog {
    min-height: calc(100vh - 60px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    /* overflow: auto; */
  }

  @media(max-width: 768px) {
    .modal-dialog {
      min-height: calc(100vh - 20px);
    }
  }



  div#countdown {
    margin-top: -1rem;
  }

  li {
    display: inline-block;
    font-family: Proud Regular;
    font-size: 1em;
    list-style-type: none;
    padding: 1em;
    text-transform: uppercase;
    color: #0062EB;
  }

  li span {
    display: block;
    font-size: 4.5rem;
  }


  @media all and (max-width: 768px) {

    li {
      font-size: 1.125rem;
      padding: .75rem;
    }

    li span {
      font-size: 3.375rem;
    }
  }
</style>

<script>
  function hov_gall(element) {
    element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery-hov.png') ?>');
  }

  function unhov_gall(element) {
    element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery.png') ?>');
  }

  function hov_ud(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'ud-hov.png') ?>');
  }

  function unhov_ud(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'ud.png') ?>');
  }

  function hovreg(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'reg-hov.png') ?>');
  }

  function unhov_reg(element) {
    element.setAttribute('src', '<?php echo base_url(IMG . 'register.png') ?>');
  }

  function hovdt(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'dt-hov.png') ?>');
  }

  function unhovdt(element) {
    element.setAttribute('src', '<?php echo base_url(IMG . 'download.png') ?>');
  }

  function hovdg(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'dg.png') ?>');
  }

  function unhovdg(element) {
    element.setAttribute('src', '<?php echo base_url(IMG . 'Join now.png') ?>');
  }

  function hoversignvir(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew2.svg') ?>');
  }

  function unhoversignvir(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew.svg') ?>');
  }
  // home
  function hover(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES . 'home-hov.png') ?>');
  }

  function unhover(element) {
    element.setAttribute('src', '<?php echo base_url(IMG . 'HOME.png') ?>');
  }

  function hover_logout(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'logout-2.png') ?>');
  }

  function unhover_logout(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'logout.png') ?>');
  }
  // brand
  function hover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES . 'brand-hov.png') ?>');
  }

  function unhover_brand(element) {
    element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
  }
  // competition
  function hover_com(element) {
    element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
    $('.hov').show();
    $('.hov-1').hide();

  }
  $(document).ready(function() {
    $('.hov').mouseleave(function() {
      $('.hov').hide();
    });
  });

  // join
  function hover_join(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
    $('.hov-1').show();
    $('.hov').hide();
  }

  $(document).ready(function() {
    $('.hov-1').mouseleave(function() {
      $('.hov-1').hide();
    });
  });

  function unhover_join(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
  }



  // login

  function hover_login_new(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN-1.svg') ?>');
  }

  function unhover_login_new(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN.svg') ?>');
  }

  // register

  function hover_regis_new(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>');
  }

  function unhover_regis_new(element) {
    element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER.svg') ?>');
  }

  $(document).ready(function() {

    AOS.init();
    $("#header").mousemove(function(e) {
      parallaxHeader(e, "#img-header-1", -60);
      parallaxHeader(e, "#img-header-2", 60);
      parallaxHeader(e, "#img-header-3", -60);
    });

    function parallaxHeader(e, target, movement) {
      var $this = $("#header");
      var relX = e.pageX - $this.offset().left;
      var relY = e.pageY - $this.offset().top;

      TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
      });
    }


    $("#participant-info").mousemove(function(e) {
      parallaxParticipant(e, "#general-bt", -60);
      parallaxParticipant(e, "#students-bt", 60);
    });

    function parallaxParticipant(e, target, movement) {
      var $this = $("#participant-info");
      var relX = e.pageX - $this.offset().left;
      var relY = e.pageY - $this.offset().top;

      TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
      });
    }


    $("#prize").mousemove(function(e) {
      parallaxPrize(e, "#img-prize-1", -10);
      parallaxPrize(e, "#img-prize-2", 40);
      parallaxPrize(e, "#img-prize-3", -40);
      parallaxPrize(e, "#img-prize-4", 40);
      parallaxPrize(e, "#img-prize-5", -40);
    });

    function parallaxPrize(e, target, movement) {
      var $this = $("#prize");
      var relX = e.pageX - $this.offset().left;
      var relY = e.pageY - $this.offset().top;

      TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
      });
    }


    $("#judge-info").mousemove(function(e) {
      parallaxJudge(e, "#judge-1", -100);
      parallaxJudge(e, "#judge-2", 100);
      parallaxJudge(e, "#judge-3", -100);
      parallaxJudge(e, "#judge-4", 100);
    });

    function parallaxJudge(e, target, movement) {
      var $this = $("#judge-info");
      var relX = e.pageX - $this.offset().left;
      var relY = e.pageY - $this.offset().top;

      TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
      });
    }


    $("#youridea").mousemove(function(e) {
      parallaxYourIdea(e, "#img-youridea-1", -40);
      parallaxYourIdea(e, "#img-youridea-2", 20);
      parallaxCriteria(e, "#img-youridea-3", -20);
      parallaxCriteria(e, "#img-youridea-4", 40);
      parallaxCriteria(e, "#img-youridea-5", -20);
      parallaxCriteria(e, "#img-youridea-6", 40);
    });

    function parallaxYourIdea(e, target, movement) {
      var $this = $("#youridea");
      var relX = e.pageX - $this.offset().left;
      var relY = e.pageY - $this.offset().top;

      TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
      });
    }


    $("#criteria").mousemove(function(e) {
      parallaxCriteria(e, "#img-criteria-1", -40);
      parallaxCriteria(e, "#img-criteria-2", 20);
      parallaxCriteria(e, "#img-criteria-3", -20);
      parallaxCriteria(e, "#img-criteria-4", 40);
      parallaxCriteria(e, "#img-criteria-5", 20);
    });

    function parallaxCriteria(e, target, movement) {
      var $this = $("#criteria");
      var relX = e.pageX - $this.offset().left;
      var relY = e.pageY - $this.offset().top;

      TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
      });
    }

    $("#pengumuman").mousemove(function(e) {
      parallaxCriteria(e, "#img-pengumuman-1", -40);
      parallaxCriteria(e, "#img-pengumuman-2", 20);
      parallaxCriteria(e, "#img-pengumuman-3", -20);
      parallaxCriteria(e, "#img-pengumuman-4", 40);
    });

    function parallaxCriteria(e, target, movement) {
      var $this = $("#pengumuman");
      var relX = e.pageX - $this.offset().left;
      var relY = e.pageY - $this.offset().top;

      TweenMax.to(target, 1, {
        x: (relX - $this.width() / 2) / $this.width() * movement,
        y: (relY - $this.height() / 2) / $this.height() * movement
      });
    }

    $('#judge-1').mouseover(function() {
      $('.buble').show();
    });

    $('.buble').mouseleave(function() {
      $('.buble').hide();
    });

    $('#judge-2').mouseover(function() {
      $('.buble2').show();
    });

    $('.buble2').mouseleave(function() {
      $('.buble2').hide();
    });

    $('#judge-3').mouseover(function() {
      $('.buble3').show();
    });

    $('.buble').mouseleave(function() {
      $('.buble3').hide();
    });



  });
</script>

<!-- menu mobile -->
<script>
  // join
  function join() {
    $("#join").hide();
    $("#joincolor").show();
    $("#btnlogmenu").show();
    $("#btnregmenu").show();
  }

  function joincolor() {
    $("#join").show();
    $("#joincolor").hide();
    $("#btnlogmenu").hide();
    $("#btnregmenu").hide();
  }
  // end join
  // competition
  function com() {
    $("#com").hide();
    $("#commob").show();
    $("#ul").show();
    $("#aboutsub").show();
    $("#aboutpr").show();
    $("#aboutvw").show();
    $("#aboutjr").show();
    $("#aboutdg").show();
    $("#abouttem").show();
    $("#abouttoc").show();
    $("#aboutuplo").show();
  }

  function comcolor() {
    $("#commob").hide();
    $("#ul").hide();
    $("#aboutsub").hide();
    $("#aboutpr").hide();
    $("#aboutvw").hide();
    $("#aboutjr").hide();
    $("#aboutdg").hide();
    $("#abouttem").hide();
    $("#abouttoc").hide();
    $("#aboutuplo").hide();
    $("#com").show();
  }
</script>


<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="justify-content-start text-left">
          <img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-right pt-5">
        <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3 mbm" style="height:18px" /></a>
        <br><a href="<?php echo base_url('vote-now') ?>"><img src="<?php echo base_url(IMG3 . 'gallery.png') ?>" onmouseover="hov_gall(this);" onmouseout="unhov_gall(this);" class="img-fluid px-2 mbm" style="height:29px" /></a><br>
        <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2 mbm" style="height:18px" /></a>
        <a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2 mbm" style="height:30px" /></a> </br>
        <a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
        <!-- sub menu -->
        <ul id="ul" style="display: none; margin-bottom:0px;">
          <li class="list-group-item"><a id="aboutsub" style="display: none;" href="#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
          <li class="list-group-item"><a id="aboutpr" style="display: none;" href="#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
          <li class="list-group-item"><a id="aboutjr" style="display: none;" href="#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
          <li class="list-group-item"><a id="aboutdg" style="display: none;" href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>

          <li class="list-group-item"><a id="abouttoc" style="display: none;" href="#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>

        </ul>

        <!-- sub menu -->
        <!-- competition -->

      </div>

    </div>
  </div>
</div>


<!-- HEADER -->
<div class="bg_com_new">
  <div class="container-fluid header pt-4 px-0" id="header">
    <!-- <div class="" style="position:absolute;z-index:-1;">
  <img src="<?php echo base_url(IMG3 . 'bg_com.png') ?>" class="img-fluid bg_com_new" />
  </div> -->
    <div class="container">
      <div class="row text-center banner">

        <div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
          <button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
        </div>


        <!-- menu -->
        <div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
          <a href="<?php echo base_url() ?>"><img src="<?php echo base_url(IMG . 'HOME.png') ?>" onmouseover="hover(this);" onmouseout="unhover(this);" class="img-fluid px-3" style="height:18px" /></a>
          <a href="<?php echo base_url('vote-now') ?>"><img src="<?php echo base_url(IMG3 . 'gallery.png') ?>" onmouseover="hov_gall(this);" onmouseout="unhov_gall(this);" class="img-fluid pr-2" style="height:29px" /></a>
          <a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" style="height:18px" /></a>
          <a href="#"><img src="<?php echo base_url(IMAGES . 'com-hov.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>
        </div>

        <!-- hover -->
        <div class="hov" style="position:relative;display: none;">
          <div class="hov-menu d-none d-md-block">
            <ul class="list-group text-right  d-flex justify-content-end">
              <li class="list-group-item"><a href="#about"><img src="<?php echo base_url(IMGS . 'about-com-active.svg') ?>" class="img-fluid mb-2" style="height:20px" /></a></li>
              <li class="list-group-item"><a href="#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
              <li class="list-group-item"><a href="#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
              <li class="list-group-item"><a href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>

              <li class="list-group-item"><a href="#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>


            </ul>
          </div>
        </div>
        <!-- join -->


        <div class="col-md-12 mb-5 mt-5 d-none d-md-block" style="z-index: -1 !important;" data-aos="zoom-in-up">
          <img src="<?php echo base_url(IMGS . 'header.png') ?>" class="img-fluid" />
        </div>

        <div class="container d-md-none d-block mt-4">
          <div class="row">
            <div class="col-1"></div>
            <div class="col-10  mb-4">
              <img src="<?php echo base_url(IMGS . 'express the change.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
            </div>
            <div class="col-1"></div>
          </div>
        </div>


        <div class="col-md-4 text-left d-none d-md-block" style="position: relative;" data-aos="fade-right">
          <img src=" <?php echo base_url(IMG . 'hand.gif') ?>" class="hand-new" style="width:450px " />
        </div>

        <div class="col-6 offset-3 col-md-4 offset-md-0 mt-0 mt-md-5 d-block d-md-none">
          <img src="<?php echo base_url(IMG3 . 'makechangecom.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
        </div>

        <div class="col-6 offset-3 col-md-4 offset-md-0 mt-0 d-none d-md-block" style="margin-top: 10rem !important;">
          <img src="<?php echo base_url(IMG3 . 'makechangecom.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
        </div>

        <div class="col-6 col-md-4 text-left d-block d-md-none" ">
        <img src=" <?php echo base_url(IMG . 'hand.gif') ?>" class="hand-new" style="width:250px" data-aos="fade-in" />
      </div>

      <div class="col-6 col-md-4 text-right">
        <img id="img-header-3" src="<?php echo base_url(IMG . 'pack.gif') ?>" class="img-fluid" style="width:330px" data-aos="zoom-in-left" />
      </div>

    </div>
  </div>

  <div style="position:relative">
    <div style="position:absolute;bottom:-160px;right:0px;z-index:1000" class="d-none d-md-block">
      <img src="<?php echo base_url(IMG . 'yellow bruh.png') ?>" class="img-fluid" style="width:300px">
    </div>

    <div style="position:absolute;bottom:-50px;right:0px;z-index:1000" class="d-block d-md-none">
      <img src="<?php echo base_url(IMG . 'yellow bruh.png') ?>" class="img-fluid" style="width:100px">
    </div>

    <div style="position:absolute;bottom:-1px;z-index:999">
      <img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
    </div>

  </div>
</div>
</div>

<!-- PARTICIPANT CATEGORIES -->
<div class="container-fluid participant-cat  pb-5" id="participant-info">
  <div class="container">
    <div class="row text-center">

      <div class="col-md-12 mb-5 pb-3 d-none d-md-block mb-3">
        <img src="<?php echo base_url(IMGS . 'greeting.png') ?>" class="img-fluid" style="width: 550px;" data-aos="zoom-in-up" />
      </div>

      <div class="container d-md-none d-block mb-5">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <img src="<?php echo base_url(IMGS . 'greeting.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
          <div class="col-1"></div>
        </div>
      </div>

      <div class=" container-fluid">
        <div class="container">
          <div class="row text-center">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              <div class="card " style="margin-top:0px !important; margin-bottom:50px; left:0px !important;" data-aos="zoom-in-down">
                <div class="card-body">
                  <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                </div>
              </div>

            </div>
            <div class="col-md-2"></div>
          </div>
        </div>
      </div>


      <div class="container d-md-none d-block mb-5">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <img src="<?php echo base_url(IMG . 'PARTICIPANT-CATEGORIES.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
          <div class="col-1"></div>
        </div>
      </div>


      <div class="col-6 col-md-4 offset-md-2" data-aos="zoom-in-down">
        <div class="row mb-4" ">
          <div class=" col-md-12" style="z-index:1000">
          <a href="<?php echo base_url('register?cat=general') ?>"><img id="general-bt" src="<?php echo base_url(IMG . 'General.png') ?>" class="img-fluid" style="width:250px" /></a>
        </div>
      </div>
      <div class="row mt-5 d-none d-md-block">
        <div class="col-md-12" style="z-index:999">
          <img src="<?php echo base_url(IMG . 'general-text-new.png') ?>" class="img-fluid" />
        </div>
      </div>
      <div class="row d-block d-md-none">
        <div class="col-md-12" style="z-index:999">
          <img src="<?php echo base_url(IMG . 'general-text-new.png') ?>" class="img-fluid" />
        </div>
      </div>
    </div>

    <div class="col-6 col-md-4" data-aos="zoom-in-up">
      <div class="row  mb-4" ">
          <div class=" col-md-12" style="z-index:1000">
        <a href="<?php echo base_url('register?cat=students') ?>"><img id="students-bt" src="<?php echo base_url(IMG . 'Students.png') ?>" class="img-fluid" style="width:250px" /></a>
      </div>
    </div>
    <div class="row mt-5 d-none d-md-block">
      <div class="col-md-12" style="z-index:999">
        <img src="<?php echo base_url(IMG . 'students-text-new.png') ?>" class="img-fluid" />
      </div>
    </div>
    <div class="row d-block d-md-none">
      <div class="col-md-12" style="z-index:999">
        <img src="<?php echo base_url(IMG . 'students-text-new.png') ?>" class="img-fluid" />
      </div>
    </div>
  </div>


</div>
</div>
<div class="d-none d-md-block" style="position:relative">
  <div class="bg-bottom">
    <img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
  </div>
</div>
<div class="d-block d-md-none" style="position:relative">
  <div class="bg-bottom-mobile">
    <img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
  </div>
</div>
</div>

<!-- IMPORTANT DATES -->
<div class="container-fluid important-dates py-5">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-12 mb-md-5 mb-4 mt-5 d-none d-md-block">
        <img src="<?php echo base_url(IMG3 . 'impo-new.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
      </div>

      <div class="col-md-12 mb-3 d-block d-md-none">
        <img src="<?php echo base_url(IMG3 . 'impo-new.png') ?>" class="img-fluid" data-aos="zoom-out-down" />
      </div>

      <div class="col-md-12 mb-5" id="pengumuman">
        <div class="row">
          <div class="col-6 col-md-3 mb-4" ">
            <img id=" img-pengumuman-1" src="<?php echo base_url(IMG3 . 'submit design.png') ?>" class="img-fluid" data-aos="zoom-in" />
        </div>
        <div class="col-6 col-md-3 mb-4">
          <img id="img-pengumuman-2" src="<?php echo base_url(IMG3 . 'begin voting.png') ?>" class="img-fluid" data-aos="zoom-out" />
        </div>
        <div class="col-6 col-md-3">
          <img id="img-pengumuman-3" src="<?php echo base_url(IMG3 . '20 finalist.png') ?>" class="img-fluid" data-aos="zoom-in" />
        </div>
        <div class="col-6 col-md-3" ">
            <img id=" img-pengumuman-4" src="<?php echo base_url(IMG3 . 'winner.png') ?>" class="img-fluid" data-aos="zoom-out" />
      </div>
    </div>
  </div>

  <div class="col-8 offset-2 col-md-4 offset-md-4">
    <a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_new">

      <img src="<?php echo base_url(IMG . 'Instagram.png') ?>" class="ig-btn img-fluid" data-aos="fade-in" /></a>
  </div>

</div>
</div>
</div>


<!-- PRIZES -->
<div class="container-fluid prizes" id="prize">
  <div class="container">
    <div class="row">
      <!-- title -->
      <div class="col-md-12 text-center mt-5 d-md-block d-none">
        <img src="<?php echo base_url(IMG . 'prize.png') ?>" class="img-fluid" data-aos="fade-up" />
      </div>

      <div class="container d-md-none d-block mb-5 mt-5">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <img src="<?php echo base_url(IMG . 'prize-mobile.png') ?>" class="img-fluid" data-aos="fade-up" />
          </div>
          <div class="col-1"></div>
        </div>
      </div>


      <!-- price left desk -->
      <div class="col-md-3 mb-3 d-md-block d-none">
        <div class="row">
          <div class="col-md-12 mb-5">
            <img id="img-prize-2" src="<?php echo base_url(IMG . 'hadiah.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <img id="img-prize-3" src="<?php echo base_url(IMGS . 'list-win-gen.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
          </div>
        </div>
      </div>
      <!-- winner center desk-->
      <div class="col-12 col-md-6 text-left d-md-block d-none" style="position: relative;">
        <img id="img-prize-1" src="<?php echo base_url(IMG . 'winner 1.png') ?>" class="img-win" data-aos="zoom-in-up" />
      </div>
      <!-- price right desk-->
      <div class="col-md-3 mb-3 d-md-block d-none">
        <div class="row">
          <div class="col-md-12 mb-5">
            <img id="img-prize-4" src="<?php echo base_url(IMG . 'hadiah-1.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <img id="img-prize-5" src="<?php echo base_url(IMGS . 'list-win-stu.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
          </div>
        </div>
      </div>

      <!-- price left mobile -->
      <div class="col-12 mb-5 d-md-none d-block">
        <div class="row">
          <div class="col-6 mb-5 mt-4">
            <img id="img-prize-2" src="<?php echo base_url(IMG . 'hadiah.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
          <div class="col-6">
            <img id="img-prize-3" src="<?php echo base_url(IMGS . 'list-win-gen.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
          </div>
        </div>
      </div>
      <!-- price right mobile -->
      <div class="col-12 mb-3 d-md-none d-block">
        <div class="row">
          <div class="col-6 ">
            <img id="img-prize-5" src="<?php echo base_url(IMGS . 'list-win-stu.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
          </div>
          <div class="col-6 mb-5 mt-4">
            <img id="img-prize-4" src="<?php echo base_url(IMG . 'hadiah-1.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
        </div>
      </div>

      <!-- vector winner center -->

      <div class="text-center d-md-none d-block">
        <div class="row">
          <div class="col-12">
            <img id="img-prize-1" src="<?php echo base_url(IMG . 'winner 1.png') ?>" class="img-fluid" data-aos="zoom-in" />
          </div>
        </div>

      </div>

    </div>
  </div>
</div>

<!-- JUDGE INFO -->
<div class="container-fluid judge-info  py-md-5 pt-5 pb-0" id="judge-info">
  <div class="container">
    <div class="row text-center">

      <div class="col-md-12 mb-md-5 mb-0">
        <img src="<?php echo base_url(IMG3 . 'judge-info.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
      </div>

      <div class="col-md-12 mb-5 py-5 d-none d-md-block">
        <div class="row">
          <div class="col-4 col-md-4">
            <div style="position: relative;">
            </div>
            <img src="<?php echo base_url(IMAGES . 'buble-1.png') ?>" class="img-fluid buble" />
            <div style="position: relative;" class="buble">
              <a href="javascript:void(0)" data-toggle="modal" data-target="#mandy-profile">
                <img src="<?php echo base_url(IMAGES . 'see-profile.png') ?>" class="img-fluid" style="position:absolute;bottom:6rem;left:7rem;" data-aos="zoom-in-up">
              </a>
            </div>
            <img src="<?php echo base_url(IMG . 'Group 3282.png') ?>" class="img-fluid " id="judge-1" />
          </div>
          <div class=" col-4 col-md-4">
            <img src="<?php echo base_url(IMAGES . 'buble-2.png') ?>" class="img-fluid buble2" />
            <div style="position: relative;" class="buble2">
              <a href="javascript:void(0)" data-toggle="modal" data-target="#muklay-profile">
                <img src="<?php echo base_url(IMAGES . 'see-2.png') ?>" class="img-fluid" style="position:absolute;bottom:5.5rem;left:7rem;width:150px;" data-aos="zoom-in-up">
              </a>
            </div>
            <img src="<?php echo base_url(IMG . 'Group 3280.png') ?>" class="img-fluid" id="judge-2" />

          </div>
          <div class="col-4 col-md-4">
            <!-- <img src="<?php echo base_url(IMGS . 'tobe-desk.png') ?>" class="img-fluid buble3" /> -->
            <img src="<?php echo base_url(IMG3 . 'Group 3289.png') ?>" class="img-fluid buble3" />
            <div style="position: relative;" class="buble3">
              <a href="javascript:void(0)" data-toggle="modal" data-target="#omesh-profile">
                <img src="<?php echo base_url(IMAGES . 'see-2.png') ?>" class="img-fluid" style="position:absolute;bottom:4rem;left:5rem;" data-aos="zoom-in-down">
              </a>
            </div>
            <img src="<?php echo base_url(IMG3 . 'omesh.png') ?>" class="img-fluid" id="judge-3" />
            <!-- <img src="<?php echo base_url(IMGS . 'blank.png') ?>" class="img-fluid" id="judge-3" style="width: 277px !important;" /> -->

          </div>
        </div>
      </div>

      <div class="col-md-12 mb-5 py-md-5 py-4  d-md-none d-block ">
        <div class="row">
          <div class="col-4 col-md-4 p-1 pt-5" style="position: relative;">
            <img src="<?php echo base_url(IMG . 'Group 3282.png') ?>" class="img-fluid" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-in" />
          </div>
          <div class=" col-8 col-md-8 p-1 ">
            <a href=" javascript:void(0)" data-toggle="modal" data-target="#mandy-profile"><img src="<?php echo base_url(IMG3 . 'buble-1-mob.png') ?>" class="img-fluid" data-aos="zoom-up" /></a>
          </div>

          <div class="col-8 col-md-8 p-1 pt-5">
            <a href="javascript:void(0)" data-toggle="modal" data-target="#muklay-profile"><img src="<?php echo base_url(IMG3 . 'buble-2-mob.png') ?>" class="img-fluid" data-aos="zoom-in" /></a>
          </div>
          <div class="col-4 col-md-4 p-1 " style="position: relative;">
            <img src="<?php echo base_url(IMG . 'Group 3280.png') ?>" class="img-fluid" style="position: absolute; top:7rem; left:0rem;" data-aos="zoom-in" />
          </div>

          <div class=" col-4 col-md-4 p-1 pt-5" style="position: relative;">
            <!-- <img src="<?php echo base_url(IMG . 'Group 3281.png') ?>" class="img-fluid" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-in" /> -->
            <!-- <img src="<?php echo base_url(IMGS . 'blank.png') ?>" class="img-fluid" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-in" /> -->
            <img src="<?php echo base_url(IMG3 . 'omesh.png') ?>" class="img-fluid" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-in" />
          </div>
          <div class=" col-8 col-md-8 p-4 ">
            <a href=" javascript:void(0)" data-toggle="modal" data-target="#omesh-profile">
              <!-- <img src="<?php echo base_url(IMAGES . 'buble-3-mob.png') ?>" class="img-fluid" data-aos="zoom-in"/> -->
              <!-- <img src="<?php echo base_url(IMGS . 'tobe-mob.png') ?>" class="img-fluid" data-aos="zoom-in"/> -->
              <img src="<?php echo base_url(IMG3 . 'Group 3333.png') ?>" class="img-fluid" data-aos="zoom-in" />
            </a>
          </div>
        </div>

      </div>


    </div>
  </div>
</div>

<!-- bg-blue -->
<div class="mt-5" style="position:relative">
  <div style="position:absolute;bottom:-1px;z-index:999">
    <img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
  </div>
</div>
<!-- TERMS AND CONDITIONS -->
<div class="container-fluid participant-cat" style="padding-bottom:0px !important" id="toc">
  <div class="container">
    <div class="row text-center">

      <div class="col-md-12 d-md-block d-none">
        <img src="<?php echo base_url(IMGS . 'toc.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
      </div>

      <div class="container d-md-none d-block mb-3 mt-3">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <img src="<?php echo base_url(IMGS . 'toc.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
          <div class="col-1"></div>
        </div>
      </div>

      <div class="col-md-12 mt-5 d-md-block d-none" style="z-index: 1;">
        <img src="<?php echo base_url(IMGS . 'step.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
      </div>

      <div class="container d-md-none d-block mb-5 mt-3" style="z-index: 1;">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <img src="<?php echo base_url(IMGS . 'step-mob.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
          </div>
          <div class="col-1"></div>
        </div>
      </div>


    </div>
  </div>

  <div class="d-none d-md-block" style="position:relative">
    <div class="bg-bottom-2">
      <img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
    </div>
  </div>
  <div class="d-block d-md-none" style="position:relative">
    <div class="bg-bottom-mobile-toc">
      <img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
    </div>
  </div>
</div>



<!-- YOUR IDEA -->
<div class="container-fluid judge-info your-canvas-your-idea py-md-5 py-0 px-0" id="youridea">
  <div class="d-none d-md-block" style="position:relative">
    <div class="bg-bottom-can-top">
      <!-- <img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>"  style="width:100%"> -->
    </div>
  </div>
  <div class="d-block d-md-none" style="position:relative">
    <div class="bg-bottom-mobile-can-top">
      <!-- <img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>"  style="width:100%"> -->
    </div>
  </div>
  <div style="position:relative">
    <div style="position:absolute;top:20px;right:0px" class="d-none d-md-block">
      <img src="<?php echo base_url(IMG . 'green brush.png') ?>" class="img-fluid" style="width:120px">
    </div>
  </div>
  <div class="container">
    <div class="row text-center">

      <div class="col-md-12 d-md-block d-none mb-md-5 mb-3">
        <img src="<?php echo base_url(IMG . 'your-canvas-your-idea.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
      </div>

      <div class="container d-md-none d-block mb-3">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <img src="<?php echo base_url(IMG . 'your-canvas-your-idea.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
          <div class="col-1"></div>
        </div>
      </div>

      <div class="container d-md-none d-block mb-5">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10">
            <img src="<?php echo base_url(IMGS . 'your-mob.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
          </div>
          <div class="col-1"></div>
        </div>
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10 mt-3">
            <a href=" <?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img id="img-youridea-2" src="<?php echo base_url(IMG . 'Join now.png') ?>" onmouseover="hovdg(this);" onmouseout="unhovdg(this);" class="img-fluid" style="width:200px" data-aos="zoom-in-up" /></a>
          </div>
          <div class="col-1"></div>
        </div>
      </div>


      <div class="col-md-7">
        <div class="row">
          <div class="col-md-12 d-none d-md-block mb-4">
            <img id=" img-youridea-1" src="<?php echo base_url(IMGS . 'your.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 d-none d-md-block  mb-3">
            <a href=" <?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img id="img-youridea-2" src="<?php echo base_url(IMG . 'Join now.png') ?>" onmouseover="hovdg(this);" onmouseout="unhovdg(this);" class="img-fluid" style="width:350px" data-aos="zoom-in-down" /></a>
          </div>
        </div>
      </div>

      <div class="col-md-5">
        <img src="<?php echo base_url(IMG . 'pruduct.png') ?>" class="img-fluid" style="width:370px" data-aos="zoom-out-up" />
      </div>

    </div>
  </div>

  <div style="position:relative">
    <div style="position:absolute;bottom:-250px;right:0px" class="d-none d-md-block">
      <img src="<?php echo base_url(IMG . 'purple brush.png') ?>" class="img-fluid" style="width:230px">
    </div>

    <div style="position:absolute;bottom:-80px;left:0px" class="d-none d-md-block">
      <img src="<?php echo base_url(IMG . 'red brush.png') ?>" class="img-fluid" style="width:230px">
    </div>
  </div>
  <div class="d-none d-md-block" style="position:relative">
    <div class="bg-bottom-can">
      <!-- <img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>"  style="width:100%"> -->
    </div>
  </div>
  <div class="d-block d-md-none" style="position:relative">
    <div class="bg-bottom-mobile-can">
      <!-- <img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>"  style="width:100%"> -->
    </div>
  </div>
</div>


<!-- COMPETITION CRITERIA -->
<div class="container-fluid competition-criteria pb-5">
  <div class="container">
    <div class="row text-center">

      <div class="col-md-6 d-none d-md-block">
        <div class="row">
          <div class="col-md-12  mb-4" style="margin-top:18rem;" ">
            <img id=" img-criteria-1" src="<?php echo base_url(IMG3 . 'makechange.png') ?>" class="img-fluid" style="width:350px" data-aos="zoom-in-up" />
        </div>
      </div>

    </div>

    <div class="col-md-6" style="margin-top:10rem;">
      <img id="img-criteria-4" src="<?php echo base_url(IMG . 'comp-criteria-text.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
    </div>

    <div class="col-md-6 d-block d-md-none">
      <div class="row">
        <div class="col-8 offset-2 col-md-12 offset-md-0 mt-3 mb-4" ">
            <img src=" <?php echo base_url(IMG3 . 'makechange.png') ?>" class="img-fluid" style="width:350px" data-aos="zoom-in-up" />
      </div>
    </div>

  </div>



</div>
</div>
</div>


<!-- MAKE CHANGE -->
<div class="container-fluid make-change pb-5" data-aos="zoom-in-up">
  <div class="container">
    <div class="row text-center">

      <div class="col-md-8 mb-3 text-left">
        <img src="<?php echo base_url(IMG . 'makechange-text.png') ?>" class="img-fluid" />
      </div>
      <div class="col-md-4">
      </div>

      <div class="col-6 offset-3 col-md-8 offset-md-2">
        <a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_blank"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" class="img-fluid" style="width:300px" /></a>
      </div>

    </div>
  </div>
</div>


<!-- FOOTER -->
<div class="container-fluid footer py-2" style="margin-top: 0rem !important;">
  <div class="container">
    <div class="row">

      <div class="col-4 col-md-6 text-left">
        <img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
      </div>

      <div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
        <img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
      </div>

    </div>
  </div>
</div>



<!-- Muklay Profile Modal -->
<div class="modal fade" id="muklay-profile" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center mb-5">
        <img src="<?php echo base_url(IMG . 'muklay-profile-new.png') ?>" class="img-fluid" />
      </div>
    </div>
  </div>
</div>

<!-- Mandy Profile Modal -->
<div class="modal fade" id="mandy-profile" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center mb-5">
        <img src="<?php echo base_url(IMG . 'mandy-profile-new.png') ?>" class="img-fluid" />
      </div>
    </div>
  </div>
</div>

<!-- Gofar Profile Modal -->
<div class="modal fade" id="omesh-profile" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center mb-5">
        <img src="<?php echo base_url(IMG3 . 'Group 3265.png') ?>" class="img-fluid" />
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#img-youridea-2').click(function() {
    $.ajax({
      type: "POST",
      dataType: "html",
      url: '<?php echo site_url('backend/dashboard/countDownload') ?>',
      data: {
        file_type: +'1'
      },
      beforeSend: function() {},
      success: function(msg) {}
    });
  });

  $('#img-criteria-3').click(function() {
    $.ajax({
      type: "POST",
      dataType: "html",
      url: '<?php echo site_url('backend/dashboard/countDownload') ?>',
      data: {
        file_type: +'2'
      },
      beforeSend: function() {},
      success: function(msg) {}
    });
  });

  $(document).ready(function() {
    $(document).on("scroll", onScroll);


    $('a[href*="#"]')
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {

        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
          location.hostname == this.hostname
        ) {

          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

          if (target.length) {
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {

              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) {

                return false;
              } else {
                $target.attr('tabindex', '-1');
                $target.focus();
              };
            });
          }
        }
      });
  });

  function onScroll(event) {
    var scrollPos = $(document).scrollTop();
    $('#hov-menu').each(function() {
      var currLink = $(this);
      var refElement = $(currLink.attr("href"));
      if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
        $('#hov-menu list-group list-group-item a').removeClass("active");
        currLink.addClass("active");
      } else {
        currLink.removeClass("active");
      }
    });
  }
</script>