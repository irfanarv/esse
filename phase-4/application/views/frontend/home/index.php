<?php
$this->data['isuser'] = $this->session->userdata('id');
?>
<style>
	.modal-dialog {
		min-height: calc(100vh - 60px);
		display: flex;
		flex-direction: column;
		justify-content: center;
	}

	@media(max-width: 768px) {
		.modal-dialog {
			min-height: calc(100vh - 20px);
		}
	}

	div#countdown {
		margin-top: -1rem;
	}

	li {
		display: inline-block;
		font-family: Proud Regular;
		font-size: 1em;
		list-style-type: none;
		padding: 1em;
		text-transform: uppercase;
		color: #0062EB;
	}

	li span {
		display: block;
		font-size: 4.5rem;
	}

	.ig-btn:hover {
		opacity: 0.5;
	}


	@media all and (max-width: 768px) {

		li {
			font-size: 1.125rem;
			padding: .75rem;
		}

		li span {
			font-size: 3.375rem;
		}
	}
</style>

<script>
	// in home
	$(document).ready(function() {
		$('.slider').slick({
			dots: true,
			autoplay: true,
			autoplaySpeed: 1000,
			infinite: true,
			speed: 500
		});
	});

	function hov_gall(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery-hov.png') ?>');
	}

	function unhov_gall(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'gallery.png') ?>');
	}


	function ig_hov(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'Instagram.png') ?>');
	}

	function ig_unhov(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'Instagram-1.png') ?>');
	}

	function hovig(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'ig-2.png') ?>');
	}

	function unhovig(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'ig.png') ?>');
	}

	function hoversignvir(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew2.svg') ?>');
	}

	function unhoversignvir(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'signnew.svg') ?>');
	}

	function hover_about_home(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-2.png') ?>');
	}

	function unhover_about_home(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about.png') ?>');
	}


	function hover_joinreg(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'vote-2.png') ?>');
	}

	function unhover_joinreg(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'votenow.png') ?>');
	}

	function hover_joinprize(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'prize-hov.png') ?>');
	}

	function unhover_joinprize(element) {
		element.setAttribute('src', '<?php echo base_url(IMG3 . 'prize.png') ?>');
	}
	// end in home

	// brand
	function hover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'brand-hov.png') ?>');
	}

	function unhover_brand(element) {
		element.setAttribute('src', '<?php echo base_url(IMG . 'BRAND STORY.png') ?>');
	}
	// competition
	function hover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'com-hov.png') ?>');
		$('.hov').show();
		$('.hov-1').hide();
	}
	$(document).ready(function() {
		$('.hov').mouseleave(function() {
			$('.hov').hide();
		});
	});

	function unhover_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMAGES . 'COMPETITION.png') ?>');
	}
	// join
	function hover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-blue.png') ?>');
		$('.hov-1').show();
		$('.hov').hide();
	}

	$(document).ready(function() {
		$('.hov-1').mouseleave(function() {
			$('.hov-1').hide();
		});
	});

	function unhover_join(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'join-menu.png') ?>');
	}

	// about competition

	function hover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>');
	}

	function unhover_about_com(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'about-competition.svg') ?>');
	}

	// upload

	function hover_upload(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'upload-blue.svg') ?>');
	}

	function unhover_upload(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'UPLOAD.svg') ?>');
	}


	// login

	function hover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN-1.svg') ?>');
	}

	function unhover_login_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'LOGIN-1.svg') ?>');
	}

	// register

	function hover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>');
	}

	function unhover_regis_new(element) {
		element.setAttribute('src', '<?php echo base_url(IMGS . 'REGISTER-blue.svg') ?>');
	}

	// end hover menu

	$(document).ready(function() {
		AOS.init();
	});

	$(document).ready(function() {
		$('#myModal').modal({
			backdrop: 'static',
			keyboard: false
		})


		$('#myModal').modal('show');

		month = $("#dob_month").val();
		year = $("#dob_year").val();
		day = $("#dob_day").val();

		if (month != '' && year != '' && day != '')
			$("#submit_bt").removeAttr("disabled");
		else
			$("#submit_bt").attr("disabled", "disabled");
		$('[data-fancybox]').fancybox({
			buttons: [
				"zoom",
				"thumbs",
				"close"
			],
			hideScrollbar: false,
		});



		$("#dob_month").on("change", function() {
			v = $(this).val();
			year = $("#dob_year").val();
			day = $("#dob_day").val();
			if (v != '' && year != '' && day != '') {
				validateAge();
			}
		});


		$("#dob_year").on("change", function(event) {
			v = $(this).val();
			month = $("#dob_month").val();
			day = $("#dob_day").val();
			if (v != '' && month != '' && day != '') {
				validateAge();
			}
		});

		$("#dob_day").on("change", function(event) {
			v = $(this).val();
			year = $("#dob_year").val();
			month = $("#dob_month").val();
			if (v != '' && month != '' && year != '') {
				validateAge();
			}
		});

		function validateAge() {

			var month = parseInt($("#dob_month").val());
			var year = parseInt($("#dob_year").val());
			var age = 18;
			var setDate = new Date(year + age, month - 1, 1);
			var currdate = new Date();

			if (currdate >= setDate) {
				$('#myModal').modal('hide');
				$('#vtc').modal('show');
				setCookie();
				player.playVideo();


			} else {
				$('#validate-age').hide();
				$('#sorry-message').show();
			}
		};


	});
</script>

<script>
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


	var player;

	function onYouTubeIframeAPIReady() {
		player = new YT.Player('player', {
			playerVars: {
				rel: 0,
				autoplay: 1,
				enablejsapi: 1,
				disablekb: 1,
				showinfo: 0,
				controls: 0,
				fs: 0,

			},
			height: '100%',
			width: '100%',
			videoId: 'd4OdM18iUCU',
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		});
	}

	function onPlayerReady(event) {
		event.target.playVideo();
	}
	var done = false;

	function onPlayerStateChange(event) {
		if (event.data === YT.PlayerState.ENDED) {
			stopVideo();
		}
	}

	function stopVideo() {
		player.stopVideo();
		$('#vtc').modal('hide');
	}





	function close_window() {
		window.open('', '_parent', '');
		window.close();
	}

	function setCookie() {
		url = "<?php echo site_url('Home/SetCookie') ?>";
		$.ajax({
			url: url,
			type: "POST",
			dataType: "JSON",
			success: function(data) {
				console.log();
			},

		});
	}
</script>

<!-- menu mobile -->
<script>
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>

<script>
	$(document).ready(function() {
		$("#judge-info").mousemove(function(e) {
			parallaxJudge(e, "#judge-1", -100);
			parallaxJudge(e, "#judge-2", 100);
			parallaxJudge(e, "#judge-3", -100);
			parallaxJudge(e, "#judge-4", 100);
		});

		function parallaxJudge(e, target, movement) {
			var $this = $("#judge-info");
			var relX = e.pageX - $this.offset().left;
			var relY = e.pageY - $this.offset().top;

			TweenMax.to(target, 1, {
				x: (relX - $this.width() / 2) / $this.width() * movement,
				y: (relY - $this.height() / 2) / $this.height() * movement
			});
		}
		$('#judge-1').mouseover(function() {
			$('.buble').show();
		});

		$('.buble').mouseleave(function() {
			$('.buble').hide();
		});

		$('#judge-2').mouseover(function() {
			$('.buble2').show();
		});

		$('.buble2').mouseleave(function() {
			$('.buble2').hide();
		});

		$('#judge-3').mouseover(function() {
			$('.buble3').show();
		});

		$('.buble').mouseleave(function() {
			$('.buble3').hide();
		});
	});
	// join
	function join() {
		$("#join").hide();
		$("#joincolor").show();
		$("#btnlogmenu").show();
		$("#btnregmenu").show();
	}

	function joincolor() {
		$("#join").show();
		$("#joincolor").hide();
		$("#btnlogmenu").hide();
		$("#btnregmenu").hide();
	}
	// end join
	// competition
	function com() {
		$("#com").hide();
		$("#commob").show();
		$("#ul").show();
		$("#aboutsub").show();
		$("#aboutpr").show();
		$("#aboutvw").show();
		$("#aboutjr").show();
		$("#aboutdg").show();
		$("#abouttem").show();
		$("#abouttoc").show();
		$("#aboutuplo").show();
	}

	function comcolor() {
		$("#commob").hide();
		$("#ul").hide();
		$("#aboutsub").hide();
		$("#aboutpr").hide();
		$("#aboutvw").hide();
		$("#aboutjr").hide();
		$("#aboutdg").hide();
		$("#abouttem").hide();
		$("#abouttoc").hide();
		$("#aboutuplo").hide();
		$("#com").show();
	}
</script>
<!-- CONFIRM AGE -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-body text-center p-3 pt-4 pb-5">

				<div class="row" id="validate-age">
					<div class="col-md-12 mb-4 mt-3">
						<img src="<?= base_url(); ?>assets/images/welcome.png" class="img-welcome" /> <br>
						<img src="<?= base_url(); ?>assets/images/text-age.png" class="img-fluid mt-3">
					</div>

					<div class="col-12 col-md-12 mb-2 text-center">
						<select name="dob_day" id="dob_day" required>
							<option value="">Date</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>
						</select>
						<select name="dob_month" id="dob_month" required>
							<option value="">Month</option>
							<option value="1">January</option>
							<option value="2">February</option>
							<option value="3">March</option>
							<option value="4">April</option>
							<option value="5">May</option>
							<option value="6">June</option>
							<option value="7">July</option>
							<option value="8">August</option>
							<option value="9">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>

						<select name="dob_year" id="dob_year" required>
							<option value="">Year</option>
							<option value="2015">2015</option>
							<option value="2014">2014</option>
							<option value="2013">2013</option>
							<option value="2012">2012</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>
							<option value="2007">2007</option>
							<option value="2006">2006</option>
							<option value="2005">2005</option>
							<option value="2004">2004</option>
							<option value="2003">2003</option>
							<option value="2002">2002</option>
							<option value="2001">2001</option>
							<option value="2000">2000</option>
							<option value="1999">1999</option>
							<option value="1998">1998</option>
							<option value="1997">1997</option>
							<option value="1996">1996</option>
							<option value="1995">1995</option>
							<option value="1994">1994</option>
							<option value="1993">1993</option>
							<option value="1992">1992</option>
							<option value="1991">1991</option>
							<option value="1990">1990</option>
							<option value="1989">1989</option>
							<option value="1988">1988</option>
							<option value="1987">1987</option>
							<option value="1986">1986</option>
							<option value="1985">1985</option>
							<option value="1984">1984</option>
							<option value="1983">1983</option>
							<option value="1982">1982</option>
							<option value="1981">1981</option>
							<option value="1980">1980</option>
							<option value="1979">1979</option>
							<option value="1978">1978</option>
							<option value="1977">1977</option>
							<option value="1976">1976</option>
							<option value="1975">1975</option>
							<option value="1974">1974</option>
							<option value="1973">1973</option>
							<option value="1972">1972</option>
							<option value="1971">1971</option>
							<option value="1970">1970</option>
							<option value="1969">1969</option>
							<option value="1968">1968</option>
							<option value="1967">1967</option>
							<option value="1966">1966</option>
							<option value="1965">1965</option>
							<option value="1964">1964</option>
							<option value="1963">1963</option>
							<option value="1962">1962</option>
							<option value="1961">1961</option>
							<option value="1960">1960</option>
							<option value="1959">1959</option>
							<option value="1958">1958</option>
							<option value="1957">1957</option>
							<option value="1956">1956</option>
							<option value="1955">1955</option>
							<option value="1954">1954</option>
							<option value="1953">1953</option>
							<option value="1952">1952</option>
							<option value="1951">1951</option>
							<option value="1950">1950</option>
							<option value="1949">1949</option>
							<option value="1948">1948</option>
							<option value="1947">1947</option>
							<option value="1946">1946</option>
							<option value="1945">1945</option>
							<option value="1944">1944</option>
							<option value="1943">1943</option>
							<option value="1942">1942</option>
							<option value="1941">1941</option>
							<option value="1940">1940</option>
						</select>
					</div>

				</div>

				<div class="row" id="sorry-message" style="display:none">
					<div class="col-md-12">
						<img src="<?php echo base_url(IMGS . 'oops.png') ?>" class="img-fluid" style="width: 300px;" />
					</div>
					<div class="col-md-12 mt-4">
						<a type="button" href="#" onclick="close_window();return false;" class="btn btn-primary">Close</a>
					</div>
				</div>


			</div>
		</div>
	</div>
</div>
<!-- MODAL VTC -->
<div class="modal  fade" id="vtc" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content modal-vtc">

			<div class="modal-body vtc text-center ">

				<div class="embed-responsive embed-responsive-16by9">
					<div id="player"></div>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- MOBILE MENU -->
<div class="modal modal-right fade" id="right_modal_sm" tabindex="-1" role="dialog" aria-labelledby="right_modal_sm">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="justify-content-start text-left">
					<img src="<?php echo base_url(IMAGES . 'logo.png') ?>" class="img-fluid side-logo" />
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-right pt-5">
				<img src="<?php echo base_url(IMAGES . 'HOME.png') ?>" class="img-fluid px-3 mbm" style="height:30px" />
				<br><a href="<?php echo base_url('vote-now') ?>"><img src="<?php echo base_url(IMG3 . 'gallery.png') ?>" onmouseover="hov_gall(this);" onmouseout="unhov_gall(this);" class="img-fluid px-2 mbm" style="height:29px" /></a><br>
				<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a>
				<!-- competition -->
				<a id="com" href="javascript:void(0);" onclick="com()"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" class="img-fluid px-2 mbm" style="height:18px" /></a> </br>
				<a id="commob" style="display: none;" href="javascript:void(0);" onclick="comcolor()"><img src="<?php echo base_url(IMGS . 'commob.png') ?>" class="img-fluid px-2" style="height:30px" /></a>
				<!-- sub menu -->
				<ul id="ul" style="display: none; margin-bottom:0px;">
					<li class="list-group-item"><a id="aboutsub" style="display: none;" href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:18px" /></a></li>
					<li class="list-group-item"><a id="aboutpr" style="display: none;" href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutjr" style="display: none;" href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>
					<li class="list-group-item"><a id="aboutdg" style="display: none;" href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>

					<li class="list-group-item"><a id="abouttoc" style="display: none;" href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid " style="height:16px" /></a></li>

				</ul>

				<!-- sub menu -->
				<!-- competition -->

			</div>

		</div>
	</div>
</div>
<!-- HEADER -->

<div style="position:relative;">
	<div class="d-block d-md-none container-fluid burger-home" style="z-index: 1000;">
		<div class="container">
			<div class="row text-center">
				<div class="col-12 text-right d-block d-sm-none">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="position:relative;">
	<div class="d-block d-md-none container-fluid burger-home" style="z-index: 1000;">
		<div class="container">
			<div class="row text-center">
				<div class="col-12 text-right d-block d-sm-none">
					<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- home -->
<div class="home banner-home-new" style="margin-bottom: 0px;">
	<div class="d-none d-md-block">
		<img src="<?php echo base_url(IMG3 . 'home-new.gif') ?>" class="img-fluid banner-home wow tada animated" style="z-index: -1 !important;" id="banner" />
		<div style="position:relative">


			<div class="logo-head">
				<img src="<?php echo base_url(IMGS . 'logohead.png') ?>" class="img-fluid" />
			</div>

			<div class="container-fluid head-new pt-4 px-0 " id="header">
				<div class="container">
					<div class="row text-center">
						<div class="col-12 text-right d-block d-sm-none" style="margin-bottom:50px">
							<button style="background:transparent" type="button" class="btn" data-toggle="modal" data-target="#right_modal_sm"><img src="<?php echo base_url(IMG . 'Burger.png') ?>" class="img-fluid" /></button>
						</div>

						<div class="col-md-9 offset-md-3 mb-5 text-right d-none d-md-block">
							<img src="<?php echo base_url(IMAGES . 'HOME.png') ?>" class="img-fluid px-3" style="height:30px" />
							<a href="<?php echo base_url('vote-now') ?>"><img src="<?php echo base_url(IMG3 . 'gallery.png') ?>" onmouseover="hov_gall(this);" onmouseout="unhov_gall(this);" class="img-fluid pr-2" style="height:29px" /></a>
							<a href="<?php echo base_url('brandstory') ?>"><img src="<?php echo base_url(IMG . 'BRAND STORY.png') ?>" onmouseover="hover_brand(this);" onmouseout="unhover_brand(this);" class="img-fluid px-2" style="height:18px" /></a>
							<a href="#"><img src="<?php echo base_url(IMAGES . 'COMPETITION.png') ?>" onmouseover="hover_com(this);" onmouseout="unhover_com(this);" class="img-fluid px-2" style="height:18px" /></a>
						</div>


						<div class="hov" style="position:relative;display: none;">
							<div class="hov-menu d-none d-md-block" style="z-index: 1 !important;">
								<ul class="list-group text-right  d-flex justify-content-end">
									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#about"><img src="<?php echo base_url(IMGS . 'about-competition-blue.svg') ?>" class="img-fluid " style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#prize"><img src="<?php echo base_url(IMGS . 'prize.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#judge-info"><img src="<?php echo base_url(IMGS . 'juri-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>
									<li class="list-group-item"><a href="<?php echo base_url(DOCS . 'Esse-Change-Guide-Competition.zip') ?>"><img src="<?php echo base_url(IMGS . 'dg-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>

									<li class="list-group-item"><a href="<?php echo base_url('about-competition') ?>#toc"><img src="<?php echo base_url(IMGS . 'toc-menu.png') ?>" class="img-fluid pr-0" style="height:15px" /></a></li>

								</ul>
							</div>
						</div>



					</div>
				</div>
			</div>

			<div class="join-new d-none d-md-block">
				<a href="<?php echo base_url('vote-now') ?>">
					<img src="<?php echo base_url(IMG3 . 'votenow.png') ?>" onmouseover="hover_joinreg(this);" onmouseout="unhover_joinreg(this);" class="img-fluid wow flash animated" id="btn-reg" /></a>
			</div>
			<div class="prize-new d-none d-md-block">
				<a href="<?php echo base_url('about-competition') ?>#prize">
					<img src="<?php echo base_url(IMG3 . 'prize.png') ?>" onmouseover="hover_joinprize(this);" onmouseout="unhover_joinprize(this);" class="wow flash animated" id="btn-prize" /></a>
			</div>
		</div>
	</div>


	<div class="d-block d-md-none" style="z-index: -1; width:100%">
		<div style="position:relative">
			<div class="logo-head" style="z-index: 1;">
				<img src="<?php echo base_url(IMGS . 'logohead.png') ?>" class="img-fluid" width="108px;" />
			</div>
		</div>
		<img src="<?php echo base_url(IMG3 . 'home-mob.gif') ?>" class="ban-mobile wow tada animated" />
	</div>
	<div style="position:relative"> 
		<div class="join-mob d-block d-md-none">
			<a href="<?php echo base_url('vote-now') ?>"> <img src="<?php echo base_url(IMG3 . 'votenow.png') ?>" class="img-fluid reg wow swing animated" /></a>
		</div>
	</div>
	<div style="position:relative"> 
		<div class="prize-home-mob d-block d-md-none">
			<a href="<?php echo base_url('about-competition') ?>#prize"> <img src="<?php echo base_url(IMG3 . 'prize.png') ?>" style="width:210px !important" onmouseover="hover_joinprize(this);" onmouseout="unhover_joinprize(this); class="img-fluid reg wow swing animated" /></a>
		</div>
	</div>

	<!-- mobile -->
</div>

<!-- bg-blue -->
<!-- <div style="position:relative">
	<div class="bg-bluehome">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
</div> -->
<!-- about -->
<div class="container-fluid  bg-home-about pb-5">
	<div class="container d-none d-md-block">
		<div class="row  text-center">
			<div class="col-md-6  isi-home">
				<img src="<?php echo base_url(IMGS . 'about-white-2.png') ?>" class="img-fluid mb-3" style="width: 500px;" data-aos="zoom-up" />
				<a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_new">
					<img src="<?php echo base_url(IMGS . 'ignew.png') ?>" onmouseover="hovig(this);" onmouseout="unhovig(this);" class="img-fluid ig-home" data-aos="zoom-in-up" />
				</a>
			</div>
			<div class="col-md-6 isi-home-card">
				<div class="card" style="margin-top:0px !important; margin-bottom:80px; left:0px !important;" data-aos="zoom-in">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container d-block d-md-none">
		<div class="row text-center">
			<div class="col-md-12 mb-3 pl-3 pr-3">
				<div class="slider" data-aos="zoom-in">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'double.jpg') ?>" alt="Double">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'applemint.jpg') ?>" alt="Apple Mint">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'grape.jpg') ?>" alt="Grape">
					<img class="img-fluid" src="<?php echo base_url(IMAGES . 'juicy.jpg') ?>" alt="Juice">
				</div>
			</div>
		</div>
	</div>
	<div class="d-none d-md-block" style="position:absolute;z-index:-1;">
	<img src="<?php echo base_url(IMAGES . 'bg-judge.png') ?>" class="img-fluid" style="width: 2560px !important;" />
	</div>
</div>

<div class="container-fluid">
	<div class="container d-block d-md-none">
		<div class="row text-center">
			<div class="col-md-12 pl-4 pr-4">
				<img class="img-fluid" src="<?php echo base_url(IMGS . 'artikel-home-1.png') ?>" data-aos="zoom-in-up">
			</div>
			<div class="col-md-12">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself" ?>" target="_new">
					<img src="<?php echo base_url(IMGS . 'ig-mob-white-1.png') ?>" class="img-fluid ig-home" data-aos="zoom-in-up" />
				</a>
			</div>
		</div>
	</div>
</div>

<div class="d-md-none d-block" style="position:relative;">
	<div style="position:absolute;bottom:-75px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>
</div>

<div class="container-fluid d-block d-md-none bg-home-about pb-5 mt-5">

	<div class="container ">
		<div class="row text-center">
			<div class="col-md-12 mb-3 pl-3 pr-3">
				<div class="card" style="margin-top:50px !important; margin-bottom:50px; left:0px !important;" data-aos="zoom-in">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- JUDGE INFO -->
<div class="container-fluid bg-judge-mobile" id="judge-info">
	<div class="container">
		<div class="row text-center">

			<div class="col-md-12 mb-md-5 mb-0">
				<img src="<?php echo base_url(IMG3 . 'judge-info.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
			</div>

			<div class="col-md-12 mb-5 py-5 d-none d-md-block">
				<div class="row">
					<div class="col-4 col-md-4">
						<div style="position: relative;">
						</div>
						<img src="<?php echo base_url(IMAGES . 'buble-1.png') ?>" class="img-fluid buble" />
						<div style="position: relative;" class="buble">
							<a href="javascript:void(0)" data-toggle="modal" data-target="#mandy-profile">
								<img src="<?php echo base_url(IMAGES . 'see-profile.png') ?>" class="img-fluid" style="position:absolute;bottom:6rem;left:7rem;" data-aos="zoom-in-up">
							</a>
						</div>
						<img src="<?php echo base_url(IMG . 'Group 3282.png') ?>" class="img-fluid " id="judge-1" />
					</div>
					<div class=" col-4 col-md-4">
						<img src="<?php echo base_url(IMAGES . 'buble-2.png') ?>" class="img-fluid buble2" />
						<div style="position: relative;" class="buble2">
							<a href="javascript:void(0)" data-toggle="modal" data-target="#muklay-profile">
								<img src="<?php echo base_url(IMAGES . 'see-2.png') ?>" class="img-fluid" style="position:absolute;bottom:5.5rem;left:7rem;width:150px;" data-aos="zoom-in-up">
							</a>
						</div>
						<img src="<?php echo base_url(IMG . 'Group 3280.png') ?>" class="img-fluid" id="judge-2" />

					</div>
					<div class="col-4 col-md-4">
						<!-- <img src="<?php echo base_url(IMGS . 'tobe-desk.png') ?>" class="img-fluid buble3" /> -->
						<img src="<?php echo base_url(IMG3 . 'Group 3289.png') ?>" class="img-fluid buble3" />
						<div style="position: relative;" class="buble3">
							<a href="javascript:void(0)" data-toggle="modal" data-target="#omesh-profile">
								<img src="<?php echo base_url(IMAGES . 'see-2.png') ?>" class="img-fluid" style="position:absolute;bottom:4rem;left:5rem;" data-aos="zoom-in-down">
							</a>
						</div>
						<img src="<?php echo base_url(IMG3 . 'omesh.png') ?>" class="img-fluid" id="judge-3" />
						<!-- <img src="<?php echo base_url(IMGS . 'blank.png') ?>" class="img-fluid" id="judge-3" style="width: 277px !important;" /> -->

					</div>
				</div>
			</div>

			<div class="col-md-12 mb-5 py-md-5 py-4  d-md-none d-block ">
				<div class="row">
					<div class="col-4 col-md-4 p-1 pt-5" style="position: relative;">
						<img src="<?php echo base_url(IMG . 'Group 3282.png') ?>" class="img-fluid" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-in" />
					</div>
					<div class=" col-8 col-md-8 p-1 ">
						<a href=" javascript:void(0)" data-toggle="modal" data-target="#mandy-profile"><img src="<?php echo base_url(IMG3 . 'buble-1-mob.png') ?>" class="img-fluid" data-aos="zoom-up" /></a>
					</div>

					<div class="col-8 col-md-8 p-1 pt-5">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#muklay-profile"><img src="<?php echo base_url(IMG3 . 'buble-2-mob.png') ?>" class="img-fluid" data-aos="zoom-in" /></a>
					</div>
					<div class="col-4 col-md-4 p-1 " style="position: relative;">
						<img src="<?php echo base_url(IMG . 'Group 3280.png') ?>" class="img-fluid" style="position: absolute; top:7rem; left:0rem;" data-aos="zoom-in" />
					</div>

					<div class=" col-4 col-md-4 p-1 pt-5" style="position: relative;">
						<img src="<?php echo base_url(IMG3 . 'omesh.png') ?>" class="img-fluid" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-in" />
						<!-- <img src="<?php echo base_url(IMGS . 'blank.png') ?>" class="img-fluid" style="position: absolute; top:5rem; left:1rem;" data-aos="zoom-in" /> -->
					</div>
					<div class=" col-8 col-md-8 p-4 ">
						<a href=" javascript:void(0)" data-toggle="modal" data-target="#omesh-profile">
							<img src="<?php echo base_url(IMG3 . 'Group 3333.png') ?>" class="img-fluid" data-aos="zoom-in" />
							<!-- <img src="<?php echo base_url(IMGS . 'tobe-mob.png') ?>" class="img-fluid" data-aos="zoom-in"/> -->
						</a>
					</div>
				</div>

			</div>


		</div>
	</div>
</div>



<!-- join -->
<div class="d-none d-md-block" style="position:absolute;z-index:-1;">
<img src="<?php echo base_url(IMG3 . 'bgnew-make.png') ?>" class="img-fluid " />
</div>
<div class="container-fluid bg-make-mobile">
	<div class="row text-center banner">
		<div class="col-md-12 mb-3 d-none d-md-block">
			<img src="<?php echo base_url(IMGS . 'makechange.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
		</div>

		<div class="container d-md-none d-block">
			<div class="row">
				<div class="col-1"></div>
				<div class="col-10  mb-5">
					<img src="<?php echo base_url(IMGS . 'express the change.png') ?>" class="img-fluid" data-aos="zoom-in-up" />
				</div>
				<div class="col-1"></div>
			</div>
		</div>



		<div class="col-md-4 text-left d-none d-md-block" style="position: relative;">
			<img src="<?php echo base_url(IMG . 'hand.gif') ?>" class="hand-new" style="width:450px " data-aos="zoom-in-up" />
		</div>

		<div class="col-6 offset-3 col-md-4 offset-md-0 mt-0 mt-md-5 d-block d-md-none">
			<img src="<?php echo base_url(IMG3 . 'makechangecom.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
		</div>

		<div class="col-6 offset-3 col-md-4 offset-md-0 mt-0 d-none d-md-block" style="margin-top: 10rem !important;">
			<img src="<?php echo base_url(IMG3 . 'makechangecom.png') ?>" class="img-fluid" data-aos="zoom-in-down" />
		</div>

		<div class="col-6 col-md-4 text-left d-block d-md-none">
			<img src="<?php echo base_url(IMG . 'hand.gif') ?>" class="hand-new" style="width:250px" data-aos="zoom-in-down" />
		</div>

		<div class="col-6 col-md-4 text-right">
			<img id="img-header-3" src="<?php echo base_url(IMG . 'pack.gif') ?>" class="img-fluid" style="width:330px" data-aos="zoom-in-up" />
		</div>

	</div>
</div>
<!-- absolute 3 icon -->
<div style="position:relative">
	<div style="position:absolute;bottom:-160px;right:0px;z-index:1000" class="d-none d-md-block">
		<img src="<?php echo base_url(IMG . 'yellow bruh.png') ?>" class="img-fluid" style="width:300px">
	</div>

	<div style="position:absolute;bottom:-50px;right:0px;z-index:1000" class="d-block d-md-none">
		<img src="<?php echo base_url(IMG . 'yellow bruh.png') ?>" class="img-fluid" style="width:100px">
	</div>

	<div style="position:absolute;bottom:-1px;z-index:999">
		<img src="<?php echo base_url(IMG . 'participant-bg-top.png') ?>" style="width:100%">
	</div>

</div>
<!-- greeting from esse -->
<div class="container-fluid  participant-cat" style="padding-bottom: 5px !important;">
	<div class="container">
		<div class="row text-center">

			<div class="col-md-6">
				<div class="card home-greeting-card" data-aos="zoom-in-up">
					<div class="card-body">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2Zfr6nvyOO0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="row mb-4">
					<img src="<?php echo base_url(IMGS . 'greeting.png') ?>" class="img-fluid mb-3 d-none d-md-block" style="width: 550px;" data-aos="zoom-in-down" />
					<div class="container d-block d-md-none">
						<div class="row">
							<div class="col-1"></div>
							<img src="<?php echo base_url(IMGS . 'greeting.png') ?>" class="img-fluid col-10 mb-3 " style="width: 250px;" data-aos="zoom-in-down" />
							<div class="col-1"></div>
						</div>
					</div>

					<div class="col-md-12" style="z-index:1000">
						<img src="<?php echo base_url(IMGS . 'homegreetmobnew.png') ?>" class="img-fluid mb-3 d-block d-md-none" data-aos="fade-down" />
						<img src="<?php echo base_url(IMGS . 'greathome3.png') ?>" class="img-fluid mb-3 d-none d-md-block" data-aos="fade-down" />
					</div>

					<div class="container d-none d-md-block">
						<div class="row">
							<div class="col-3"></div>
							<div class="col-9">
								<a href="<?php echo base_url('about-competition') ?>">
									<img src="<?php echo base_url(IMGS . 'about.png') ?>" onmouseover="hover_about_home(this);" onmouseout="unhover_about_home(this);" class="img-fluid d-none d-md-block" style="width: 310px;" data-aos="fade-up" />
								</a>
							</div>

						</div>
					</div>

					<div class="container d-block d-md-none">
						<div class="row">
							<div class="col-1"></div>
							<div class="col-10">
								<a href="<?php echo base_url('about-competition') ?>">
									<img src="<?php echo base_url(IMGS . 'about.png') ?>" onmouseover="hover_about_home(this);" onmouseout="unhover_about_home(this);" class="img-fluid d-block d-md-none" data-aos="fade-up" />
								</a>
							</div>
							<div class="col-1"></div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
	<div class="d-none d-md-block" style="position:relative">
		<div class="bg-bottom-home">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
	<div class="d-block d-md-none" style="position:relative">
		<div class="bg-bottom-mobile-new">
			<img src="<?php echo base_url(IMG . 'participant-bg-bottom.png') ?>" style="width:100%">
		</div>
	</div>
</div>

<div class="container-fluid ig-story d-none d-md-block" style="padding-top: 10rem !important;">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid" style="width:350px" /></a>
			</div>

		</div>
	</div>
</div>

<div class="container-fluid ig-story d-block d-md-none" style="padding-top: 5rem !important;">
	<div class="container">
		<div class="row text-center">

			<div class="col-6 offset-3 col-md-8 offset-md-2">
				<a href="<?php echo "https://www.instagram.com/2020loveyourself/" ?>" target="_new"><img src="<?php echo base_url(IMG . 'Instagram-1.png') ?>" onmouseover="ig_hov(this);" onmouseout="ig_unhov(this);" class="img-fluid" /></a>
			</div>

		</div>
	</div>
</div>


<!-- important -->

<!-- FOOTER -->
<div class="container-fluid footer footer-home py-1 d-none d-md-block" style="margin-top: 3rem !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>

<div class="container-fluid footer footer-home py-1 d-block d-md-none" style="margin-top: 1.5rem !important;">
	<div class="container">
		<div class="row">

			<div class="col-4 col-md-6 text-left">
				<img src="<?php echo base_url(IMG . 'esse-logo-footer.png') ?>" class="img-fluid" />
			</div>

			<div class="col-4 offset-4 col-md-6 offset-md-0 text-right">
				<img src="<?php echo base_url(IMG . 'alr-footer.png') ?>" class="img-fluid" />
			</div>

		</div>
	</div>
</div>

<!-- footer -->


<!-- Muklay Profile Modal -->
<div class="modal fade" id="muklay-profile" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center mb-5">
				<img src="<?php echo base_url(IMG . 'muklay-profile-new.png') ?>" class="img-fluid" />
			</div>
		</div>
	</div>
</div>

<!-- Mandy Profile Modal -->
<div class="modal fade" id="mandy-profile" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center mb-5">
				<img src="<?php echo base_url(IMG . 'mandy-profile-new.png') ?>" class="img-fluid" />
			</div>
		</div>
	</div>
</div>

<!-- Gofar Profile Modal -->
<div class="modal fade" id="omesh-profile" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog  modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center mb-5">
				<img src="<?php echo base_url(IMG3 . 'Group 3265.png') ?>" class="img-fluid" />
			</div>
		</div>
	</div>
</div>