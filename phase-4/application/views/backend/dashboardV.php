<?php
error_reporting(0);
foreach ($visitor as $result) {
    $bulan[] = $result->tgl;
    $value[] = (float) $result->jumlah;
}
foreach ($visitBrowser as $result) {
    $browser[] = $result->browser;
    $jml[] = (float) $result->jml;
}

?>
<div class="pcoded-main-container">
    <div class="pcoded-content">
        <div class="page-header">
            <div class="page-block">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="page-header-title">
                            <h5 class="m-b-10">Hello <?php echo $this->session->userdata('nama'); ?> ,
                                <?php
                                date_default_timezone_set('Asia/Jakarta');
                                $Hour = date('G');
                                if ($Hour >= 5 && $Hour <= 11) {
                                    echo "Selamat Pagi";
                                } else if ($Hour >= 12 && $Hour <= 15) {
                                    echo "Selamat Siang";
                                } else if ($Hour >= 16 && $Hour <= 18) {
                                    echo "Selamat Sore";
                                } else if ($Hour >= 19 || $Hour <= 4) {
                                    echo "Selamat Malam";
                                }
                                ?>
                                👋
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?php foreach ($winnerVote as $wv) : ?>
                <div class="col-md-12 col-xl-4">
                    <div class="card widget-statstic-card">
                        <div class="card-body">
                            <div class="card-header-left mb-3">
                                <h5 class="mb-0"><?php echo $wv->name; ?></h5>
                            </div>
                            <i class="feather icon-award st-icon bg-c-yellow"></i>
                            <div class="text-left">
                                <br>
                                <br>
                                <h3 class="d-inline-block"><?php echo $wv->vote_total; ?> Voters</h3>
                                <span class="float-right bg-c-yellow"><?php
                                                                        if ($wv->set_win == 1) {
                                                                            echo "1st";
                                                                        } elseif ($wv->set_win == 2) {
                                                                            echo "2nd";
                                                                        } elseif ($wv->set_win == 3) {
                                                                            echo "3rd";
                                                                        } ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="col-xl-12 col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Voting</h5>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active text-uppercase" id="winner-tab" data-toggle="tab" href="#winner" role="tab" aria-controls="winner" aria-selected="true">Winner</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="false">General</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="student-tab" data-toggle="tab" href="#student" role="tab" aria-controls="student" aria-selected="false">Students</a>
							</li>
						</ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="winner" role="tabpanel" aria-labelledby="winner-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_win" class="table nowrap">
                                        <thead>
                                            <tr>
                                                <th>Winner</th>
                                                
                                                <th>Design By</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>ID Card</th>
                                                <th>Total Votes</th>
                                                


                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="general" role="tabpanel" aria-labelledby="general-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_gen" class="table nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th>Design By</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>ID Card</th>
                                                <th>Total Votes</th>
                                                


                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="student" role="tabpanel" aria-labelledby="student-tab">
                                <div class="dt-responsive table-responsive">
                                    <table id="table_student" class="table nowrap">
                                        <thead>
                                            <tr>
                                                
                                                <th>Design By</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>ID Card</th>
                                                <th>Total Votes</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-xl-3 col-md-6">
                <div class="card ticket-card">
                    <div class="card-body">
                        <p class="m-b-25 badge-light-primary lbl-card"><i class="fas fa-file-download m-r-5"></i>File Guide</p>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-blue"><?php echo number_format($todayGuide); ?></h2>
                            <p class="m-b-0 d-inline-block">Today Download</p>
                        </div>
                        <hr>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-blue"><?php echo number_format($totalGuide); ?></h2>
                            <p class="m-b-0 d-inline-block">Total Download</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card ticket-card">
                    <div class="card-body">
                        <p class="m-b-25 badge-light-success lbl-card"><i class="fas fa-file-download m-r-5"></i> File Template</p>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-green"><?php echo number_format($todayTemplate); ?></h2>
                            <p class="m-b-0 d-inline-block">Today Download</p>
                        </div>
                        <hr>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-green"><?php echo number_format($totalTemplate); ?></h2>
                            <p class="m-b-0 d-inline-block">Total Download</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card ticket-card">
                    <div class="card-body">
                        <p class="m-b-25 badge-light-warning lbl-card"><i class="fas fa-users m-r-5"></i> General Register</p>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-yellow"><?php echo number_format($todayGeneral); ?></h2>
                            <p class="m-b-0 d-inline-block">Today Registered</p>
                        </div>
                        <hr>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-yellow"><?php echo number_format($totalGeneral); ?></h2>
                            <p class="m-b-0 d-inline-block">Total Participants</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="card ticket-card">
                    <div class="card-body">
                        <p class="m-b-25 badge-light-danger lbl-card"><i class="fas fa-users m-r-5"></i> Student Register</p>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-red"><?php echo number_format($todayStudent); ?></h2>
                            <p class="m-b-0 d-inline-block">Today Registered</p>
                        </div>
                        <hr>
                        <div class="text-center">
                            <h2 class="m-b-0 d-inline-block text-c-red"><?php echo number_format($totalStudent); ?></h2>
                            <p class="m-b-0 d-inline-block">Total Participants</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Statistic Of Visitors</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="salesChart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-12">
                <div class="card bg-c-blue text-white widget-visitor-card">
                    <div class="card-body text-center">
                        <h2 class="text-white"><?php echo number_format($monthvisit); ?></h2>
                        <h6 class="text-white">Visitors of the month</h6>
                        <i class="feather icon-file-text"></i>
                    </div>
                </div>
                <div class="card bg-c-yellow text-white widget-visitor-card">
                    <div class="card-body text-center">
                        <h2 class="text-white"><?php echo number_format($lastmonthvisit); ?></h2>
                        <h6 class="text-white">Last month visitor</h6>
                        <i class="feather icon-award"></i>
                    </div>
                </div>
                <div class="card bg-c-green text-white widget-visitor-card">
                    <div class="card-body text-center">
                        <h2 class="text-white"><?php echo number_format($totalvisits); ?></h2>
                        <h6 class="text-white">Total Visitors</h6>
                        <i class="feather icon-trending-up"></i>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h5>Visitors By Browsers</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="oilChart" height="130"></canvas>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js" integrity="sha512-ju6u+4bPX50JQmgU97YOGAXmRMrD9as4LE05PdC3qycsGQmjGlfm041azyB1VfCXpkpt1i9gqXCT6XuxhBJtKg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

<script>
    $(function() {

        var salesChartCanvas = $("#salesChart").get(0).getContext("2d");

        var salesChart = new Chart(salesChartCanvas);

        var salesChartData = {
            labels: <?php echo json_encode($bulan); ?>,

            datasets: [{
                    label: "Date of visits",
                    backgroundColor: "rgba(0,0,0,.05)",
                    fillColor: "rgb(210, 214, 222)",
                    strokeColor: "rgb(210, 214, 222)",
                    pointColor: "rgb(210, 214, 222)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgb(220,220,220)",
                    data: <?php echo json_encode($bulan); ?>
                },

                {
                    label: "Visitors",
                    backgroundColor: "rgb(75, 192, 192)",
                    fillColor: "rgb(75, 192, 192)",
                    strokeColor: "rgba(60,141,188,0.8)",
                    pointColor: "#FF6384",
                    pointStrokeColor: "rgba(60,141,188,1)",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(60,141,188,1)",
                    data: <?php echo json_encode($value); ?>
                }
            ]
        };


        var salesChart = new Chart(salesChartCanvas, {
            type: "line",
            data: salesChartData,
            responsive: true,
            maintainAspectRatio: true,

        });

    });
</script>

<script>
    var oilCanvas = document.getElementById("oilChart");


    Chart.defaults.global.defaultFontSize = 18;

    var oilData = {
        labels: <?php echo json_encode($browser); ?>,
        datasets: [{
            data: <?php echo json_encode($jml); ?>,
            backgroundColor: [
                "#3498db",
                "#1abc9c",
                "#9b59b6",
                "#f1c40f",
                "#e74c3c",
                "#3742fa",
                "#ffa502",
                "#ff6b81",
                "#747d8c"
            ]
        }]
    };

    var pieChart = new Chart(oilCanvas, {
        type: 'pie',
        data: oilData
    });
</script>
<script type="text/javascript">
    var table_win;
    var table_gen;
    var table_student;
    $(document).ready(function() {

        table_win = $('#table_win').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('backend/Dashboard/win') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

        table_gen = $('#table_gen').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('backend/Dashboard/gen') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

        table_student = $('#table_student').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('backend/Dashboard/student') ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],

        });

    });
    </script>
