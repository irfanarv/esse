<?php
defined('BASEPATH') or exit('No direct script access allowed');



class AuthM extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
  }

  function login_admin($username, $password)
  {
    $this->db->select('id,nama,email,password');
    $this->db->where('email', $username);
    $this->db->where('password', sha1($password));
    $q = $this->db->get('user');
    $user = $q->result();
    $num = $q->num_rows();
    if ($num > 0) {
      $this->session->set_userdata('id', $user[0]->id);
      $this->session->set_userdata('nama', $user[0]->nama);
      return true;
    } else {
      return false;
    }
  }

  public function check_login($email, $password)
  {

    $this->db->where('email', $email);
    $query = $this->db->get('participans');
    $user = $query->result();
    if ($query->num_rows() == 1) {
      $hash = $query->row('password');
      if (password_verify($password, $hash)) {
        $this->session->set_userdata('id', $user[0]->id);
        $this->session->set_userdata('name', $user[0]->name);
        // return $query->result();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  public function checkEmail($username)
  {
    $this->db->where('email', $username);
    return $this->db->get('participans')->row_array();
  }

  function reset_password($email, $newpass)
  {
    $this->load->library('email');
    $this->load->config('email');

    $from   = "makechange.id <admin@makechange.id>";
    $_from   = "admin@makechange.id";
    $_me   = "Make Change";

    $subject = 'Make Change | Reset Password';

    $data['email'] = $email;
    $data['newpass'] = $newpass;
    //
    $email_body = $this->load->view('template/email/reset', $data, true);

    $this->email->set_newline("\r\n");
    $this->email->from($from);
    $this->email->to($email); 
    $this->email->subject($subject);
    $this->email->message($email_body);
    $this->email->set_mailtype("html");
    $this->email->reply_to($_from, $_me);

    if ($this->email->send()) {
    } else {
      show_error($this->email->print_debugger());
    }
  }

  // google oauth
  function google_already($id)
  {
    $this->db->where('oauth_uid', $id);
    $query = $this->db->get('account');
    if ($query->num_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }



  function update_google_user($data, $id)
  {
    $this->db->where('oauth_uid', $id);
    $this->db->update('account', $data);
  }

  function insert_google_user($data)
  {
    $this->db->insert('account', $data);
  }
}
